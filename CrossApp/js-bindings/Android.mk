LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := scriptingcore-v8

LOCAL_MODULE_FILENAME := libscriptingcore-v8


LOCAL_CFLAGS := -DCOCOS2D_JAVASCRIPT

LOCAL_EXPORT_CFLAGS := -DCOCOS2D_JAVASCRIPT

LOCAL_C_INCLUDES := $(LOCAL_PATH) \
										$(LOCAL_PATH)/auto \
										$(LOCAL_PATH)/manual \
										$(LOCAL_PATH)/../platform \
										$(LOCAL_PATH)/../support \
										$(LOCAL_PATH)/../basics \
										$(LOCAL_PATH)/../cocoa \
										$(LOCAL_PATH)/../control \
										$(LOCAL_PATH)/../images \
										$(LOCAL_PATH)/../view

LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH) \

LOCAL_WHOLE_STATIC_LIBRARIES := v8_static
LOCAL_WHOLE_STATIC_LIBRARIES += CrossApp_static

LOCAL_LDLIBS := -landroid
LOCAL_LDLIBS += -llog

include $(BUILD_STATIC_LIBRARY)

$(call import-module,../../external/android)
