/****************************************************************************
 Copyright (c) 2017-2021 Xiamen Yaji Software Co., Ltd.

 http://www.cocos.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated engine source code (the "Software"), a limited,
 worldwide, royalty-free, non-assignable, revocable and non-exclusive license
 to use Cocos Creator solely to develop games on your target platforms. You shall
 not use Cocos Creator software for developing other software or tools that's
 used for developing games. You are not granted to publish, distribute,
 sublicense, and/or sell copies of Cocos Creator.

 The software or tools in this License Agreement are licensed, not sold.
 Xiamen Yaji Software Co., Ltd. reserves all rights not expressly granted to you.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
****************************************************************************/

#include "jsb_conversions.h"

#include <regex>
#include <sstream>

#include "../base/TypeDef.h"
#include "../../math/CAMath.h"

// seval to native

bool jsb_set_extend_property(const char *ns, const char *clsName)
{ //NOLINT
    se::Object *globalObj = se::ScriptEngine::getInstance()->getGlobalObject();
    se::Value   nsVal;
    if (globalObj->getProperty(ns, &nsVal) && nsVal.isObject()) {
        se::Value ccVal;
        if (globalObj->getProperty("ca", &ccVal) && ccVal.isObject()) {
            se::Value ccClassVal;
            if (ccVal.toObject()->getProperty("Class", &ccClassVal) && ccClassVal.isObject()) {
                se::Value extendVal;
                if (ccClassVal.toObject()->getProperty("extend", &extendVal) && extendVal.isObject() && extendVal.toObject()->isFunction()) {
                    se::Value targetClsVal;
                    if (nsVal.toObject()->getProperty(clsName, &targetClsVal) && targetClsVal.isObject()) {
                        return targetClsVal.toObject()->setProperty("extend", extendVal);
                    }
                }
            }
        }
    }
    return false;
}

bool seval_to_int32(const se::Value &v, int32_t *ret) {
    assert(ret != nullptr);
    if (v.isNumber()) {
        *ret = v.toInt32();
        return true;
    }
    if (v.isBoolean()) {
        *ret = v.toBoolean() ? 1 : 0;
        return true;
    }
    *ret = 0;
    return false;
}

bool seval_to_uint32(const se::Value &v, uint32_t *ret) {
    assert(ret != nullptr);
    if (v.isNumber()) {
        *ret = v.toUint32();
        return true;
    }
    if (v.isBoolean()) {
        *ret = v.toBoolean() ? 1 : 0;
        return true;
    }
    *ret = 0;
    return false;
}

bool seval_to_int8(const se::Value &v, int8_t *ret) {
    assert(ret != nullptr);
    if (v.isNumber()) {
        *ret = v.toInt8();
        return true;
    }
    if (v.isBoolean()) {
        *ret = v.toBoolean() ? 1 : 0;
        return true;
    }
    *ret = 0;
    return false;
}

bool seval_to_uint8(const se::Value &v, uint8_t *ret) {
    assert(ret != nullptr);
    if (v.isNumber()) {
        *ret = v.toUint8();
        return true;
    }
    if (v.isBoolean()) {
        *ret = v.toBoolean() ? 1 : 0;
        return true;
    }
    *ret = 0;
    return false;
}

bool seval_to_int16(const se::Value &v, int16_t *ret) {
    assert(ret != nullptr);
    if (v.isNumber()) {
        *ret = v.toInt16();
        return true;
    }
    if (v.isBoolean()) {
        *ret = v.toBoolean() ? 1 : 0;
        return true;
    }
    *ret = 0;
    return false;
}

bool seval_to_uint16(const se::Value &v, uint16_t *ret) {
    assert(ret != nullptr);
    if (v.isNumber()) {
        *ret = v.toUint16();
        return true;
    }
    if (v.isBoolean()) {
        *ret = v.toBoolean() ? 1 : 0;
        return true;
    }
    *ret = 0;
    return false;
}

bool seval_to_boolean(const se::Value &v, bool *ret) {
    assert(ret != nullptr);
    if (v.isBoolean()) {
        *ret = v.toBoolean();
    } else if (v.isNumber()) {
        *ret = v.toInt32() != 0;
    } else if (v.isNullOrUndefined()) {
        *ret = false;
    } else if (v.isObject()) {
        *ret = true;
    } else if (v.isString()) {
        *ret = !v.toString().empty();
    } else {
        *ret = false;
        assert(false);
    }

    return true;
}

bool seval_to_float(const se::Value &v, float *ret) {
    assert(ret != nullptr);
    if (v.isNumber()) {
        *ret = v.toFloat();
        if (!std::isnan(*ret)) {
            return true;
        }
    }
    *ret = 0.0f;
    return false;
}

bool seval_to_double(const se::Value &v, double *ret) {
    if (v.isNumber()) {
        *ret = v.toNumber();
        if (!std::isnan(*ret)) {
            return true;
        }
    }
    *ret = 0.0;
    return false;
}

bool seval_to_size(const se::Value &v, size_t *ret) {
    assert(ret != nullptr);
    if (v.isNumber()) {
        *ret = v.toSize();
        return true;
    }
    *ret = 0;
    return false;
}

bool seval_to_std_string(const se::Value &v, std::string *ret) {
    assert(ret != nullptr);
    *ret = v.toStringForce();
    return true;
}

bool seval_to_dpoint(const se::Value &v, CrossApp::DPoint *pt) {
    assert(pt != nullptr);
    SE_PRECONDITION2(v.isObject(), false, "Convert parameter to Vec2 failed!");
    se::Object *obj = v.toObject();
    se::Value   x;
    se::Value   y;
    bool        ok = obj->getProperty("x", &x);
    SE_PRECONDITION3(ok && x.isNumber(), false, *pt = DPointZero);
    ok = obj->getProperty("y", &y);
    SE_PRECONDITION3(ok && y.isNumber(), false, *pt = DPointZero);
    pt->x = x.toFloat();
    pt->y = y.toFloat();
    return true;
}

bool seval_to_dpoint3d(const se::Value &v, CrossApp::DPoint3D *pt) {
    assert(pt != nullptr);
    SE_PRECONDITION2(v.isObject(), false, "Convert parameter to Vec3 failed!");
    se::Object *obj = v.toObject();
    se::Value   x;
    se::Value   y;
    se::Value   z;
    bool        ok = obj->getProperty("x", &x);
    SE_PRECONDITION3(ok && x.isNumber(), false, *pt = DPoint3DZero);
    ok = obj->getProperty("y", &y);
    SE_PRECONDITION3(ok && y.isNumber(), false, *pt = DPoint3DZero);
    ok = obj->getProperty("z", &z);
    SE_PRECONDITION3(ok && z.isNumber(), false, *pt = DPoint3DZero);
    pt->x = x.toFloat();
    pt->y = y.toFloat();
    pt->z = z.toFloat();
    return true;
}

bool seval_to_drect(const se::Value &v, CrossApp::DRect *pt) {
//    assert(pt != nullptr);
    SE_PRECONDITION2(v.isObject(), false, "Convert parameter to Vec3 failed!");
    se::Object *obj = v.toObject();
    se::Value   x;
    se::Value   y;
    se::Value   width;
    se::Value   height;
    bool        ok = obj->getProperty("x", &x);
    SE_PRECONDITION3(ok && x.isNumber(), false, *pt = DRectZero);
    ok = obj->getProperty("y", &y);
    SE_PRECONDITION3(ok && y.isNumber(), false, *pt = DRectZero);
    ok = obj->getProperty("width", &width);
    SE_PRECONDITION3(ok && width.isNumber(), false, *pt = DRectZero);
    ok = obj->getProperty("height", &height);
    SE_PRECONDITION3(ok && height.isNumber(), false, *pt = DRectZero);
    pt->origin.x = x.toFloat();
    pt->origin.y = y.toFloat();
    pt->size.width = width.toFloat();
    pt->size.height = height.toFloat();
    return true;
}

bool seval_to_dhorizontallayout(const se::Value &v, CrossApp::DHorizontalLayout *layout){
    assert(layout != nullptr);
    SE_PRECONDITION2(v.isObject(), false, "Convert parameter to DHorizontalLayout failed!");
    se::Object *obj = v.toObject();
    se::Value   left;
    se::Value   right;
    se::Value   width;
    se::Value   center;
    se::Value   normalizedWidth;
    
    bool        ok = obj->getProperty("left", &left);
    SE_PRECONDITION3(ok && left.isNumber(), false, *layout = DHorizontalLayoutZero);
    ok = obj->getProperty("right", &right);
    SE_PRECONDITION3(ok && right.isNumber(), false, *layout = DHorizontalLayoutZero);
    ok = obj->getProperty("width", &width);
    SE_PRECONDITION3(ok && width.isNumber(), false, *layout = DHorizontalLayoutZero);
    ok = obj->getProperty("center", &center);
    SE_PRECONDITION3(ok && center.isNumber(), false, *layout = DHorizontalLayoutZero);
    ok = obj->getProperty("normalizedWidth", &normalizedWidth);
    SE_PRECONDITION3(ok && normalizedWidth.isNumber(), false, *layout = DHorizontalLayoutZero);
    
    layout->left = left.toFloat();
    layout->right = right.toFloat();
    layout->width = width.toFloat();
    layout->center = center.toFloat();
    layout->normalizedWidth = normalizedWidth.toFloat();
    
    return true;
}

bool seval_to_dverticallayout(const se::Value &v, CrossApp::DVerticalLayout *layout){
    assert(layout != nullptr);
    SE_PRECONDITION2(v.isObject(), false, "Convert parameter to DVerticalLayout failed!");
    se::Object *obj = v.toObject();
    se::Value   top;
    se::Value   bottom;
    se::Value   height;
    se::Value   center;
    se::Value   normalizedHeight;
    
    bool        ok = obj->getProperty("top", &top);
    SE_PRECONDITION3(ok && top.isNumber(), false, *layout = DVerticalLayoutZero);
    ok = obj->getProperty("bottom", &bottom);
    SE_PRECONDITION3(ok && bottom.isNumber(), false, *layout = DVerticalLayoutZero);
    ok = obj->getProperty("height", &height);
    SE_PRECONDITION3(ok && height.isNumber(), false, *layout = DVerticalLayoutZero);
    ok = obj->getProperty("center", &center);
    SE_PRECONDITION3(ok && center.isNumber(), false, *layout = DVerticalLayoutZero);
    ok = obj->getProperty("normalizedHeight", &normalizedHeight);
    SE_PRECONDITION3(ok && normalizedHeight.isNumber(), false, *layout = DVerticalLayoutZero);
    
    layout->top = top.toFloat();
    layout->bottom = bottom.toFloat();
    layout->height = height.toFloat();
    layout->center = center.toFloat();
    layout->normalizedHeight = normalizedHeight.toFloat();
    
    return true;
}


bool seval_to_dlayout(const se::Value &v,CrossApp::DLayout *layout){
    assert(layout != nullptr);
    SE_PRECONDITION2(v.isObject(), false, "Convert parameter to DLayout failed!");
    se::Object *obj = v.toObject();
    se::Value   verticallayout;
    se::Value   horizontallayout;

    bool        ok = obj->getProperty("vertical", &verticallayout);
    SE_PRECONDITION3(ok && verticallayout.isObject(), false, layout->vertical = DVerticalLayoutZero);
    ok = obj->getProperty("horizontal", &horizontallayout);
    SE_PRECONDITION3(ok && horizontallayout.isObject(), false, layout->horizontal = DHorizontalLayoutZero);

    seval_to_dverticallayout(verticallayout, &layout->vertical);
    seval_to_dhorizontallayout(horizontallayout, &layout->horizontal);

    return true;
    
}

bool seval_to_cacolor4b(const se::Value &v, CrossApp::CAColor4B *color4b){
    assert(color4b != nullptr);
    SE_PRECONDITION2(v.isObject(), false, "Convert parameter to CAColor4B failed!");
    se::Object *obj = v.toObject();
    se::Value   r;
    se::Value   g;
    se::Value   b;
    se::Value   a;
    
    bool        ok = obj->getProperty("r", &r);
    SE_PRECONDITION3(ok && r.isNumber(), false, *color4b = CrossApp::CAColor4B::BLACK);
    ok = obj->getProperty("g", &g);
    SE_PRECONDITION3(ok && g.isNumber(), false, *color4b = CrossApp::CAColor4B::BLACK);
    ok = obj->getProperty("b", &b);
    SE_PRECONDITION3(ok && b.isNumber(), false, *color4b = CrossApp::CAColor4B::BLACK);
    ok = obj->getProperty("a", &a);
    SE_PRECONDITION3(ok && a.isNumber(), false, *color4b = CrossApp::CAColor4B::BLACK);
    color4b->r = r.toInt32();
    color4b->g = g.toInt32();
    color4b->b = b.toInt32();
    color4b->a = a.toInt32();
    return true;
}

bool seval_to_cacolor4f(const se::Value &v, CrossApp::CAColor4F *color4f)
{
    assert(color4f != nullptr);
    SE_PRECONDITION2(v.isObject(), false, "Convert parameter to CAColor4B failed!");
    se::Object *obj = v.toObject();
    se::Value   r;
    se::Value   g;
    se::Value   b;
    se::Value   a;
    
    bool        ok = obj->getProperty("r", &r);
    SE_PRECONDITION3(ok && r.isNumber(), false, *color4f = CrossApp::CAColor4F::BLACK);
    ok = obj->getProperty("g", &g);
    SE_PRECONDITION3(ok && g.isNumber(), false, *color4f = CrossApp::CAColor4F::BLACK);
    ok = obj->getProperty("b", &b);
    SE_PRECONDITION3(ok && b.isNumber(), false, *color4f = CrossApp::CAColor4F::BLACK);
    ok = obj->getProperty("a", &a);
    SE_PRECONDITION3(ok && a.isNumber(), false, *color4f = CrossApp::CAColor4F::BLACK);
    color4f->r = r.toFloat();
    color4f->g = g.toFloat();
    color4f->b = b.toFloat();
    color4f->a = a.toFloat();
    return true;
}

bool seval_to_cafontshadow(const se::Value &v, CrossApp::CAFontShadow *shadow)
{
    assert(shadow != nullptr);
    SE_PRECONDITION2(v.isObject(), false, "Convert parameter to CAFontShadow failed!");
    se::Object *obj = v.toObject();
    
    se::Value   shadowEnabled;//bool
    se::Value   shadowOffset;//DSize
    se::Value   shadowBlur;//float
    se::Value   shadowColor;//CAColor4B

    bool        ok = obj->getProperty("shadowEnabled", &shadowEnabled);
    if (ok && shadowEnabled.isBoolean()){
        shadow->shadowEnabled = shadowEnabled.toBoolean();
    }
    ok = obj->getProperty("shadowOffset", &shadowOffset);
    if (ok && shadowOffset.isObject()){
        ok = seval_to_dsize(shadowOffset, &shadow->shadowOffset);
    }
    
    ok = obj->getProperty("shadowBlur", &shadowBlur);
    if (ok && shadowBlur.isNumber()){
        shadow->shadowBlur = shadowBlur.toFloat();
    }
    
    ok = obj->getProperty("shadowColor", &shadowColor);
    if (ok && shadowColor.isObject()){
        ok = seval_to_cacolor4b(shadowColor, &shadow->shadowColor);
    }
    return true;
}

bool seval_to_cafontstroke(const se::Value &v, CrossApp::CAFontStroke *stroke)
{
    assert(stroke != nullptr);
    SE_PRECONDITION2(v.isObject(), false, "Convert parameter to CAFontStroke failed!");
    se::Object *obj = v.toObject();
    
    se::Value   strokeEnabled;//bool
    se::Value   strokeColor;//CAColor4B
    se::Value   strokeSize;//float

    bool        ok = obj->getProperty("strokeEnabled", &strokeEnabled);
    if (ok && strokeEnabled.isBoolean()){
        stroke->strokeEnabled = strokeEnabled.toBoolean();
    }
    
    ok = obj->getProperty("strokeColor", &strokeColor);
    if (ok && strokeColor.isObject()){
        ok = seval_to_cacolor4b(strokeColor, &stroke->strokeColor);
    }
    
    ok = obj->getProperty("strokeSize", &strokeSize);
    if (ok && strokeSize.isObject()){
        stroke->strokeSize = strokeSize.toFloat();
    }
    return true;
}

bool seval_to_cafont(const se::Value &v, CrossApp::CAFont *font)
{
    assert(font != nullptr);
    SE_PRECONDITION2(v.isObject(), false, "Convert parameter to CAFont failed!");
    se::Object *obj = v.toObject();
    
    se::Value   bold;//bool
    se::Value   underline;//bool
    se::Value   deleteline;//bool
    se::Value   italics;//bool
    se::Value   italicsValue;//float
    se::Value   wordWrap;//bool
    se::Value   fontName;//std::string
    se::Value   fontSize;//float
    se::Value   lineSpacing;//float
    se::Value   color;//CAColor4B
    se::Value   shadow;//CAFontShadow
    se::Value   stroke;//CAFontStroke
    
    bool        ok = obj->getProperty("bold", &bold);
    if (ok && bold.isBoolean()){
        font->bold = bold.toBoolean();
    }
    ok = obj->getProperty("underLine", &underline);
    if (ok && underline.isBoolean()){
        font->underLine = underline.toBoolean();
    }
    
    ok = obj->getProperty("deleteline", &deleteline);
    if (ok && underline.isBoolean()){
        font->deleteLine = underline.toBoolean();
    }
    
    ok = obj->getProperty("italics", &italics);
    if (ok && italics.isBoolean()){
        font->italics = italics.toBoolean();
    }
    
    ok = obj->getProperty("italicsValue", &italicsValue);
    if (ok && italicsValue.isNumber()){
        font->italicsValue = italicsValue.toFloat();
    }
    
    ok = obj->getProperty("wordWrap", &wordWrap);
    if (ok && wordWrap.isBoolean()){
        font->wordWrap = wordWrap.toBoolean();
    }
    
    ok = obj->getProperty("fontName", &fontName);
    if (ok && fontName.isString()){
        font->fontName = fontName.toString();
    }
    
    ok = obj->getProperty("fontSize", &fontSize);
    if (ok && fontSize.isNumber()){
        font->fontSize = fontSize.toFloat();
    }
    
    ok = obj->getProperty("lineSpacing", &lineSpacing);
    if (ok && lineSpacing.isNumber()){
        font->lineSpacing = lineSpacing.toFloat();
    }
    
    ok = obj->getProperty("color", &color);
    if (ok){
        CrossApp::CAColor4B color4b;
        seval_to_cacolor4b(color, &color4b);
        font->color = color4b;
    }
    
    ok = obj->getProperty("color", &color);
    if (ok){
        CrossApp::CAColor4B color4b;
        seval_to_cacolor4b(color, &color4b);
        font->color = color4b;
    }
    
    ok = obj->getProperty("shadow", &shadow);
    if (ok && shadow.isObject()){
        ok = seval_to_cafontshadow(shadow, &font->shadow);
    }
    
    ok = obj->getProperty("stroke", &stroke);
    if (ok && stroke.isObject()){
        ok = seval_to_cafontstroke(stroke, &font->stroke);
    }

    return true;
}

bool seval_to_mat(const se::Value &v, int length, float *out) {
    assert(out != nullptr);
    SE_PRECONDITION2(v.isObject(), false, "Convert parameter to Matrix failed!");
    se::Object *obj = v.toObject();

    se::Value tmp;
    char      propName[3] = {0};
    for (int i = 0; i < length; ++i) {
        snprintf(propName, 3, "m%2d", i);
        obj->getProperty(propName, &tmp);
        *(out + i) = tmp.toFloat();
    }

    return true;
}

bool seval_to_matrix(const se::Value &v, CrossApp::Mat4 *mat) {
    assert(mat != nullptr);
    SE_PRECONDITION2(v.isObject(), false, "Convert parameter to Matrix4 failed!");
    se::Object *obj = v.toObject();

    if (obj->isTypedArray()) {
        // typed array
        SE_PRECONDITION2(obj->isTypedArray(), false, "Convert parameter to Matrix4 failed!");

        size_t   length = 0;
        uint8_t *ptr    = nullptr;
        obj->getTypedArrayData(&ptr, &length);

        memcpy(mat->m, ptr, length);
    } else {
        bool        ok = false;
        se::Value   tmp;
        std::string prefix = "m";
        for (uint32_t i = 0; i < 16; ++i) {
            std::string name;
            if (i < 10) {
                name = prefix + "0" + std::to_string(i);
            } else {
                name = prefix + std::to_string(i);
            }
            ok = obj->getProperty(name.c_str(), &tmp);
            SE_PRECONDITION3(ok, false, *mat = CrossApp::Mat4::IDENTITY);

            if (tmp.isNumber()) {
                mat->m[i] = tmp.toFloat();
            } else {
                SE_REPORT_ERROR("%u, not supported type in matrix", i);
                *mat = CrossApp::Mat4::IDENTITY;
                return false;
            }

            tmp.setUndefined();
        }
    }

    return true;
}

bool seval_to_Uint8Array(const se::Value &v, uint8_t *ret) {
    assert(ret != nullptr);
    SE_PRECONDITION2(v.isObject(), false, "Convert parameter to Array failed!");
    se::Object *obj = v.toObject();
    SE_PRECONDITION2(obj->isArray(), false, "Convert parameter to Array failed!");

    CC_UNUSED bool ok     = true;
    uint32_t       length = 0;
    obj->getArrayLength(&length);
    se::Value value;
    uint8_t   data = 0;
    for (uint32_t i = 0; i < length; ++i) {
        if (obj->getArrayElement(i, &value)) {
            ok = seval_to_uint8(value, &data);
            SE_PRECONDITION2(ok, false, "Convert parameter to Array failed!");
            ret[i] = data;
        }
    }

    return true;
}

bool seval_to_uintptr_t(const se::Value &v, uintptr_t *ret) {
    assert(ret != nullptr);
    if (v.isNumber()) {
        *ret = static_cast<uintptr_t>(v.toDouble());
        return true;
    }
    *ret = 0UL;
    return false;
}

bool seval_to_Data(const se::Value &v, CrossApp::CAData *ret)
{
    assert(ret != nullptr);
    SE_PRECONDITION2(v.isObject() && v.toObject()->isTypedArray(), false, "Convert parameter to Data failed!");
    uint8_t *ptr    = nullptr;
    size_t   length = 0;
    bool     ok     = v.toObject()->getTypedArrayData(&ptr, &length);
    if (ok) {
        ret->copy(ptr, length);
    } else {
        ret->clear();
    }
    return ok;
}

bool seval_to_dsize(const se::Value &v, CrossApp::DSize *size) {
    assert(size != nullptr);
    SE_PRECONDITION2(v.isObject(), false, "Convert parameter to Size failed!");
    se::Object *obj = v.toObject();
    se::Value   width;
    se::Value   height;

    bool ok = obj->getProperty("width", &width);
    SE_PRECONDITION3(ok && width.isNumber(), false, *size = DSizeZero);
    ok = obj->getProperty("height", &height);
    SE_PRECONDITION3(ok && height.isNumber(), false, *size = DSizeZero);
    size->width  = width.toFloat();
    size->height = height.toFloat();
    return true;
}

bool seval_to_cavalue(const se::Value &v, CrossApp::CAValue *ret) {
    assert(ret != nullptr);
    bool ok = true;
    if (v.isObject()) {
        se::Object *jsobj = v.toObject();
        if (!jsobj->isArray()) {
            // It's a normal js object.
            CrossApp::CAValueMap dictVal;
            ok = seval_to_cavaluemap(v, &dictVal);
            SE_PRECONDITION3(ok, false, *ret = CrossApp::CAValue::Null);
            *ret = CrossApp::CAValue(dictVal);
        } else {
            // It's a js array object.
            CrossApp::CAValueVector arrVal;
            ok = seval_to_cavaluevector(v, &arrVal);
            SE_PRECONDITION3(ok, false, *ret = CrossApp::CAValue::Null);
            *ret = CrossApp::CAValue(arrVal);
        }
    } else if (v.isString()) {
        *ret = v.toString();
    } else if (v.isNumber()) {
        *ret = v.toNumber();
    } else if (v.isBoolean()) {
        *ret = v.toBoolean();
    } else if (v.isNullOrUndefined()) {
        *ret = CrossApp::CAValue::Null;
    } else {
        SE_PRECONDITION2(false, false, "type not supported!");
    }

    return ok;
}

bool seval_to_cavaluemap(const se::Value &v, CrossApp::CAValueMap *ret) {
    assert(ret != nullptr);

    if (v.isNullOrUndefined()) {
        ret->clear();
        return true;
    }

    SE_PRECONDITION3(v.isObject(), false, ret->clear());
    SE_PRECONDITION3(!v.isNullOrUndefined(), false, ret->clear());

    se::Object *obj = v.toObject();

    CrossApp::CAValueMap &dict = *ret;

    std::vector<std::string> allKeys;
    SE_PRECONDITION3(obj->getAllKeys(&allKeys), false, ret->clear());

    bool      ok = false;
    se::Value value;
    CrossApp::CAValue ccvalue;
    for (const auto &key : allKeys) {
        SE_PRECONDITION3(obj->getProperty(key.c_str(), &value), false, ret->clear());
        ok = seval_to_cavalue(value, &ccvalue);
        SE_PRECONDITION3(ok, false, ret->clear());
        dict.emplace(key, ccvalue);
    }

    return true;
}

static bool isNumberString(const std::string &str) {
    for (const auto &c : str) {
        if (!isdigit(c)) {
            return false;
        }
    }
    return true;
}

bool seval_to_cavaluemapintkey(const se::Value &v, CrossApp::CAValueMapIntKey *ret) {
    assert(ret != nullptr);
    if (v.isNullOrUndefined()) {
        ret->clear();
        return true;
    }

    SE_PRECONDITION3(v.isObject(), false, ret->clear());
    SE_PRECONDITION3(!v.isNullOrUndefined(), false, ret->clear());

    se::Object *obj = v.toObject();

    CrossApp::CAValueMapIntKey &dict = *ret;

    std::vector<std::string> allKeys;
    SE_PRECONDITION3(obj->getAllKeys(&allKeys), false, ret->clear());

    bool      ok = false;
    se::Value value;
    CrossApp::CAValue ccvalue;
    for (const auto &key : allKeys) {
        SE_PRECONDITION3(obj->getProperty(key.c_str(), &value), false, ret->clear());

        if (!isNumberString(key)) {
            SE_LOGD("seval_to_ccvaluemapintkey, found not numeric key: %s", key.c_str());
            continue;
        }

        int intKey = atoi(key.c_str());

        ok = seval_to_cavalue(value, &ccvalue);
        SE_PRECONDITION3(ok, false, ret->clear());
        dict.emplace(intKey, ccvalue);
    }

    return true;
}

bool seval_to_cavaluevector(const se::Value &v, CrossApp::CAValueVector *ret) {
    assert(ret != nullptr);

    SE_PRECONDITION3(v.isObject(), false, ret->clear());

    se::Object *obj = v.toObject();
    SE_PRECONDITION3(obj->isArray(), false, ret->clear());

    uint32_t len = 0;
    obj->getArrayLength(&len);

    bool      ok = false;
    se::Value value;
    CrossApp::CAValue ccvalue;
    for (uint32_t i = 0; i < len; ++i) {
        if (obj->getArrayElement(i, &value)) {
            ok = seval_to_cavalue(value, &ccvalue);
            SE_PRECONDITION3(ok, false, ret->clear());
            ret->push_back(ccvalue);
        }
    }

    return true;
}

bool seval_to_std_vector_string(const se::Value &v, std::vector<std::string> *ret) {
    assert(ret != nullptr);
    SE_PRECONDITION2(v.isObject(), false, "Convert parameter to vector of String failed!");
    se::Object *obj = v.toObject();
    SE_PRECONDITION2(obj->isArray(), false, "Convert parameter to vector of String failed!");
    uint32_t len = 0;
    if (obj->getArrayLength(&len)) {
        se::Value value;
        for (uint32_t i = 0; i < len; ++i) {
            SE_PRECONDITION3(obj->getArrayElement(i, &value) && value.isString(), false, ret->clear());
            ret->push_back(value.toString());
        }
        return true;
    }

    ret->clear();
    return true;
}

bool seval_to_std_vector_int(const se::Value &v, std::vector<int> *ret) {
    assert(ret != nullptr);
    SE_PRECONDITION2(v.isObject(), false, "Convert parameter to vector of int failed!");
    se::Object *obj = v.toObject();

    if (obj->isArray()) {
        uint32_t len = 0;
        if (obj->getArrayLength(&len)) {
            se::Value value;
            for (uint32_t i = 0; i < len; ++i) {
                SE_PRECONDITION3(obj->getArrayElement(i, &value) && value.isNumber(), false, ret->clear());
                ret->push_back(value.toInt32());
            }
            return true;
        }
    } else if (obj->isTypedArray()) {
        size_t                     bytesPerElements = 0;
        uint8_t *                  data             = nullptr;
        size_t                     dataBytes        = 0;
        se::Object::TypedArrayType type             = obj->getTypedArrayType();

#define SE_UINT8_PTR_TO_INT(ptr)  (*((uint8_t *)(ptr)))
#define SE_UINT16_PTR_TO_INT(ptr) (*((uint16_t *)(ptr)))
#define SE_UINT32_PTR_TO_INT(ptr) (*((uint32_t *)(ptr)))

        if (obj->getTypedArrayData(&data, &dataBytes)) {
            for (size_t i = 0; i < dataBytes; i += bytesPerElements) {
                switch (type) {
                    case se::Object::TypedArrayType::INT8:
                    case se::Object::TypedArrayType::UINT8:
                    case se::Object::TypedArrayType::UINT8_CLAMPED:
                        ret->push_back(SE_UINT8_PTR_TO_INT(data + i));
                        bytesPerElements = 1;
                        break;
                    case se::Object::TypedArrayType::INT16:
                    case se::Object::TypedArrayType::UINT16:
                        ret->push_back(SE_UINT16_PTR_TO_INT(data + i));
                        bytesPerElements = 2;
                        break;
                    case se::Object::TypedArrayType::INT32:
                    case se::Object::TypedArrayType::UINT32:
                        ret->push_back(SE_UINT32_PTR_TO_INT(data + i));
                        bytesPerElements = 4;
                        break;
                    default:
                        SE_LOGE("Unsupported typed array: %d\n", (int)type);
                        assert(false);
                        break;
                }
            }
        }

#undef SE_UINT8_PTR_TO_INT
#undef SE_UINT16_PTR_TO_INT
#undef SE_UINT32_PTR_TO_INT

        return true;
    } else {
        assert(false);
    }

    ret->clear();
    return true;
}

bool seval_to_std_vector_uint16(const se::Value &v, std::vector<uint16_t> *ret) {
    assert(ret != nullptr);
    SE_PRECONDITION2(v.isObject(), false, "Convert parameter to vector of uint16 failed!");
    se::Object *obj = v.toObject();

    if (obj->isArray()) {
        uint32_t len = 0;
        if (obj->getArrayLength(&len)) {
            se::Value value;
            for (uint32_t i = 0; i < len; ++i) {
                SE_PRECONDITION3(obj->getArrayElement(i, &value) && value.isNumber(), false, ret->clear());
                ret->push_back(value.toUint16());
            }
            return true;
        }
    } else if (obj->isTypedArray()) {
        size_t                     bytesPerElements = 0;
        uint8_t *                  data             = nullptr;
        size_t                     dataBytes        = 0;
        se::Object::TypedArrayType type             = obj->getTypedArrayType();

        if (obj->getTypedArrayData(&data, &dataBytes)) {
            for (size_t i = 0; i < dataBytes; i += bytesPerElements) {
                switch (type) {
                    case se::Object::TypedArrayType::INT16:
                    case se::Object::TypedArrayType::UINT16:
                        ret->push_back(*((uint16_t *)(data + i)));
                        bytesPerElements = 2;
                        break;
                    default:
                        SE_LOGE("Unsupported typed array: %d\n", (int)type);
                        assert(false);
                        break;
                }
            }
        }
        return true;
    } else {
        assert(false);
    }
    ret->clear();
    return true;
}

bool seval_to_std_vector_float(const se::Value &v, std::vector<float> *ret) {
    assert(ret != nullptr);
    SE_PRECONDITION2(v.isObject(), false, "Convert parameter to vector of float failed!");
    se::Object *obj = v.toObject();
    SE_PRECONDITION2(obj->isArray(), false, "Convert parameter to vector of float failed!");
    uint32_t len = 0;
    if (obj->getArrayLength(&len)) {
        se::Value value;
        for (uint32_t i = 0; i < len; ++i) {
            SE_PRECONDITION3(obj->getArrayElement(i, &value) && value.isNumber(), false, ret->clear());
            ret->push_back(value.toFloat());
        }
        return true;
    }

    ret->clear();
    return true;
}

bool seval_to_std_vector_dpoint(const se::Value &v, std::vector<CrossApp::DPoint> *ret) {
    assert(ret != nullptr);
    SE_PRECONDITION2(v.isObject(), false, "Convert parameter to vector of Vec2 failed!");
    se::Object *obj = v.toObject();
    SE_PRECONDITION2(obj->isArray(), false, "Convert parameter to vector of Vec2 failed!");
    uint32_t len = 0;
    if (obj->getArrayLength(&len)) {
        se::Value value;
        CrossApp::DPoint  pt;
        for (uint32_t i = 0; i < len; ++i) {
            SE_PRECONDITION3(obj->getArrayElement(i, &value) && seval_to_dpoint(value, &pt), false, ret->clear());
            ret->push_back(pt);
        }
        return true;
    }

    ret->clear();
    return true;
}

bool seval_to_std_map_string_string(const se::Value &v, std::map<std::string, std::string> *ret) {
    assert(ret != nullptr);

    if (v.isNullOrUndefined()) {
        ret->clear();
        return true;
    }

    SE_PRECONDITION2(v.isObject(), false, "Convert parameter to map of String to String failed!");
    SE_PRECONDITION3(!v.isNullOrUndefined(), false, ret->clear());

    se::Object *obj = v.toObject();

    std::vector<std::string> allKeys;
    SE_PRECONDITION3(obj->getAllKeys(&allKeys), false, ret->clear());

    bool        ok = false;
    se::Value   value;
    std::string strValue;
    for (const auto &key : allKeys) {
        SE_PRECONDITION3(obj->getProperty(key.c_str(), &value), false, ret->clear());
        ok = seval_to_std_string(value, &strValue);
        SE_PRECONDITION3(ok, false, ret->clear());
        ret->emplace(key, strValue);
    }

    return true;
}


//////////////////////////////////////////////////////////////////////////////////
// native to seval

bool int32_to_seval(int32_t v, se::Value *ret) {
    ret->setInt32(v);
    return true;
}

bool uint32_to_seval(uint32_t v, se::Value *ret) {
    ret->setUint32(v);
    return true;
}

bool int16_to_seval(uint16_t v, se::Value *ret) {
    ret->setInt16(v);
    return true;
}

bool uint16_to_seval(uint16_t v, se::Value *ret) {
    ret->setUint16(v);
    return true;
}

bool int8_to_seval(int8_t v, se::Value *ret) {
    ret->setInt8(v);
    return true;
}

bool uint8_to_seval(uint8_t v, se::Value *ret) {
    ret->setUint8(v);
    return true;
}

bool boolean_to_seval(bool v, se::Value *ret) {
    ret->setBoolean(v);
    return true;
}

bool float_to_seval(float v, se::Value *ret) {
    ret->setFloat(v);
    return true;
}

bool double_to_seval(double v, se::Value *ret) {
    ret->setDouble(v);
    return true;
}

bool long_to_seval(long v, se::Value *ret) {
    ret->setLong(v);
    return true;
}

bool ulong_to_seval(unsigned long v, se::Value *ret) {
    ret->setUlong(v);
    return true;
}

bool longlong_to_seval(long long v, se::Value *ret) {
    ret->setLong((long)v);
    return true;
}

bool uintptr_t_to_seval(uintptr_t v, se::Value *ret) {
    ret->setDouble(v);
    return true;
}

bool size_to_seval(size_t v, se::Value *ret) {
    ret->setSize(v);
    return true;
}

bool std_string_to_seval(const std::string &v, se::Value *ret) {
    ret->setString(v);
    return true;
}

bool Data_to_seval(const CrossApp::CAData &v, se::Value *ret) {
    assert(ret != nullptr);
    if (v.isNull()) {
        ret->setNull();
    } else {
        se::HandleObject obj(se::Object::createTypedArray(se::Object::TypedArrayType::UINT8, v.getBytes(), v.getLength()));
        ret->setObject(obj, true);
    }
    return true;
}

bool dpoint_to_seval(const CrossApp::DPoint &v, se::Value *ret) {
    assert(ret != nullptr);
    se::HandleObject obj(se::Object::createPlainObject());
    obj->setProperty("x", se::Value(v.x));
    obj->setProperty("y", se::Value(v.y));
    ret->setObject(obj);

    return true;
}

bool dpoint3d_to_seval(const CrossApp::DPoint3D &v, se::Value *ret) {
    assert(ret != nullptr);
    se::HandleObject obj(se::Object::createPlainObject());
    obj->setProperty("x", se::Value(v.x));
    obj->setProperty("y", se::Value(v.y));
    obj->setProperty("z", se::Value(v.z));
    ret->setObject(obj);

    return true;
}

bool matrix_to_seval(const CrossApp::Mat4 &v, se::Value *ret) {
    assert(ret != nullptr);
    se::HandleObject obj(se::Object::createArrayObject(16));

    for (uint8_t i = 0; i < 16; ++i) {
        obj->setArrayElement(i, se::Value(v.m[i]));
    }

    ret->setObject(obj);
    return true;
}

bool dsize_to_seval(const CrossApp::DSize &v, se::Value *ret) {
    assert(ret != nullptr);
    se::HandleObject obj(se::Object::createPlainObject());
    obj->setProperty("width", se::Value(v.width));
    obj->setProperty("height", se::Value(v.height));
    ret->setObject(obj);
    return true;
}

bool drect_to_seval(const CrossApp::DRect &v, se::Value *ret) {
    assert(ret != nullptr);
    se::HandleObject obj(se::Object::createPlainObject());
    obj->setProperty("x", se::Value(v.origin.x));
    obj->setProperty("y", se::Value(v.origin.y));
    obj->setProperty("width", se::Value(v.size.width));
    obj->setProperty("height", se::Value(v.size.height));
    ret->setObject(obj);

    return true;
}


bool cavector_to_seval(const CrossApp::CAVector<CAListViewCell *> &v,se::Value *ret){
    assert(ret != nullptr);
    se::HandleObject obj(se::Object::createArrayObject(v.size()));
    bool             ok = true;

    uint32_t i = 0;
    for (const auto &value : v) {
        se::Value val;
        ok = nativevalue_to_se(value, val, nullptr);
        if (!ok){
            ret->setUndefined();
            break;
        }
        if (!obj->setArrayElement(i, val)) {
            ok = false;
            ret->setUndefined();
            break;
        }
        ++i;
    }

    if (ok) {
        ret->setObject(obj);
    }

    return ok;
}

bool cavector_to_seval(const CrossApp::CAVector<CATabBarItem *> &v,se::Value *ret){
    assert(ret != nullptr);
    se::HandleObject obj(se::Object::createArrayObject(v.size()));
    bool             ok = true;

    uint32_t i = 0;
    for (const auto &value : v) {
        se::Value val;
        ok = nativevalue_to_se(value, val, nullptr);
        if (!ok){
            ret->setUndefined();
            break;
        }
        if (!obj->setArrayElement(i, val)) {
            ok = false;
            ret->setUndefined();
            break;
        }
        ++i;
    }

    if (ok) {
        ret->setObject(obj);
    }

    return ok;
}
bool cavector_to_seval(const CrossApp::CAVector<AnimationFrame *> &v,se::Value *ret){
    assert(ret != nullptr);
    se::HandleObject obj(se::Object::createArrayObject(v.size()));
    bool             ok = true;

    uint32_t i = 0;
    for (const auto &value : v) {
        se::Value val;
        ok = nativevalue_to_se(value, val, nullptr);
        if (!ok){
            ret->setUndefined();
            break;
        }
        if (!obj->setArrayElement(i, val)) {
            ok = false;
            ret->setUndefined();
            break;
        }
        ++i;
    }

    if (ok) {
        ret->setObject(obj);
    }

    return ok;
}
bool cavector_to_seval(const CrossApp::CAVector<CATableViewCell *> &v,se::Value *ret){
    assert(ret != nullptr);
    se::HandleObject obj(se::Object::createArrayObject(v.size()));
    bool             ok = true;

    uint32_t i = 0;
    for (const auto &value : v) {
        se::Value val;
        ok = nativevalue_to_se(value, val, nullptr);
        if (!ok){
            ret->setUndefined();
            break;
        }
        if (!obj->setArrayElement(i, val)) {
            ok = false;
            ret->setUndefined();
            break;
        }
        ++i;
    }

    if (ok) {
        ret->setObject(obj);
    }

    return ok;
}
bool cavector_to_seval(const CrossApp::CAVector<CAWaterfallViewCell *> &v,se::Value *ret){
    assert(ret != nullptr);
    se::HandleObject obj(se::Object::createArrayObject(v.size()));
    bool             ok = true;

    uint32_t i = 0;
    for (const auto &value : v) {
        se::Value val;
        ok = nativevalue_to_se(value, val, nullptr);
        if (!ok){
            ret->setUndefined();
            break;
        }
        if (!obj->setArrayElement(i, val)) {
            ok = false;
            ret->setUndefined();
            break;
        }
        ++i;
    }

    if (ok) {
        ret->setObject(obj);
    }

    return ok;
}
bool cavector_to_seval(const CrossApp::CAVector<CACollectionViewCell *> &v,se::Value *ret){
    assert(ret != nullptr);
    se::HandleObject obj(se::Object::createArrayObject(v.size()));
    bool             ok = true;

    uint32_t i = 0;
    for (const auto &value : v) {
        se::Value val;
        ok = nativevalue_to_se(value, val, nullptr);
        if (!ok){
            ret->setUndefined();
            break;
        }
        if (!obj->setArrayElement(i, val)) {
            ok = false;
            ret->setUndefined();
            break;
        }
        ++i;
    }

    if (ok) {
        ret->setObject(obj);
    }

    return ok;
}
bool cavector_to_seval(const CrossApp::CAVector<CAView *> &v,se::Value *ret){
    assert(ret != nullptr);
    se::HandleObject obj(se::Object::createArrayObject(v.size()));
    bool             ok = true;

    uint32_t i = 0;
    for (const auto &value : v) {
        se::Value val;
        ok = nativevalue_to_se(value, val, nullptr);
        if (!ok){
            ret->setUndefined();
            break;
        }
        if (!obj->setArrayElement(i, val)) {
            ok = false;
            ret->setUndefined();
            break;
        }
        ++i;
    }

    if (ok) {
        ret->setObject(obj);
    }

    return ok;
}
bool cavector_to_seval(const CrossApp::CAVector<CAImage *> &v,se::Value *ret){
    assert(ret != nullptr);
    se::HandleObject obj(se::Object::createArrayObject(v.size()));
    bool             ok = true;

    uint32_t i = 0;
    for (const auto &value : v) {
        se::Value val;
        ok = nativevalue_to_se(value, val, nullptr);
        if (!ok){
            ret->setUndefined();
            break;
        }
        if (!obj->setArrayElement(i, val)) {
            ok = false;
            ret->setUndefined();
            break;
        }
        ++i;
    }

    if (ok) {
        ret->setObject(obj);
    }

    return ok;
}
bool cavector_to_seval(const CrossApp::CAVector<CAObject *> &v,se::Value *ret){
    assert(ret != nullptr);
    se::HandleObject obj(se::Object::createArrayObject(v.size()));
    bool             ok = true;

    uint32_t i = 0;
    for (const auto &value : v) {
        se::Value val;
        ok = nativevalue_to_se(value, val, nullptr);
        if (!ok){
            ret->setUndefined();
            break;
        }
        if (!obj->setArrayElement(i, val)) {
            ok = false;
            ret->setUndefined();
            break;
        }
        ++i;
    }

    if (ok) {
        ret->setObject(obj);
    }

    return ok;
}
bool cavector_to_seval(const CrossApp::CAVector<CGSprite *> &v,se::Value *ret){
    assert(ret != nullptr);
    se::HandleObject obj(se::Object::createArrayObject(v.size()));
    bool             ok = true;

    uint32_t i = 0;
    for (const auto &value : v) {
        se::Value val;
        ok = nativevalue_to_se(value, val, nullptr);
        if (!ok){
            ret->setUndefined();
            break;
        }
        if (!obj->setArrayElement(i, val)) {
            ok = false;
            ret->setUndefined();
            break;
        }
        ++i;
    }

    if (ok) {
        ret->setObject(obj);
    }

    return ok;
}
bool cavector_to_seval(const CrossApp::CAVector<CATouch *> &v,se::Value *ret){
    assert(ret != nullptr);
    se::HandleObject obj(se::Object::createArrayObject(v.size()));
    bool             ok = true;

    uint32_t i = 0;
    for (const auto &value : v) {
        se::Value val;
        ok = nativevalue_to_se(value, val, nullptr);
        if (!ok){
            ret->setUndefined();
            break;
        }
        if (!obj->setArrayElement(i, val)) {
            ok = false;
            ret->setUndefined();
            break;
        }
        ++i;
    }

    if (ok) {
        ret->setObject(obj);
    }

    return ok;
}

bool dhorizontallayout_to_seval(const CrossApp::DHorizontalLayout &horlayout,se::Value *ret){
    se::HandleObject obj(se::Object::createPlainObject());
    obj->setProperty("left", se::Value(horlayout.left));
    obj->setProperty("right", se::Value(horlayout.right));
    obj->setProperty("width", se::Value(horlayout.width));
    obj->setProperty("center", se::Value(horlayout.center));
    obj->setProperty("normalizedWidth", se::Value(horlayout.normalizedWidth));
    ret->setObject(obj);
    return true;

}
bool dverticallayout_to_seval(const CrossApp::DVerticalLayout &verlayout,se::Value *ret){
    se::HandleObject obj(se::Object::createPlainObject());
    obj->setProperty("top", se::Value(verlayout.top));
    obj->setProperty("bottom", se::Value(verlayout.bottom));
    obj->setProperty("height", se::Value(verlayout.height));
    obj->setProperty("center", se::Value(verlayout.center));
    obj->setProperty("normalizedHeight", se::Value(verlayout.normalizedHeight));
    ret->setObject(obj);
    return true;
}

bool dlayout_to_seval(const CrossApp::DLayout &layout,se::Value *ret){
    assert(ret != nullptr);
    se::HandleObject obj(se::Object::createPlainObject());
    se::Value horlayout;
    se::Value verlayout;
    bool ok = true;
    ok &= dhorizontallayout_to_seval(layout.horizontal, &horlayout);
    ok &= dverticallayout_to_seval(layout.vertical, &verlayout);
    obj->setProperty("horizontal", horlayout);
    obj->setProperty("vertical", verlayout);
    if (ok){
        
        ret->setObject(obj);
        return true;
    }
    return false;
    
    
//    obj->setProperty("", <#const Value &value#>)
}

bool cacolor4b_to_seval(const CrossApp::CAColor4B &v,se::Value *ret){
    assert(ret != nullptr);
    se::HandleObject obj(se::Object::createPlainObject());
    obj->setProperty("r", se::Value(v.r));
    obj->setProperty("g", se::Value(v.g));
    obj->setProperty("b", se::Value(v.b));
    obj->setProperty("a", se::Value(v.a));
//    obj->setProperty("y", se::Value(v.origin.y));
//    obj->setProperty("width", se::Value(v.size.width));
//    obj->setProperty("height", se::Value(v.size.height));
    ret->setObject(obj);
    return true;
}

bool cafont_to_seval(const CrossApp::CAFont &v,se::Value *ret){
    assert(ret != nullptr);
    se::HandleObject obj(se::Object::createPlainObject());
    obj->setProperty("bold", se::Value(v.bold));
    obj->setProperty("fontSize", se::Value(v.fontSize));
    obj->setProperty("fontName", se::Value(v.fontName));
    obj->setProperty("underLine", se::Value(v.underLine));
    obj->setProperty("deleteLine", se::Value(v.deleteLine));
    obj->setProperty("italics", se::Value(v.italics));
    obj->setProperty("italicsValue", se::Value(v.italicsValue));
    obj->setProperty("wordWrap", se::Value(v.wordWrap));
    obj->setProperty("lineSpacing", se::Value(v.lineSpacing));
    se::Value color;
    cacolor4b_to_seval(v.color, &color);
    obj->setProperty("color", color);
    ret->setObject(obj);
    return true;
}

bool cavalue_to_seval(const CrossApp::CAValue &v, se::Value *ret) {
    assert(ret != nullptr);
    bool ok = true;
    switch (v.getType()) {
        case CrossApp::CAValue::Type::NONE:
            ret->setNull();
            break;
        case CrossApp::CAValue::Type::BOOLEAN:
            ret->setBoolean(v.asBool());
            break;
        case CrossApp::CAValue::Type::FLOAT:
        case CrossApp::CAValue::Type::DOUBLE:
            ret->setNumber(v.asDouble());
            break;
        case CrossApp::CAValue::Type::INTEGER:
            ret->setInt32(v.asInt());
            break;
        case CrossApp::CAValue::Type::STRING:
            ret->setString(v.asString());
            break;
        case CrossApp::CAValue::Type::VECTOR:
            ok = cavaluevector_to_seval(v.asValueVector(), ret);
            break;
        case CrossApp::CAValue::Type::MAP:
            ok = cavaluemap_to_seval(v.asValueMap(), ret);
            break;
        case CrossApp::CAValue::Type::INT_KEY_MAP:
            ok = cavaluemapintkey_to_seval(v.asIntKeyMap(), ret);
            break;
        default:
            SE_LOGE("Could not the way to convert cc::Value::Type (%d) type!", (int)v.getType());
            ok = false;
            break;
    }

    return ok;
}

bool cavaluemap_to_seval(const CrossApp::CAValueMap &v, se::Value *ret) {
    assert(ret != nullptr);
    se::HandleObject obj(se::Object::createPlainObject());
    bool             ok = true;
    for (const auto &e : v) {
        const std::string &key   = e.first;
        const CrossApp::CAValue &  value = e.second;

        if (key.empty()) {
            continue;
        }

        se::Value tmp;
        if (!cavalue_to_seval(value, &tmp)) {
            ok = false;
            ret->setUndefined();
            break;
        }

        obj->setProperty(key.c_str(), tmp);
    }
    if (ok) {
        ret->setObject(obj);
    }
    return ok;
}

bool cavaluemapintkey_to_seval(const CrossApp::CAValueMapIntKey &v, se::Value *ret) {
    assert(ret != nullptr);

    se::HandleObject obj(se::Object::createPlainObject());
    bool             ok = true;
    for (const auto &e : v) {
        std::stringstream keyss;
        keyss << e.first;
        std::string      key   = keyss.str();
        const CrossApp::CAValue &value = e.second;

        if (key.empty()) {
            continue;
        }

        se::Value tmp;
        if (!cavalue_to_seval(value, &tmp)) {
            ok = false;
            ret->setUndefined();
            break;
        }

        obj->setProperty(key.c_str(), tmp);
    }
    if (ok) {
        ret->setObject(obj);
    }

    return ok;
}

bool cavaluevector_to_seval(const CrossApp::CAValueVector &v, se::Value *ret) {
    assert(ret != nullptr);
    se::HandleObject obj(se::Object::createArrayObject(v.size()));
    bool             ok = true;

    uint32_t i = 0;
    for (const auto &value : v) {
        se::Value tmp;
        if (!cavalue_to_seval(value, &tmp)) {
            ok = false;
            ret->setUndefined();
            break;
        }

        obj->setArrayElement(i, tmp);
        ++i;
    }
    if (ok) {
        ret->setObject(obj);
    }

    return ok;
}


namespace {

template <typename T>
bool std_vector_T_to_seval(const std::vector<T> &v, se::Value *ret) {
    assert(ret != nullptr);
    se::HandleObject obj(se::Object::createArrayObject(v.size()));
    bool             ok = true;

    uint32_t i = 0;
    for (const auto &value : v) {
        if (!obj->setArrayElement(i, se::Value(value))) {
            ok = false;
            ret->setUndefined();
            break;
        }
        ++i;
    }

    if (ok) {
        ret->setObject(obj);
    }

    return ok;
}

} // namespace

bool std_vector_string_to_seval(const std::vector<std::string> &v, se::Value *ret) {
    return std_vector_T_to_seval(v, ret);
}

bool std_vector_int_to_seval(const std::vector<int> &v, se::Value *ret) {
    return std_vector_T_to_seval(v, ret);
}

bool std_vector_uint16_to_seval(const std::vector<uint16_t> &v, se::Value *ret) {
    return std_vector_T_to_seval(v, ret);
}

bool std_vector_float_to_seval(const std::vector<float> &v, se::Value *ret) {
    return std_vector_T_to_seval(v, ret);
}

bool std_map_string_string_to_seval(const std::map<std::string, std::string> &v, se::Value *ret) {
    assert(ret != nullptr);

    se::HandleObject obj(se::Object::createPlainObject());
    bool             ok = true;
    for (const auto &e : v) {
        const std::string &key   = e.first;
        const std::string &value = e.second;

        if (key.empty()) {
            continue;
        }

        se::Value tmp;
        if (!std_string_to_seval(value, &tmp)) {
            ok = false;
            ret->setUndefined();
            break;
        }

        obj->setProperty(key.c_str(), tmp);
    }

    if (ok) {
        ret->setObject(obj);
    }

    return ok;
}

namespace {
enum class DataType {
    INT,
    FLOAT
};

void toDpoint(void *data, DataType type, se::Value *ret) {
    auto *intptr                                   = static_cast<int32_t *>(data);
    auto *                                floatptr = static_cast<float *>(data);
    CrossApp::DPoint                             vec2;
    if (DataType::INT == type) {
        vec2.x = static_cast<float>(intptr[0]);
        vec2.y = static_cast<float>(intptr[1]);
    } else {
        vec2.x = *floatptr;
        vec2.y = *(floatptr + 1);
    }

    dpoint_to_seval(vec2, ret);
}

void toDpoint3D(void *data, DataType type, se::Value *ret) {
    auto *   intptr   = static_cast<int32_t *>(data);
    auto *   floatptr = static_cast<float *>(data);
    CrossApp::DPoint3D vec3;
    if (DataType::INT == type) {
        vec3.x = static_cast<float>(intptr[0]);
        vec3.y = static_cast<float>(intptr[1]);
        vec3.z = static_cast<float>(intptr[2]);
    } else {
        vec3.x = floatptr[0];
        vec3.y = floatptr[1];
        vec3.z = floatptr[2];
    }

    dpoint3d_to_seval(vec3, ret);
}

void toMat(float *data, int num, se::Value *ret) {
    se::HandleObject obj(se::Object::createPlainObject());

    char propName[4] = {0};
    for (int i = 0; i < num; ++i) {
        if (i < 10) {
            snprintf(propName, 3, "m0%d", i);
        }

        else {
            snprintf(propName, 3, "m%d", i);
        }

        obj->setProperty(propName, se::Value(*(data + i)));
    }
    ret->setObject(obj);
}
} // namespace

template <>
bool nativevalue_to_se(const CrossApp::CAValue &from, se::Value &to, se::Object *) {
    return cavalue_to_seval(from, &to);
}

template <class T>
bool nativevalue_to_se(const CrossApp::CAVector<T> &from, se::Value &to, se::Object *){
    
    return cavector_to_seval(from, &to);
}

template <>
bool nativevalue_to_se(const CrossApp::CAFont &from, se::Value &to, se::Object *){
    return cafont_to_seval(from, &to);
}

template <>
bool nativevalue_to_se(const CrossApp::CAColor4B &from, se::Value &to, se::Object *){
    return cacolor4b_to_seval(from, &to);
    //cafont_to_seval(from, &to);
}

template <>
bool nativevalue_to_se(const CrossApp::CAValueMap &from, se::Value &to, se::Object *) {
    return cavaluemap_to_seval(from, &to);
}

template <>
bool nativevalue_to_se(const CrossApp::DPoint &from, se::Value &to, se::Object *) {
    return dpoint_to_seval(from, &to);
}

template <>
bool nativevalue_to_se(const CrossApp::DPoint3D &from, se::Value &to, se::Object *) {
    return dpoint3d_to_seval(from, &to);
}

template <>
bool nativevalue_to_se(const CrossApp::DSize &from, se::Value &to, se::Object *) {
    return dsize_to_seval(from, &to);
}

template <>
bool nativevalue_to_se(const CrossApp::DRect &from, se::Value &to, se::Object *) {
    return drect_to_seval(from, &to);
}

template <>
bool nativevalue_to_se(const CrossApp::CAVector<CrossApp::CAListViewCell *> &from, se::Value &to, se::Object *){
    return cavector_to_seval(from, &to);
}

template <>
bool nativevalue_to_se(const CrossApp::CAVector<CrossApp::CATabBarItem *> &from, se::Value &to, se::Object *){
    return cavector_to_seval(from, &to);
}

template <>
bool nativevalue_to_se(const CrossApp::CAVector<CrossApp::CATableViewCell *> &from, se::Value &to, se::Object *){
    return cavector_to_seval(from, &to);
}

template <>
bool nativevalue_to_se(const CrossApp::CAVector<CrossApp::AnimationFrame *> &from, se::Value &to, se::Object *){
    return cavector_to_seval(from, &to);
}

template <>
bool nativevalue_to_se(const CrossApp::CAVector<CrossApp::CAWaterfallViewCell *> &from, se::Value &to, se::Object *){
    return cavector_to_seval(from, &to);
}

template <>
bool nativevalue_to_se(const CrossApp::CAVector<CrossApp::CACollectionViewCell *> &from, se::Value &to, se::Object *){
    return cavector_to_seval(from, &to);
}

template <>
bool nativevalue_to_se(const CrossApp::CAVector<CrossApp::CAView *> &from, se::Value &to, se::Object *){
    return cavector_to_seval(from, &to);
}

template <>
bool nativevalue_to_se(const CrossApp::CAVector<CrossApp::CAImage *> &from, se::Value &to, se::Object *){
    return cavector_to_seval(from, &to);
}

template <>
bool nativevalue_to_se(const CrossApp::CAVector<CrossApp::CATouch *> &from, se::Value &to, se::Object *){
    return cavector_to_seval(from, &to);
}

template <>
bool nativevalue_to_se(const CrossApp::CAVector<CrossApp::CAObject *> &from, se::Value &to, se::Object *){
    return cavector_to_seval(from, &to);
}

template <>
bool nativevalue_to_se(const CrossApp::CAVector<CrossApp::CGSprite *> &from, se::Value &to, se::Object *){
    return cavector_to_seval(from, &to);
}

template <>
bool nativevalue_to_se(const CrossApp::DLayout &from, se::Value &to, se::Object *){
    return dlayout_to_seval(from, &to);
}


#if USE_SPINE

template <>
bool sevalue_to_native(const se::Value &v, spine::Vector<spine::String> *ret, se::Object *) {
    assert(v.isObject());
    se::Object *obj = v.toObject();
    assert(obj->isArray());

    bool     ok  = true;
    uint32_t len = 0;
    ok           = obj->getArrayLength(&len);
    if (!ok) {
        ret->clear();
        return false;
    }

    se::Value tmp;
    for (uint32_t i = 0; i < len; ++i) {
        ok = obj->getArrayElement(i, &tmp);
        if (!ok || !tmp.isObject()) {
            ret->clear();
            return false;
        }

        const char *str = tmp.toString().c_str();
        ret->add(str);
    }

    return true;
}

template <>
bool nativevalue_to_se(const spine::String &obj, se::Value &val, se::Object *) {
    val.setString(obj.buffer());
    return true;
}

template <>
bool nativevalue_to_se(const spine::Vector<spine::String> &v, se::Value &ret, se::Object *) {
    se::HandleObject obj(se::Object::createArrayObject(v.size()));
    bool             ok = true;

    spine::Vector<spine::String> tmpv = v;
    for (uint32_t i = 0, count = (uint32_t)tmpv.size(); i < count; i++) {
        if (!obj->setArrayElement(i, se::Value(tmpv[i].buffer()))) {
            ok = false;
            ret.setUndefined();
            break;
        }
    }

    if (ok)
        ret.setObject(obj);

    return ok;
}

template <>
bool seval_to_Map_string_key(const se::Value &v, cc::Map<std::string, cc::middleware::Texture2D *> *ret) {
    assert(ret != nullptr);
    assert(v.isObject());
    se::Object *obj = v.toObject();

    std::vector<std::string> allKeys;
    bool                     ok = obj->getAllKeys(&allKeys);
    if (!ok) {
        ret->clear();
        return false;
    }

    se::Value tmp;
    for (const auto &key : allKeys) {
        auto pngPos = key.find(".png");
        if (pngPos == std::string::npos) {
            continue;
        }

        ok = obj->getProperty(key.c_str(), &tmp);
        if (!ok || !tmp.isObject()) {
            ret->clear();
            return false;
        }

        auto *nativeObj = static_cast<cc::middleware::Texture2D *>(tmp.toObject()->getPrivateData());
        ret->insert(key, nativeObj);
    }

    return true;
}



#endif
