/****************************************************************************
 Copyright (c) 2017-2021 Xiamen Yaji Software Co., Ltd.

 http://www.cocos.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated engine source code (the "Software"), a limited,
 worldwide, royalty-free, non-assignable, revocable and non-exclusive license
 to use Cocos Creator solely to develop games on your target platforms. You shall
 not use Cocos Creator software for developing other software or tools that's
 used for developing games. You are not granted to publish, distribute,
 sublicense, and/or sell copies of Cocos Creator.

 The software or tools in this License Agreement are licensed, not sold.
 Xiamen Yaji Software Co., Ltd. reserves all rights not expressly granted to you.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
****************************************************************************/

#include "jsb_crossapp_manual.h"
#include "../jswrapper/SeApi.h"
#include "jsb_conversions.h"
#include "../auto/jsb_crossapp_auto.h"

using namespace ca;

extern se::Object *__jsb_cc_FileUtils_proto;

static bool jsb_ccx_empty_func(se::State &s) {
    return true;
}
SE_BIND_FUNC(jsb_ccx_empty_func)

static bool JSB_localStorageGetItem(se::State &s) {
    const auto &args = s.args();
    size_t argc = args.size();
    if (argc == 1) {
        std::string ret_val;
        bool ok = true;
        std::string key;
        ok = seval_to_std_string(args[0], &key);
        SE_PRECONDITION2(ok, false, "Error processing arguments");
        std::string value;
        value = CrossApp::localStorageGetItem(key.c_str());
        if (ok)
            s.rval().setString(value);
        else
            s.rval().setNull(); // Should return null to make JSB behavior same as Browser since returning undefined will make JSON.parse(undefined) trigger exception.

        return true;
    }

    SE_REPORT_ERROR("Invalid number of arguments");
    return false;
}
SE_BIND_FUNC(JSB_localStorageGetItem)

static bool JSB_localStorageRemoveItem(se::State &s) {
    const auto &args = s.args();
    size_t argc = args.size();
    if (argc == 1) {
        bool ok = true;
        std::string key;
        ok = seval_to_std_string(args[0], &key);
        SE_PRECONDITION2(ok, false, "Error processing arguments");
        localStorageRemoveItem(key.c_str());
        return true;
    }

    SE_REPORT_ERROR("Invalid number of arguments");
    return false;
}
SE_BIND_FUNC(JSB_localStorageRemoveItem)

static bool JSB_localStorageSetItem(se::State &s) {
    const auto &args = s.args();
    size_t argc = args.size();
    if (argc == 2) {
        bool ok = true;
        std::string key;
        ok = seval_to_std_string(args[0], &key);
        SE_PRECONDITION2(ok, false, "Error processing arguments");

        std::string value;
        ok = seval_to_std_string(args[1], &value);
        SE_PRECONDITION2(ok, false, "Error processing arguments");
        localStorageSetItem(key.c_str(), value.c_str());
        return true;
    }

    SE_REPORT_ERROR("Invalid number of arguments");
    return false;
}
SE_BIND_FUNC(JSB_localStorageSetItem)

static bool JSB_localStorageClear(se::State &s) {
    const auto &args = s.args();
    size_t argc = args.size();
    if (argc == 0) {
        localStorageFree();
        return true;
    }

    SE_REPORT_ERROR("Invalid number of arguments");
    return false;
}
SE_BIND_FUNC(JSB_localStorageClear)



static bool js_engine_FileUtils_listFilesRecursively(se::State &s) {
    auto *cobj = (CrossApp::FileUtils *)s.nativeThisObject();
    SE_PRECONDITION2(cobj, false, "js_engine_FileUtils_listFilesRecursively : Invalid Native Object");
    const auto &args = s.args();
    size_t argc = args.size();
    CC_UNUSED bool ok = true;
    if (argc == 2) {
        std::string arg0;
        std::vector<std::string> arg1;
        ok &= seval_to_std_string(args[0], &arg0);
        SE_PRECONDITION2(ok, false, "js_engine_FileUtils_listFilesRecursively : Error processing arguments");
//        cobj->listFilesRecursively(arg0, &arg1);
        se::Object *list = args[1].toObject();
        SE_PRECONDITION2(args[1].isObject() && list->isArray(), false, "js_engine_FileUtils_listFilesRecursively : 2nd argument should be an Array");
        for (uint i = 0; i < static_cast<uint>(arg1.size()); i++) {
            list->setArrayElement(i, se::Value(arg1[i]));
        }
        list->setProperty("length", se::Value(static_cast<uint>(arg1.size())));
        return true;
    }
    SE_REPORT_ERROR("wrong number of arguments: %d, was expecting %d", (int)argc, 2);
    return false;
}
SE_BIND_FUNC(js_engine_FileUtils_listFilesRecursively)

static bool js_se_setExceptionCallback(se::State &s) {
    auto &args = s.args();
    if (args.size() != 1 || !args[0].isObject() || !args[0].toObject()->isFunction()) {
        SE_REPORT_ERROR("expect 1 arguments of Function type, %d provided", (int)args.size());
        return false;
    }

    se::Object *objFunc = args[0].toObject();
    // se::Value::reset will invoke decRef() while destroying s.args()
    // increase ref here
    objFunc->incRef();
    if (s.thisObject()) {
        s.thisObject()->attachObject(objFunc); // prevent GC
    } else {
        //prevent GC in C++ & JS
        objFunc->root();
    }

    se::ScriptEngine::getInstance()->setJSExceptionCallback([objFunc](const char *location, const char *message, const char *stack) {
        se::ValueArray jsArgs;
        jsArgs.resize(3);
        jsArgs[0] = se::Value(location);
        jsArgs[1] = se::Value(message);
        jsArgs[2] = se::Value(stack);
        objFunc->call(jsArgs, nullptr);
    });
    return true;
}
SE_BIND_FUNC(js_se_setExceptionCallback)

static bool register_filetuils_ext(se::Object *obj) {
    __jsb_CrossApp_FileUtils_proto->defineFunction("listFilesRecursively", _SE(js_engine_FileUtils_listFilesRecursively));
    return true;
}

static bool register_se_setExceptionCallback(se::Object *obj) {
    se::Value jsb;
    if (!obj->getProperty("jsb", &jsb)) {
        jsb.setObject(se::Object::createPlainObject());
        obj->setProperty("jsb", jsb);
    }
    auto *jsbObj = jsb.toObject();
    jsbObj->defineFunction("onError", _SE(js_se_setExceptionCallback));

    return true;
}


bool js_crossapp_release(se::State &s){
    
    auto &args = s.args();
    se::Object *objFunc = args[0].toObject();
    CrossApp::CAObject* cobj = (CrossApp::CAObject *)objFunc;
    if (cobj == NULL){
        SE_PRECONDITION2(false, false, "type not supported!");
    }
    cobj->release();
    return true;
}
SE_BIND_FUNC(js_crossapp_release)


bool js_crossapp_CAMotionManager_startGyroscope(se::State &s){
    auto &args = s.args();
    bool ok = true;
    size_t argc = args.size();
    CrossApp::CAMotionManager* cobj = (CrossApp::CAMotionManager *)s.nativeThisObject();
    SE_PRECONDITION2(cobj, false,
                     "js_crossapp_CAMotionManager_startGyroscope : Invalid Native Object");
    if (argc == 1) {
        std::function<void (const CrossApp::CAMotionManager::Data &)> arg0;
        se::Value jsThis(s.thisObject());
        se::Value jsFunc(args[0]);
        do {
            if (args[0].isObject() && args[0].toObject()->isFunction()){
                auto lambda = [=, &ok](const CrossApp::CAMotionManager::Data & data) -> void {

                    se::Value rval;
                    CAValueMap larg0;
                    larg0["x"] = data.x;
                    larg0["y"] = data.y;
                    larg0["z"] = data.z;
                    larg0["timestamp"] = data.timestamp;
                    se::ValueArray args;
                    args.resize(1);
                    ok &= cavaluemap_to_seval(larg0, &args[0]);
                    se::Object* thisObj = jsThis.isObject() ? jsThis.toObject() : nullptr;
                    se::Object* funcObj = jsFunc.toObject();
                    bool succeed = funcObj->call(args, thisObj,&rval);
                    if (!succeed){
                        se::ScriptEngine::getInstance()->clearException();
                    }
                };
                arg0 = lambda;
            }else{
                arg0 = nullptr;
            }
        }while(0);
        SE_PRECONDITION2(ok,false,"js_crossapp_CAMotionManager_startGyroscope : Error processing arguments");
        cobj->startGyroscope(arg0);
        return true;
    }
    return false;
}
SE_BIND_FUNC(js_crossapp_CAMotionManager_startGyroscope)

static bool js_crossapp_CANotificationCenter_addObserver(se::State& s)
{
    CrossApp::CANotificationCenter* cobj = SE_THIS_OBJECT<CrossApp::CANotificationCenter>(s);
    SE_PRECONDITION2(cobj, false, "js_crossapp_CANotificationCenter_addObserver : Invalid Native Object");
    const auto& args = s.args();
    size_t argc = args.size();
    CC_UNUSED bool ok = true;
    if (argc == 3) {
        HolderType<std::function<void (CrossApp::CAObject *)>, true> arg0 = {};
        HolderType<CrossApp::CAObject*, false> arg1 = {};
        HolderType<std::string, true> arg2 = {};
        do {
            if (args[0].isObject() && args[0].toObject()->isFunction())
            {
                se::Value jsThis(s.thisObject());
                se::Value jsFunc(args[0]);
                jsThis.toObject()->attachObject(jsFunc.toObject());
                auto lambda = [=](CAObject* larg0) -> void {
                    se::ScriptEngine::getInstance()->clearException();
                    se::AutoHandleScope hs;
                    
                    if(larg0 == NULL){
                        se::ValueArray args;
                        args.resize(0);
                        se::Value rval;
                        se::Object* thisObj = jsThis.isObject() ? jsThis.toObject() : nullptr;
                        se::Object* funcObj = jsFunc.toObject();
                        bool succeed = funcObj->call(args, thisObj, &rval);
                        if (!succeed) {
                            se::ScriptEngine::getInstance()->clearException();
                        }
                        return;
                    }
                    CC_UNUSED bool ok = true;
                    se::ValueArray args;
                    args.resize(1);
                    CAValue *value = (CAValue *)larg0;
                    ok &= nativevalue_to_se(larg0, args[0], nullptr /*ctx*/);
                    se::Value rval;
                    se::Object* thisObj = jsThis.isObject() ? jsThis.toObject() : nullptr;
                    se::Object* funcObj = jsFunc.toObject();
                    bool succeed = funcObj->call(args, thisObj, &rval);
                    if (!succeed) {
                        se::ScriptEngine::getInstance()->clearException();
                    }
                };
                arg0.data = lambda;
            }
            else
            {
                arg0.data = nullptr;
            }
        } while(false)
        ;
        ok &= sevalue_to_native(args[1], &arg1, s.thisObject());
        ok &= sevalue_to_native(args[2], &arg2, s.thisObject());
        SE_PRECONDITION2(ok, false, "js_crossapp_CANotificationCenter_addObserver : Error processing arguments");
        cobj->addObserver(arg0.value(), arg1.value(), arg2.value());
        return true;
    }
    SE_REPORT_ERROR("wrong number of arguments: %d, was expecting %d", (int)argc, 3);
    return false;
}
SE_BIND_FUNC(js_crossapp_CANotificationCenter_addObserver)


//bool js_crossapp_CANotificationCenter_addObserver(se::State &s){
//    auto &args = s.args();
//    bool ok = true;
//    size_t argc = args.size();
//    CrossApp::CANotificationCenter* cobj = (CrossApp::CANotificationCenter *)s.nativeThisObject();
//    SE_PRECONDITION2(cobj, false,
//                     "js_crossapp_CANotificationCenter_addObserver : Invalid Native Object");
//    if (argc == 3){
//        std::function<void (CrossApp::CAObject *)> arg0;
//        CrossApp::CAObject* arg1 = nullptr;
//        std::string arg2;
//        se::Value jsThis(s.thisObject());
//        se::Value jsFunc(args[0]);
//        do {
//            if (args[0].isObject() && args[0].toObject()->isFunction()){
//                auto lambda = [=, &ok](CrossApp::CAObject* larg0) -> void {
//                    se::Value rval;
//                    do {
//                        if (larg0) {
//                            if (CAValue* value = dynamic_cast<CAValue*>(larg0)) {
//                                ok &= cavalue_to_seval(*value, &rval);
//                                if (!ok) { ok = true; break; }
//                            } else {
//                                rval = se::Value(larg0);
//                            }
//                        } else {
//                            rval = se::Value::Null;
//                        }
//                    } while (0);
//                    se::Object* thisObj = jsThis.isObject() ? jsThis.toObject() : nullptr;
//                    se::Object* funcObj = jsFunc.toObject();
//                    bool succeed = funcObj->call(args, thisObj,&rval);
//                    if (!succeed){
//                        se::ScriptEngine::getInstance()->clearException();
//                    }
//                };
//                arg0 = lambda;
//            }else{
//                arg0 = nullptr;
//            }
//        } while (0);
//        do {
//            if (args[1].isNull()){arg1 = nullptr;break;}
//            if (!args[1].isObject()) { ok = false; break; }
//            ok &= seval_to_native_ptr(args[1], &arg1);
//            if (!ok) { ok = true; break; }
//        } while (0);
//        ok &= seval_to_std_string(args[2], &arg2);
//        SE_PRECONDITION2(ok,false,"js_crossapp_CANotificationCenter_addObserver : Error processing arguments");
//        cobj->addObserver(arg0, arg1, arg2);
//        return true;
//    }
//    return false;
//}
//SE_BIND_FUNC(js_crossapp_CANotificationCenter_addObserver)

bool js_crossapp_CADatePickerView_onSelectRow(se::State &s){
    CrossApp::CADatePickerView* cobj = (CrossApp::CADatePickerView *)s.nativeThisObject();
    SE_PRECONDITION2(cobj, false,
                     "js_crossapp_CADatePickerView_onSelectRow : Invalid Native Object");
    auto &args = s.args();
    bool ok = true;
    if (args.size() != 1 || !args[0].isObject() || !args[0].toObject()->isFunction()) {
        SE_REPORT_ERROR("expect 1 arguments of Function type, %d provided", (int)args.size());
        return false;
    }
    std::function<void (const tm &)> arg0;
    se::Value jsThis(s.thisObject());
    se::Value jsFunc(args[0]);
    jsThis.toObject()->attachObject(jsFunc.toObject());
    do {
        auto lambda = [=, &ok](const tm & var) -> void {
            se::ScriptEngine::getInstance()->clearException();
            se::AutoHandleScope hs;
            CAValueMap larg0;
            se::Value rval;
            larg0["tm_sec"] = var.tm_sec;
            larg0["tm_min"] = var.tm_min;
            larg0["tm_hour"] = var.tm_hour;
            larg0["tm_mday"] = var.tm_mday;
            larg0["tm_mon"] = var.tm_mon;
            larg0["tm_year"] = var.tm_year;
            larg0["tm_wday"] = var.tm_wday;
            larg0["tm_yday"] = var.tm_yday;
            larg0["tm_isdst"] = var.tm_isdst;
            se::ValueArray args;
            args.resize(1);
            ok &= cavaluemap_to_seval(larg0, &args[0]);
            se::Object* thisObj = jsThis.isObject() ? jsThis.toObject() : nullptr;
            se::Object* funcObj = jsFunc.toObject();
            bool succeed = funcObj->call(args, thisObj,&rval);
            if (!succeed){
                se::ScriptEngine::getInstance()->clearException();
            }
        };
        arg0 = lambda;
    } while(0);
    SE_PRECONDITION2(ok,false,"js_crossapp_CADatePickerView_onSelectRow : Error processing arguments");
    cobj->onSelectRow(arg0);
    return true;
    
}
SE_BIND_FUNC(js_crossapp_CADatePickerView_onSelectRow)

bool js_crossapp_CAAddressBook_getAddressBook(se::State &s){
    CrossApp::CAAddressBook* cobj = (CrossApp::CAAddressBook *)s.nativeThisObject();
    SE_PRECONDITION2(cobj, false,
                     "js_crossapp_CAAddressBook_getAddressBook : Invalid Native Object");
    auto &args = s.args();
    bool ok = true;
    size_t argc = args.size();
    if (argc == 1) {
        std::function<void (const std::vector<CrossApp::CAAddressBook::Data, std::allocator<CrossApp::CAAddressBook::Data> > &)> arg0;
        se::Value jsThis(s.thisObject());
        se::Value jsFunc(args[0]);
        do {
            if (args[0].isObject() && args[0].toObject()->isFunction()){
                auto lambda = [=, &ok](const std::vector<CAAddressBook::Data>& vec) -> void {
                    se::ScriptEngine::getInstance()->clearException();
                    se::AutoHandleScope hs;
                    se::Value rval;
                    CAValueVector largVec;
                    for (auto& data : vec)
                    {
                        CAValueMap larg0;
                        larg0["firstName"]     = CAValue(data.firstName);
                        larg0["lastName"]     = CAValue(data.lastName);
                        larg0["middleName"]     = CAValue(data.middleName);
                        larg0["prefix"]     = CAValue(data.prefix);
                        larg0["suffix"]     = CAValue(data.suffix);
                        larg0["nickname"]     = CAValue(data.nickname);
                        larg0["firstNamePhonetic"]     = CAValue(data.firstNamePhonetic);
                        larg0["lastNamePhonetic"]     = CAValue(data.lastNamePhonetic);
                        larg0["middleNamePhonetic"]     = CAValue(data.middleNamePhonetic);
                        larg0["organization"]     = CAValue(data.organization);
                        larg0["jobtitle"]     = CAValue(data.jobtitle);
                        larg0["department"]     = CAValue(data.department);
                        larg0["birthday"]     = CAValue(data.birthday);
                        larg0["note"]     = CAValue(data.note);
                        larg0["lastEdit"]     = CAValue(data.lastEdit);
                        larg0["email"]     = CAValue(data.email);
                        larg0["country"]     = CAValue(data.country);
                        larg0["city"]     = CAValue(data.city);
                        larg0["province"]     = CAValue(data.province);
                        larg0["street"]     = CAValue(data.street);
                        larg0["zip"]     = CAValue(data.zip);
                        larg0["countrycode"]     = CAValue(data.countrycode);
                        larg0["phoneNumber"]     = CAValue(data.phoneNumber);
                        larg0["fullname"]     = CAValue(data.fullname);
                        larg0["firstLetter"]     = CAValue(data.firstLetter);
                        largVec.push_back(CAValue(larg0));
                    }
                    se::ValueArray args;
                    args.resize(1);
                    ok &= cavaluevector_to_seval(largVec, &args[0]);
                    se::Object* thisObj = jsThis.isObject() ? jsThis.toObject() : nullptr;
                    se::Object* funcObj = jsFunc.toObject();
                    bool succeed = funcObj->call(args, thisObj,&rval);
                    if (!succeed){
                        se::ScriptEngine::getInstance()->clearException();
                    }
                };
                arg0 = lambda;
            }else{
                arg0 = nullptr;
            }
        } while (0);
        SE_PRECONDITION2(ok,false,"js_crossapp_CAAddressBook_getAddressBook : Error processing arguments");
        cobj->getAddressBook(arg0);
        return true;
    }
    return false;
}
SE_BIND_FUNC(js_crossapp_CAAddressBook_getAddressBook)


static bool js_crossapp_CAScheduler_isScheduled(se::State& s)
{
    CC_UNUSED bool ok = true;
    CrossApp::CAScheduler* cobj = SE_THIS_OBJECT<CrossApp::CAScheduler>(s);
    SE_PRECONDITION2( cobj, false, "js_crossapp_CAScheduler_isScheduled : Invalid Native Object");
    const auto& args = s.args();
    size_t argc = args.size();
    do {
        if (argc == 2) {
            HolderType<std::string, true> arg0 = {};
            HolderType<CrossApp::CAObject*, false> arg1 = {};

            ok &= sevalue_to_native(args[0], &arg0, s.thisObject());
            if (!ok) { ok = true; break; }
            ok &= sevalue_to_native(args[1], &arg1, s.thisObject());
            if (!ok) { ok = true; break; }
            bool result = cobj->isScheduled(arg0.value(), arg1.value());
            ok &= nativevalue_to_se(result, s.rval(), s.thisObject() /*ctx*/);
            SE_PRECONDITION2(ok, false, "js_crossapp_CAScheduler_isScheduled : Error processing arguments");
            SE_HOLD_RETURN_VALUE(result, s.thisObject(), s.rval());
            return true;
        }
    } while(false);

    SE_REPORT_ERROR("wrong number of arguments: %d", (int)argc);
    return false;
}
SE_BIND_FUNC(js_crossapp_CAScheduler_isScheduled)

static bool js_crossapp_CAScheduler_schedule(se::State& s)
{
    CC_UNUSED bool ok = true;
    CrossApp::CAScheduler* cobj = SE_THIS_OBJECT<CrossApp::CAScheduler>(s);
    SE_PRECONDITION2( cobj, false, "js_crossapp_CAScheduler_schedule : Invalid Native Object");
    const auto& args = s.args();
    size_t argc = args.size();
    do {
        if (argc == 4) {
            HolderType<std::function<void (float)>, true> arg0 = {};
            HolderType<std::string, true> arg1 = {};
            HolderType<CrossApp::CAObject*, false> arg2 = {};
            HolderType<float, false> arg3 = {};

            do {
                if (args[0].isObject() && args[0].toObject()->isFunction())
                {
                    se::Value jsThis(s.thisObject());
                    se::Value jsFunc(args[0]);
                    jsThis.toObject()->attachObject(jsFunc.toObject());
                    auto lambda = [=](float larg0) -> void {
                        se::ScriptEngine::getInstance()->clearException();
                        se::AutoHandleScope hs;
            
                        CC_UNUSED bool ok = true;
                        se::ValueArray args;
                        args.resize(1);
                        ok &= nativevalue_to_se(larg0, args[0], nullptr /*ctx*/);
                        se::Value rval;
                        se::Object* thisObj = jsThis.isObject() ? jsThis.toObject() : nullptr;
                        se::Object* funcObj = jsFunc.toObject();
                        bool succeed = funcObj->call(args, thisObj, &rval);
                        if (!succeed) {
                            se::ScriptEngine::getInstance()->clearException();
                        }
                    };
                    arg0.data = lambda;
                }
                else
                {
                    arg0.data = nullptr;
                }
            } while(false)
            ;
            if (!ok) { ok = true; break; }
            ok &= sevalue_to_native(args[1], &arg1, s.thisObject());
            if (!ok) { ok = true; break; }
            ok &= sevalue_to_native(args[2], &arg2, s.thisObject());
            if (!ok) { ok = true; break; }
            ok &= sevalue_to_native(args[3], &arg3, s.thisObject());
            if (!ok) { ok = true; break; }
            cobj->schedule(arg0.value(), arg1.value(), arg2.value(), arg3.value());
            return true;
        }
    } while(false);

    do {
        if (argc == 5) {
            HolderType<std::function<void (float)>, true> arg0 = {};
            HolderType<std::string, true> arg1 = {};
            HolderType<CrossApp::CAObject*, false> arg2 = {};
            HolderType<float, false> arg3 = {};
            HolderType<bool, false> arg4 = {};

            do {
                if (args[0].isObject() && args[0].toObject()->isFunction())
                {
                    se::Value jsThis(s.thisObject());
                    se::Value jsFunc(args[0]);
                    jsThis.toObject()->attachObject(jsFunc.toObject());
                    auto lambda = [=](float larg0) -> void {
                        se::ScriptEngine::getInstance()->clearException();
                        se::AutoHandleScope hs;
            
                        CC_UNUSED bool ok = true;
                        se::ValueArray args;
                        args.resize(1);
                        ok &= nativevalue_to_se(larg0, args[0], nullptr /*ctx*/);
                        se::Value rval;
                        se::Object* thisObj = jsThis.isObject() ? jsThis.toObject() : nullptr;
                        se::Object* funcObj = jsFunc.toObject();
                        bool succeed = funcObj->call(args, thisObj, &rval);
                        if (!succeed) {
                            se::ScriptEngine::getInstance()->clearException();
                        }
                    };
                    arg0.data = lambda;
                }
                else
                {
                    arg0.data = nullptr;
                }
            } while(false)
            ;
            if (!ok) { ok = true; break; }
            ok &= sevalue_to_native(args[1], &arg1, s.thisObject());
            if (!ok) { ok = true; break; }
            ok &= sevalue_to_native(args[2], &arg2, s.thisObject());
            if (!ok) { ok = true; break; }
            ok &= sevalue_to_native(args[3], &arg3, s.thisObject());
            if (!ok) { ok = true; break; }
            ok &= sevalue_to_native(args[4], &arg4, s.thisObject());
            cobj->schedule(arg0.value(), arg1.value(), arg2.value(), arg3.value(), arg4.value());
            return true;
        }
    } while(false);

    do {
        if (argc == 6) {
            HolderType<std::function<void (float)>, true> arg0 = {};
            HolderType<std::string, true> arg1 = {};
            HolderType<CrossApp::CAObject*, false> arg2 = {};
            HolderType<float, false> arg3 = {};
            HolderType<unsigned int, false> arg4 = {};
            HolderType<float, false> arg5 = {};

            do {
                if (args[0].isObject() && args[0].toObject()->isFunction())
                {
                    se::Value jsThis(s.thisObject());
                    se::Value jsFunc(args[0]);
                    jsThis.toObject()->attachObject(jsFunc.toObject());
                    auto lambda = [=](float larg0) -> void {
                        se::ScriptEngine::getInstance()->clearException();
                        se::AutoHandleScope hs;
            
                        CC_UNUSED bool ok = true;
                        se::ValueArray args;
                        args.resize(1);
                        ok &= nativevalue_to_se(larg0, args[0], nullptr /*ctx*/);
                        se::Value rval;
                        se::Object* thisObj = jsThis.isObject() ? jsThis.toObject() : nullptr;
                        se::Object* funcObj = jsFunc.toObject();
                        bool succeed = funcObj->call(args, thisObj, &rval);
                        if (!succeed) {
                            se::ScriptEngine::getInstance()->clearException();
                        }
                    };
                    arg0.data = lambda;
                }
                else
                {
                    arg0.data = nullptr;
                }
            } while(false)
            ;
            if (!ok) { ok = true; break; }
            ok &= sevalue_to_native(args[1], &arg1, s.thisObject());
            if (!ok) { ok = true; break; }
            ok &= sevalue_to_native(args[2], &arg2, s.thisObject());
            if (!ok) { ok = true; break; }
            ok &= sevalue_to_native(args[3], &arg3, s.thisObject());
            if (!ok) { ok = true; break; }
            ok &= sevalue_to_native(args[4], &arg4, s.thisObject());
            if (!ok) { ok = true; break; }
            ok &= sevalue_to_native(args[5], &arg5, s.thisObject());
            if (!ok) { ok = true; break; }
            cobj->schedule(arg0.value(), arg1.value(), arg2.value(), arg3.value(), arg4.value(), arg5.value());
            return true;
        }
    } while(false);

    do {
        if (argc == 7) {
            HolderType<std::function<void (float)>, true> arg0 = {};
            HolderType<std::string, true> arg1 = {};
            HolderType<CrossApp::CAObject*, false> arg2 = {};
            HolderType<float, false> arg3 = {};
            HolderType<unsigned int, false> arg4 = {};
            HolderType<float, false> arg5 = {};
            HolderType<bool, false> arg6 = {};

            do {
                if (args[0].isObject() && args[0].toObject()->isFunction())
                {
                    se::Value jsThis(s.thisObject());
                    se::Value jsFunc(args[0]);
                    jsThis.toObject()->attachObject(jsFunc.toObject());
                    auto lambda = [=](float larg0) -> void {
                        se::ScriptEngine::getInstance()->clearException();
                        se::AutoHandleScope hs;
            
                        CC_UNUSED bool ok = true;
                        se::ValueArray args;
                        args.resize(1);
                        ok &= nativevalue_to_se(larg0, args[0], nullptr /*ctx*/);
                        se::Value rval;
                        se::Object* thisObj = jsThis.isObject() ? jsThis.toObject() : nullptr;
                        se::Object* funcObj = jsFunc.toObject();
                        bool succeed = funcObj->call(args, thisObj, &rval);
                        if (!succeed) {
                            se::ScriptEngine::getInstance()->clearException();
                        }
                    };
                    arg0.data = lambda;
                }
                else
                {
                    arg0.data = nullptr;
                }
            } while(false)
            ;
            if (!ok) { ok = true; break; }
            ok &= sevalue_to_native(args[1], &arg1, s.thisObject());
            if (!ok) { ok = true; break; }
            ok &= sevalue_to_native(args[2], &arg2, s.thisObject());
            if (!ok) { ok = true; break; }
            ok &= sevalue_to_native(args[3], &arg3, s.thisObject());
            if (!ok) { ok = true; break; }
            ok &= sevalue_to_native(args[4], &arg4, s.thisObject());
            if (!ok) { ok = true; break; }
            ok &= sevalue_to_native(args[5], &arg5, s.thisObject());
            if (!ok) { ok = true; break; }
            ok &= sevalue_to_native(args[6], &arg6, s.thisObject());
            cobj->schedule(arg0.value(), arg1.value(), arg2.value(), arg3.value(), arg4.value(), arg5.value(), arg6.value());
            return true;
        }
    } while(false);

    SE_REPORT_ERROR("wrong number of arguments: %d", (int)argc);
    return false;
}
SE_BIND_FUNC(js_crossapp_CAScheduler_schedule)

static bool js_crossapp_CAScheduler_scheduleOnce(se::State& s)
{
    CC_UNUSED bool ok = true;
    CrossApp::CAScheduler* cobj = SE_THIS_OBJECT<CrossApp::CAScheduler>(s);
    SE_PRECONDITION2( cobj, false, "js_crossapp_CAScheduler_scheduleOnce : Invalid Native Object");
    const auto& args = s.args();
    size_t argc = args.size();
    do {
        if (argc == 4) {
            HolderType<std::function<void (float)>, true> arg0 = {};
            HolderType<std::string, true> arg1 = {};
            HolderType<CrossApp::CAObject*, false> arg2 = {};
            HolderType<float, false> arg3 = {};

            do {
                if (args[0].isObject() && args[0].toObject()->isFunction())
                {
                    se::Value jsThis(s.thisObject());
                    se::Value jsFunc(args[0]);
                    jsThis.toObject()->attachObject(jsFunc.toObject());
                    auto lambda = [=](float larg0) -> void {
                        se::ScriptEngine::getInstance()->clearException();
                        se::AutoHandleScope hs;
            
                        CC_UNUSED bool ok = true;
                        se::ValueArray args;
                        args.resize(1);
                        ok &= nativevalue_to_se(larg0, args[0], nullptr /*ctx*/);
                        se::Value rval;
                        se::Object* thisObj = jsThis.isObject() ? jsThis.toObject() : nullptr;
                        se::Object* funcObj = jsFunc.toObject();
                        bool succeed = funcObj->call(args, thisObj, &rval);
                        if (!succeed) {
                            se::ScriptEngine::getInstance()->clearException();
                        }
                    };
                    arg0.data = lambda;
                }
                else
                {
                    arg0.data = nullptr;
                }
            } while(false)
            ;
            if (!ok) { ok = true; break; }
            ok &= sevalue_to_native(args[1], &arg1, s.thisObject());
            if (!ok) { ok = true; break; }
            ok &= sevalue_to_native(args[2], &arg2, s.thisObject());
            if (!ok) { ok = true; break; }
            ok &= sevalue_to_native(args[3], &arg3, s.thisObject());
            if (!ok) { ok = true; break; }
            cobj->scheduleOnce(arg0.value(), arg1.value(), arg2.value(), arg3.value());
            return true;
        }
    } while(false);

    do {
        if (argc == 5) {
            HolderType<std::function<void (float)>, true> arg0 = {};
            HolderType<std::string, true> arg1 = {};
            HolderType<CrossApp::CAObject*, false> arg2 = {};
            HolderType<float, false> arg3 = {};
            HolderType<bool, false> arg4 = {};

            do {
                if (args[0].isObject() && args[0].toObject()->isFunction())
                {
                    se::Value jsThis(s.thisObject());
                    se::Value jsFunc(args[0]);
                    jsThis.toObject()->attachObject(jsFunc.toObject());
                    auto lambda = [=](float larg0) -> void {
                        se::ScriptEngine::getInstance()->clearException();
                        se::AutoHandleScope hs;
            
                        CC_UNUSED bool ok = true;
                        se::ValueArray args;
                        args.resize(1);
                        ok &= nativevalue_to_se(larg0, args[0], nullptr /*ctx*/);
                        se::Value rval;
                        se::Object* thisObj = jsThis.isObject() ? jsThis.toObject() : nullptr;
                        se::Object* funcObj = jsFunc.toObject();
                        bool succeed = funcObj->call(args, thisObj, &rval);
                        if (!succeed) {
                            se::ScriptEngine::getInstance()->clearException();
                        }
                    };
                    arg0.data = lambda;
                }
                else
                {
                    arg0.data = nullptr;
                }
            } while(false)
            ;
            if (!ok) { ok = true; break; }
            ok &= sevalue_to_native(args[1], &arg1, s.thisObject());
            if (!ok) { ok = true; break; }
            ok &= sevalue_to_native(args[2], &arg2, s.thisObject());
            if (!ok) { ok = true; break; }
            ok &= sevalue_to_native(args[3], &arg3, s.thisObject());
            if (!ok) { ok = true; break; }
            ok &= sevalue_to_native(args[4], &arg4, s.thisObject());
            cobj->scheduleOnce(arg0.value(), arg1.value(), arg2.value(), arg3.value(), arg4.value());
            return true;
        }
    } while(false);

    SE_REPORT_ERROR("wrong number of arguments: %d", (int)argc);
    return false;
}
SE_BIND_FUNC(js_crossapp_CAScheduler_scheduleOnce)

static bool js_crossapp_CAScheduler_unschedule(se::State& s)
{
    CC_UNUSED bool ok = true;
    CrossApp::CAScheduler* cobj = SE_THIS_OBJECT<CrossApp::CAScheduler>(s);
    SE_PRECONDITION2( cobj, false, "js_crossapp_CAScheduler_unschedule : Invalid Native Object");
    const auto& args = s.args();
    size_t argc = args.size();
    do {
        if (argc == 2) {
            HolderType<std::string, true> arg0 = {};
            HolderType<CrossApp::CAObject*, false> arg1 = {};

            ok &= sevalue_to_native(args[0], &arg0, s.thisObject());
            if (!ok) { ok = true; break; }
            ok &= sevalue_to_native(args[1], &arg1, s.thisObject());
            if (!ok) { ok = true; break; }
            cobj->unschedule(arg0.value(), arg1.value());
            return true;
        }
    } while(false);

    SE_REPORT_ERROR("wrong number of arguments: %d", (int)argc);
    return false;
}
SE_BIND_FUNC(js_crossapp_CAScheduler_unschedule)


static bool js_crossapp_CAListView_onCellAtIndexPath(se::State& s)
{
    CrossApp::CAListView* cobj = SE_THIS_OBJECT<CrossApp::CAListView>(s);
    SE_PRECONDITION2(cobj, false, "js_crossapp_CAListView_onCellAtIndexPath : Invalid Native Object");
    const auto& args = s.args();
    size_t argc = args.size();
    CC_UNUSED bool ok = true;
    if (argc == 1) {
        HolderType<std::function<CrossApp::CAListViewCell * (CrossApp::DSize, unsigned int)>, true> arg0 = {};
        do {
            if (args[0].isObject() && args[0].toObject()->isFunction())
            {
                se::Value jsThis(s.thisObject());
                se::Value jsFunc(args[0]);
                jsThis.toObject()->attachObject(jsFunc.toObject());
                auto lambda = [=](CrossApp::DSize larg0, unsigned int larg1) -> CAListViewCell *{
                    se::ScriptEngine::getInstance()->clearException();
                    se::AutoHandleScope hs;
        
                    CC_UNUSED bool ok = true;
                    se::ValueArray args;
                    args.resize(2);
                    ok &= nativevalue_to_se(larg0, args[0], nullptr /*ctx*/);
                    ok &= nativevalue_to_se(larg1, args[1], nullptr /*ctx*/);
                    se::Value rval;
                    se::Object* thisObj = jsThis.isObject() ? jsThis.toObject() : nullptr;
                    se::Object* funcObj = jsFunc.toObject();
                    bool succeed = funcObj->call(args, thisObj, &rval);
                    if (!succeed) {
                        se::ScriptEngine::getInstance()->clearException();
                    }
                    HolderType<CrossApp::CAListViewCell*, false>  result;
                    ok &= sevalue_to_native(rval, &result, nullptr);
                    SE_PRECONDITION2(ok, result.value(), "lambda function : Error processing return value with type CAListViewCell");
                    return result.value();
                };
                arg0.data = lambda;
            }
            else
            {
                arg0.data = nullptr;
            }
        } while(false)
        ;
        SE_PRECONDITION2(ok, false, "js_crossapp_CAListView_onCellAtIndexPath : Error processing arguments");
        cobj->onCellAtIndexPath(arg0.value());
        return true;
    }
    SE_REPORT_ERROR("wrong number of arguments: %d, was expecting %d", (int)argc, 1);
    return false;
}
SE_BIND_FUNC(js_crossapp_CAListView_onCellAtIndexPath)

static bool js_crossapp_CATableView_onCellAtIndexPath(se::State& s)
{
    CrossApp::CATableView* cobj = SE_THIS_OBJECT<CrossApp::CATableView>(s);
    SE_PRECONDITION2(cobj, false, "js_crossapp_CATableView_onCellAtIndexPath : Invalid Native Object");
    const auto& args = s.args();
    size_t argc = args.size();
    CC_UNUSED bool ok = true;
    if (argc == 1) {
        HolderType<std::function<CrossApp::CATableViewCell * (CrossApp::DSize, unsigned int, unsigned int)>, true> arg0 = {};
        do {
            if (args[0].isObject() && args[0].toObject()->isFunction())
            {
                se::Value jsThis(s.thisObject());
                se::Value jsFunc(args[0]);
                jsThis.toObject()->attachObject(jsFunc.toObject());
                auto lambda = [=](CrossApp::DSize larg0, unsigned int larg1, unsigned int larg2) -> CATableViewCell *{
                    se::ScriptEngine::getInstance()->clearException();
                    se::AutoHandleScope hs;
        
                    CC_UNUSED bool ok = true;
                    se::ValueArray args;
                    args.resize(3);
                    ok &= nativevalue_to_se(larg0, args[0], nullptr /*ctx*/);
                    ok &= nativevalue_to_se(larg1, args[1], nullptr /*ctx*/);
                    ok &= nativevalue_to_se(larg2, args[2], nullptr /*ctx*/);
                    se::Value rval;
                    se::Object* thisObj = jsThis.isObject() ? jsThis.toObject() : nullptr;
                    se::Object* funcObj = jsFunc.toObject();
                    bool succeed = funcObj->call(args, thisObj, &rval);
                    if (!succeed) {
                        se::ScriptEngine::getInstance()->clearException();
                    }
                    HolderType<CrossApp::CATableViewCell*, false> result;
                    ok &= sevalue_to_native(rval, &result, nullptr);
                    SE_PRECONDITION2(ok, result.value(), "lambda function : Error processing return value with type CATableViewCell");
                    return result.value();
                };
                arg0.data = lambda;
            }
            else
            {
                arg0.data = nullptr;
            }
        } while(false)
        ;
        SE_PRECONDITION2(ok, false, "js_crossapp_CATableView_onCellAtIndexPath : Error processing arguments");
        cobj->onCellAtIndexPath(arg0.value());
        return true;
    }
    SE_REPORT_ERROR("wrong number of arguments: %d, was expecting %d", (int)argc, 1);
    return false;
}
SE_BIND_FUNC(js_crossapp_CATableView_onCellAtIndexPath)

static bool js_crossapp_CATableView_onSectionViewForFooterInSection(se::State& s)
{
    CrossApp::CATableView* cobj = SE_THIS_OBJECT<CrossApp::CATableView>(s);
    SE_PRECONDITION2(cobj, false, "js_crossapp_CATableView_onSectionViewForFooterInSection : Invalid Native Object");
    const auto& args = s.args();
    size_t argc = args.size();
    CC_UNUSED bool ok = true;
    if (argc == 1) {
        HolderType<std::function<CrossApp::CAView * (CrossApp::DSize, unsigned int)>, true> arg0 = {};
        do {
            if (args[0].isObject() && args[0].toObject()->isFunction())
            {
                se::Value jsThis(s.thisObject());
                se::Value jsFunc(args[0]);
                jsThis.toObject()->attachObject(jsFunc.toObject());
                auto lambda = [=](CrossApp::DSize larg0, unsigned int larg1) -> CAView *{
                    se::ScriptEngine::getInstance()->clearException();
                    se::AutoHandleScope hs;
        
                    CC_UNUSED bool ok = true;
                    se::ValueArray args;
                    args.resize(2);
                    ok &= nativevalue_to_se(larg0, args[0], nullptr /*ctx*/);
                    ok &= nativevalue_to_se(larg1, args[1], nullptr /*ctx*/);
                    se::Value rval;
                    se::Object* thisObj = jsThis.isObject() ? jsThis.toObject() : nullptr;
                    se::Object* funcObj = jsFunc.toObject();
                    bool succeed = funcObj->call(args, thisObj, &rval);
                    if (!succeed) {
                        se::ScriptEngine::getInstance()->clearException();
                    }
                    HolderType<CrossApp::CAView *, false> result;
                    ok &= sevalue_to_native(rval, &result, nullptr);
                    SE_PRECONDITION2(ok, result.value(), "lambda function : Error processing return value with type CAView");
                    return result.value();
                };
                arg0.data = lambda;
            }
            else
            {
                arg0.data = nullptr;
            }
        } while(false)
        ;
        SE_PRECONDITION2(ok, false, "js_crossapp_CATableView_onSectionViewForFooterInSection : Error processing arguments");
        cobj->onSectionViewForFooterInSection(arg0.value());
        return true;
    }
    SE_REPORT_ERROR("wrong number of arguments: %d, was expecting %d", (int)argc, 1);
    return false;
}
SE_BIND_FUNC(js_crossapp_CATableView_onSectionViewForFooterInSection)

static bool js_crossapp_CATableView_onSectionViewForHeaderInSection(se::State& s)
{
    CrossApp::CATableView* cobj = SE_THIS_OBJECT<CrossApp::CATableView>(s);
    SE_PRECONDITION2(cobj, false, "js_crossapp_CATableView_onSectionViewForHeaderInSection : Invalid Native Object");
    const auto& args = s.args();
    size_t argc = args.size();
    CC_UNUSED bool ok = true;
    if (argc == 1) {
        HolderType<std::function<CrossApp::CAView * (CrossApp::DSize, unsigned int)>, true> arg0 = {};
        do {
            if (args[0].isObject() && args[0].toObject()->isFunction())
            {
                se::Value jsThis(s.thisObject());
                se::Value jsFunc(args[0]);
                jsThis.toObject()->attachObject(jsFunc.toObject());
                auto lambda = [=](CrossApp::DSize larg0, unsigned int larg1) -> CAView *{
                    se::ScriptEngine::getInstance()->clearException();
                    se::AutoHandleScope hs;
        
                    CC_UNUSED bool ok = true;
                    se::ValueArray args;
                    args.resize(2);
                    ok &= nativevalue_to_se(larg0, args[0], nullptr /*ctx*/);
                    ok &= nativevalue_to_se(larg1, args[1], nullptr /*ctx*/);
                    se::Value rval;
                    se::Object* thisObj = jsThis.isObject() ? jsThis.toObject() : nullptr;
                    se::Object* funcObj = jsFunc.toObject();
                    bool succeed = funcObj->call(args, thisObj, &rval);
                    if (!succeed) {
                        se::ScriptEngine::getInstance()->clearException();
                    }
                    HolderType<CrossApp::CAView*, false> result;
                    ok &= sevalue_to_native(rval, &result, nullptr);
                    SE_PRECONDITION2(ok, result.value(), "lambda function : Error processing return value with type CAView");
                    return result.value();
                };
                arg0.data = lambda;
            }
            else
            {
                arg0.data = nullptr;
            }
        } while(false)
        ;
        SE_PRECONDITION2(ok, false, "js_crossapp_CATableView_onSectionViewForHeaderInSection : Error processing arguments");
        cobj->onSectionViewForHeaderInSection(arg0.value());
        return true;
    }
    SE_REPORT_ERROR("wrong number of arguments: %d, was expecting %d", (int)argc, 1);
    return false;
}
SE_BIND_FUNC(js_crossapp_CATableView_onSectionViewForHeaderInSection)

static bool js_crossapp_CACollectionView_onCellAtIndexPath(se::State& s)
{
    CrossApp::CACollectionView* cobj = SE_THIS_OBJECT<CrossApp::CACollectionView>(s);
    SE_PRECONDITION2(cobj, false, "js_crossapp_CACollectionView_onCellAtIndexPath : Invalid Native Object");
    const auto& args = s.args();
    size_t argc = args.size();
    CC_UNUSED bool ok = true;
    if (argc == 1) {
        HolderType<std::function<CrossApp::CACollectionViewCell * (CrossApp::DSize, unsigned int, unsigned int, unsigned int)>, true> arg0 = {};
        do {
            if (args[0].isObject() && args[0].toObject()->isFunction())
            {
                se::Value jsThis(s.thisObject());
                se::Value jsFunc(args[0]);
                jsThis.toObject()->attachObject(jsFunc.toObject());
                auto lambda = [=](CrossApp::DSize larg0, unsigned int larg1, unsigned int larg2, unsigned int larg3) -> CACollectionViewCell *{
                    se::ScriptEngine::getInstance()->clearException();
                    se::AutoHandleScope hs;
        
                    CC_UNUSED bool ok = true;
                    se::ValueArray args;
                    args.resize(4);
                    ok &= nativevalue_to_se(larg0, args[0], nullptr /*ctx*/);
                    ok &= nativevalue_to_se(larg1, args[1], nullptr /*ctx*/);
                    ok &= nativevalue_to_se(larg2, args[2], nullptr /*ctx*/);
                    ok &= nativevalue_to_se(larg3, args[3], nullptr /*ctx*/);
                    se::Value rval;
                    se::Object* thisObj = jsThis.isObject() ? jsThis.toObject() : nullptr;
                    se::Object* funcObj = jsFunc.toObject();
                    bool succeed = funcObj->call(args, thisObj, &rval);
                    if (!succeed) {
                        se::ScriptEngine::getInstance()->clearException();
                    }
                    HolderType<CrossApp::CACollectionViewCell *, false> result;
                    ok &= sevalue_to_native(rval, &result, nullptr);
                    SE_PRECONDITION2(ok, result.value(), "lambda function : Error processing return value with type CACollectionViewCell");
                    return result.value();
                };
                arg0.data = lambda;
            }
            else
            {
                arg0.data = nullptr;
            }
        } while(false)
        ;
        SE_PRECONDITION2(ok, false, "js_crossapp_CACollectionView_onCellAtIndexPath : Error processing arguments");
        cobj->onCellAtIndexPath(arg0.value());
        return true;
    }
    SE_REPORT_ERROR("wrong number of arguments: %d, was expecting %d", (int)argc, 1);
    return false;
}
SE_BIND_FUNC(js_crossapp_CACollectionView_onCellAtIndexPath)

static bool js_crossapp_CACollectionView_onSectionViewForFooterInSection(se::State& s)
{
    CrossApp::CACollectionView* cobj = SE_THIS_OBJECT<CrossApp::CACollectionView>(s);
    SE_PRECONDITION2(cobj, false, "js_crossapp_CACollectionView_onSectionViewForFooterInSection : Invalid Native Object");
    const auto& args = s.args();
    size_t argc = args.size();
    CC_UNUSED bool ok = true;
    if (argc == 1) {
        HolderType<std::function<CrossApp::CAView * (CrossApp::DSize, unsigned int)>, true> arg0 = {};
        do {
            if (args[0].isObject() && args[0].toObject()->isFunction())
            {
                se::Value jsThis(s.thisObject());
                se::Value jsFunc(args[0]);
                jsThis.toObject()->attachObject(jsFunc.toObject());
                auto lambda = [=](CrossApp::DSize larg0, unsigned int larg1) -> CAView *{
                    se::ScriptEngine::getInstance()->clearException();
                    se::AutoHandleScope hs;
        
                    CC_UNUSED bool ok = true;
                    se::ValueArray args;
                    args.resize(2);
                    ok &= nativevalue_to_se(larg0, args[0], nullptr /*ctx*/);
                    ok &= nativevalue_to_se(larg1, args[1], nullptr /*ctx*/);
                    se::Value rval;
                    se::Object* thisObj = jsThis.isObject() ? jsThis.toObject() : nullptr;
                    se::Object* funcObj = jsFunc.toObject();
                    bool succeed = funcObj->call(args, thisObj, &rval);
                    if (!succeed) {
                        se::ScriptEngine::getInstance()->clearException();
                    }
                    HolderType<CrossApp::CAView *, false> result;
                    ok &= sevalue_to_native(rval, &result, nullptr);
                    SE_PRECONDITION2(ok, result.value(), "lambda function : Error processing return value with type CAView");
                    return result.value();
                };
                arg0.data = lambda;
            }
            else
            {
                arg0.data = nullptr;
            }
        } while(false)
        ;
        SE_PRECONDITION2(ok, false, "js_crossapp_CACollectionView_onSectionViewForFooterInSection : Error processing arguments");
        cobj->onSectionViewForFooterInSection(arg0.value());
        return true;
    }
    SE_REPORT_ERROR("wrong number of arguments: %d, was expecting %d", (int)argc, 1);
    return false;
}
SE_BIND_FUNC(js_crossapp_CACollectionView_onSectionViewForFooterInSection)

static bool js_crossapp_CACollectionView_onSectionViewForHeaderInSection(se::State& s)
{
    CrossApp::CACollectionView* cobj = SE_THIS_OBJECT<CrossApp::CACollectionView>(s);
    SE_PRECONDITION2(cobj, false, "js_crossapp_CACollectionView_onSectionViewForHeaderInSection : Invalid Native Object");
    const auto& args = s.args();
    size_t argc = args.size();
    CC_UNUSED bool ok = true;
    if (argc == 1) {
        HolderType<std::function<CrossApp::CAView * (CrossApp::DSize, unsigned int)>, true> arg0 = {};
        do {
            if (args[0].isObject() && args[0].toObject()->isFunction())
            {
                se::Value jsThis(s.thisObject());
                se::Value jsFunc(args[0]);
                jsThis.toObject()->attachObject(jsFunc.toObject());
                auto lambda = [=](CrossApp::DSize larg0, unsigned int larg1) -> CAView *{
                    se::ScriptEngine::getInstance()->clearException();
                    se::AutoHandleScope hs;
        
                    CC_UNUSED bool ok = true;
                    se::ValueArray args;
                    args.resize(2);
                    ok &= nativevalue_to_se(larg0, args[0], nullptr /*ctx*/);
                    ok &= nativevalue_to_se(larg1, args[1], nullptr /*ctx*/);
                    se::Value rval;
                    se::Object* thisObj = jsThis.isObject() ? jsThis.toObject() : nullptr;
                    se::Object* funcObj = jsFunc.toObject();
                    bool succeed = funcObj->call(args, thisObj, &rval);
                    if (!succeed) {
                        se::ScriptEngine::getInstance()->clearException();
                    }
                    HolderType<CrossApp::CAView *, false> result;
                    ok &= sevalue_to_native(rval, &result, nullptr);
                    SE_PRECONDITION2(ok, result.value(), "lambda function : Error processing return value with type CAView");
                    return result.value();
                };
                arg0.data = lambda;
            }
            else
            {
                arg0.data = nullptr;
            }
        } while(false)
        ;
        SE_PRECONDITION2(ok, false, "js_crossapp_CACollectionView_onSectionViewForHeaderInSection : Error processing arguments");
        cobj->onSectionViewForHeaderInSection(arg0.value());
        return true;
    }
    SE_REPORT_ERROR("wrong number of arguments: %d, was expecting %d", (int)argc, 1);
    return false;
}
SE_BIND_FUNC(js_crossapp_CACollectionView_onSectionViewForHeaderInSection)

static bool js_crossapp_CAAutoCollectionView_onCellAtIndexPath(se::State& s)
{
    CrossApp::CAAutoCollectionView* cobj = SE_THIS_OBJECT<CrossApp::CAAutoCollectionView>(s);
    SE_PRECONDITION2(cobj, false, "js_crossapp_CAAutoCollectionView_onCellAtIndexPath : Invalid Native Object");
    const auto& args = s.args();
    size_t argc = args.size();
    CC_UNUSED bool ok = true;
    if (argc == 1) {
        HolderType<std::function<CrossApp::CACollectionViewCell * (CrossApp::DSize, unsigned int, unsigned int)>, true> arg0 = {};
        do {
            if (args[0].isObject() && args[0].toObject()->isFunction())
            {
                se::Value jsThis(s.thisObject());
                se::Value jsFunc(args[0]);
                jsThis.toObject()->attachObject(jsFunc.toObject());
                auto lambda = [=](CrossApp::DSize larg0, unsigned int larg1, unsigned int larg2) -> CACollectionViewCell *{
                    se::ScriptEngine::getInstance()->clearException();
                    se::AutoHandleScope hs;
        
                    CC_UNUSED bool ok = true;
                    se::ValueArray args;
                    args.resize(3);
                    ok &= nativevalue_to_se(larg0, args[0], nullptr /*ctx*/);
                    ok &= nativevalue_to_se(larg1, args[1], nullptr /*ctx*/);
                    ok &= nativevalue_to_se(larg2, args[2], nullptr /*ctx*/);
                    se::Value rval;
                    se::Object* thisObj = jsThis.isObject() ? jsThis.toObject() : nullptr;
                    se::Object* funcObj = jsFunc.toObject();
                    bool succeed = funcObj->call(args, thisObj, &rval);
                    if (!succeed) {
                        se::ScriptEngine::getInstance()->clearException();
                    }
                    HolderType<CrossApp::CACollectionViewCell *, false> result;
                    ok &= sevalue_to_native(rval, &result, nullptr);
                    SE_PRECONDITION2(ok, result.value(), "lambda function : Error processing return value with type CACollectionViewCell");
                    return result.value();
                };
                arg0.data = lambda;
            }
            else
            {
                arg0.data = nullptr;
            }
        } while(false)
        ;
        SE_PRECONDITION2(ok, false, "js_crossapp_CAAutoCollectionView_onCellAtIndexPath : Error processing arguments");
        cobj->onCellAtIndexPath(arg0.value());
        return true;
    }
    SE_REPORT_ERROR("wrong number of arguments: %d, was expecting %d", (int)argc, 1);
    return false;
}
SE_BIND_FUNC(js_crossapp_CAAutoCollectionView_onCellAtIndexPath)

static bool js_crossapp_CAAutoCollectionView_onSectionViewForFooterInSection(se::State& s)
{
    CrossApp::CAAutoCollectionView* cobj = SE_THIS_OBJECT<CrossApp::CAAutoCollectionView>(s);
    SE_PRECONDITION2(cobj, false, "js_crossapp_CAAutoCollectionView_onSectionViewForFooterInSection : Invalid Native Object");
    const auto& args = s.args();
    size_t argc = args.size();
    CC_UNUSED bool ok = true;
    if (argc == 1) {
        HolderType<std::function<CrossApp::CAView * (CrossApp::DSize, unsigned int)>, true> arg0 = {};
        do {
            if (args[0].isObject() && args[0].toObject()->isFunction())
            {
                se::Value jsThis(s.thisObject());
                se::Value jsFunc(args[0]);
                jsThis.toObject()->attachObject(jsFunc.toObject());
                auto lambda = [=](CrossApp::DSize larg0, unsigned int larg1) -> CAView *{
                    se::ScriptEngine::getInstance()->clearException();
                    se::AutoHandleScope hs;
        
                    CC_UNUSED bool ok = true;
                    se::ValueArray args;
                    args.resize(2);
                    ok &= nativevalue_to_se(larg0, args[0], nullptr /*ctx*/);
                    ok &= nativevalue_to_se(larg1, args[1], nullptr /*ctx*/);
                    se::Value rval;
                    se::Object* thisObj = jsThis.isObject() ? jsThis.toObject() : nullptr;
                    se::Object* funcObj = jsFunc.toObject();
                    bool succeed = funcObj->call(args, thisObj, &rval);
                    if (!succeed) {
                        se::ScriptEngine::getInstance()->clearException();
                    }
                    HolderType<CrossApp::CAView *, false> result;
                    ok &= sevalue_to_native(rval, &result, nullptr);
                    SE_PRECONDITION2(ok, result.value(), "lambda function : Error processing return value with type CAView");
                    return result.value();
                };
                arg0.data = lambda;
            }
            else
            {
                arg0.data = nullptr;
            }
        } while(false)
        ;
        SE_PRECONDITION2(ok, false, "js_crossapp_CAAutoCollectionView_onSectionViewForFooterInSection : Error processing arguments");
        cobj->onSectionViewForFooterInSection(arg0.value());
        return true;
    }
    SE_REPORT_ERROR("wrong number of arguments: %d, was expecting %d", (int)argc, 1);
    return false;
}
SE_BIND_FUNC(js_crossapp_CAAutoCollectionView_onSectionViewForFooterInSection)

static bool js_crossapp_CAAutoCollectionView_onSectionViewForHeaderInSection(se::State& s)
{
    CrossApp::CAAutoCollectionView* cobj = SE_THIS_OBJECT<CrossApp::CAAutoCollectionView>(s);
    SE_PRECONDITION2(cobj, false, "js_crossapp_CAAutoCollectionView_onSectionViewForHeaderInSection : Invalid Native Object");
    const auto& args = s.args();
    size_t argc = args.size();
    CC_UNUSED bool ok = true;
    if (argc == 1) {
        HolderType<std::function<CrossApp::CAView * (CrossApp::DSize, unsigned int)>, true> arg0 = {};
        do {
            if (args[0].isObject() && args[0].toObject()->isFunction())
            {
                se::Value jsThis(s.thisObject());
                se::Value jsFunc(args[0]);
                jsThis.toObject()->attachObject(jsFunc.toObject());
                auto lambda = [=](CrossApp::DSize larg0, unsigned int larg1) -> CAView *{
                    se::ScriptEngine::getInstance()->clearException();
                    se::AutoHandleScope hs;
        
                    CC_UNUSED bool ok = true;
                    se::ValueArray args;
                    args.resize(2);
                    ok &= nativevalue_to_se(larg0, args[0], nullptr /*ctx*/);
                    ok &= nativevalue_to_se(larg1, args[1], nullptr /*ctx*/);
                    se::Value rval;
                    se::Object* thisObj = jsThis.isObject() ? jsThis.toObject() : nullptr;
                    se::Object* funcObj = jsFunc.toObject();
                    bool succeed = funcObj->call(args, thisObj, &rval);
                    if (!succeed) {
                        se::ScriptEngine::getInstance()->clearException();
                    }
                    HolderType<CrossApp::CAView *, false> result;
                    ok &= sevalue_to_native(rval, &result, nullptr);
                    SE_PRECONDITION2(ok, result.value(), "lambda function : Error processing return value with type CAView");
                    return result.value();
                };
                arg0.data = lambda;
            }
            else
            {
                arg0.data = nullptr;
            }
        } while(false)
        ;
        SE_PRECONDITION2(ok, false, "js_crossapp_CAAutoCollectionView_onSectionViewForHeaderInSection : Error processing arguments");
        cobj->onSectionViewForHeaderInSection(arg0.value());
        return true;
    }
    SE_REPORT_ERROR("wrong number of arguments: %d, was expecting %d", (int)argc, 1);
    return false;
}
SE_BIND_FUNC(js_crossapp_CAAutoCollectionView_onSectionViewForHeaderInSection)

static bool js_crossapp_CAWaterfallView_onCellAtIndexPath(se::State& s)
{
    CrossApp::CAWaterfallView* cobj = SE_THIS_OBJECT<CrossApp::CAWaterfallView>(s);
    SE_PRECONDITION2(cobj, false, "js_crossapp_CAWaterfallView_onCellAtIndexPath : Invalid Native Object");
    const auto& args = s.args();
    size_t argc = args.size();
    CC_UNUSED bool ok = true;
    if (argc == 1) {
        HolderType<std::function<CrossApp::CAWaterfallViewCell * (CrossApp::DSize, unsigned int)>, true> arg0 = {};
        do {
            if (args[0].isObject() && args[0].toObject()->isFunction())
            {
                se::Value jsThis(s.thisObject());
                se::Value jsFunc(args[0]);
                jsThis.toObject()->attachObject(jsFunc.toObject());
                auto lambda = [=](CrossApp::DSize larg0, unsigned int larg1) -> CAWaterfallViewCell *{
                    se::ScriptEngine::getInstance()->clearException();
                    se::AutoHandleScope hs;
        
                    CC_UNUSED bool ok = true;
                    se::ValueArray args;
                    args.resize(2);
                    ok &= nativevalue_to_se(larg0, args[0], nullptr /*ctx*/);
                    ok &= nativevalue_to_se(larg1, args[1], nullptr /*ctx*/);
                    se::Value rval;
                    se::Object* thisObj = jsThis.isObject() ? jsThis.toObject() : nullptr;
                    se::Object* funcObj = jsFunc.toObject();
                    bool succeed = funcObj->call(args, thisObj, &rval);
                    if (!succeed) {
                        se::ScriptEngine::getInstance()->clearException();
                    }
                    HolderType<CrossApp::CAWaterfallViewCell *, false> result;
                    ok &= sevalue_to_native(rval, &result, nullptr);
                    SE_PRECONDITION2(ok, result.value(), "lambda function : Error processing return value with type CAWaterfallViewCell");
                    return result.value();
                };
                arg0.data = lambda;
            }
            else
            {
                arg0.data = nullptr;
            }
        } while(false)
        ;
        SE_PRECONDITION2(ok, false, "js_crossapp_CAWaterfallView_onCellAtIndexPath : Error processing arguments");
        cobj->onCellAtIndexPath(arg0.value());
        return true;
    }
    SE_REPORT_ERROR("wrong number of arguments: %d, was expecting %d", (int)argc, 1);
    return false;
}
SE_BIND_FUNC(js_crossapp_CAWaterfallView_onCellAtIndexPath)

static bool js_crossapp_CAPickerView_onViewForRow(se::State& s)
{
    CrossApp::CAPickerView* cobj = SE_THIS_OBJECT<CrossApp::CAPickerView>(s);
    SE_PRECONDITION2(cobj, false, "js_crossapp_CAPickerView_onViewForRow : Invalid Native Object");
    const auto& args = s.args();
    size_t argc = args.size();
    CC_UNUSED bool ok = true;
    if (argc == 1) {
        HolderType<std::function<CrossApp::CAView * (unsigned int, unsigned int)>, true> arg0 = {};
        do {
            if (args[0].isObject() && args[0].toObject()->isFunction())
            {
                se::Value jsThis(s.thisObject());
                se::Value jsFunc(args[0]);
                jsThis.toObject()->attachObject(jsFunc.toObject());
                auto lambda = [=](unsigned int larg0, unsigned int larg1) -> CAView *{
                    se::ScriptEngine::getInstance()->clearException();
                    se::AutoHandleScope hs;
        
                    CC_UNUSED bool ok = true;
                    se::ValueArray args;
                    args.resize(2);
                    ok &= nativevalue_to_se(larg0, args[0], nullptr /*ctx*/);
                    ok &= nativevalue_to_se(larg1, args[1], nullptr /*ctx*/);
                    se::Value rval;
                    se::Object* thisObj = jsThis.isObject() ? jsThis.toObject() : nullptr;
                    se::Object* funcObj = jsFunc.toObject();
                    bool succeed = funcObj->call(args, thisObj, &rval);
                    if (!succeed) {
                        se::ScriptEngine::getInstance()->clearException();
                    }
                    HolderType<CrossApp::CAView *, false> result;
                    ok &= sevalue_to_native(rval, &result, nullptr);
                    SE_PRECONDITION2(ok, result.value(), "lambda function : Error processing return value with type CAView");
                    return result.value();
                };
                arg0.data = lambda;
            }
            else
            {
                arg0.data = nullptr;
            }
        } while(false)
        ;
        SE_PRECONDITION2(ok, false, "js_crossapp_CAPickerView_onViewForRow : Error processing arguments");
        cobj->onViewForRow(arg0.value());
        return true;
    }
    SE_REPORT_ERROR("wrong number of arguments: %d, was expecting %d", (int)argc, 1);
    return false;
}
SE_BIND_FUNC(js_crossapp_CAPickerView_onViewForRow)



static bool js_crossapp_CAPickerView_onViewForSelected(se::State& s)
{
    CrossApp::CAPickerView* cobj = SE_THIS_OBJECT<CrossApp::CAPickerView>(s);
    SE_PRECONDITION2(cobj, false, "js_crossapp_CAPickerView_onViewForSelected : Invalid Native Object");
    const auto& args = s.args();
    size_t argc = args.size();
    CC_UNUSED bool ok = true;
    if (argc == 1) {
        HolderType<std::function<CrossApp::CAView * (unsigned int, CrossApp::DSize)>, true> arg0 = {};
        do {
            if (args[0].isObject() && args[0].toObject()->isFunction())
            {
                se::Value jsThis(s.thisObject());
                se::Value jsFunc(args[0]);
                jsThis.toObject()->attachObject(jsFunc.toObject());
                auto lambda = [=](unsigned int larg0, CrossApp::DSize larg1) -> CAView *{
                    se::ScriptEngine::getInstance()->clearException();
                    se::AutoHandleScope hs;
        
                    CC_UNUSED bool ok = true;
                    se::ValueArray args;
                    args.resize(2);
                    ok &= nativevalue_to_se(larg0, args[0], nullptr /*ctx*/);
                    ok &= nativevalue_to_se(larg1, args[1], nullptr /*ctx*/);
                    se::Value rval;
                    se::Object* thisObj = jsThis.isObject() ? jsThis.toObject() : nullptr;
                    se::Object* funcObj = jsFunc.toObject();
                    bool succeed = funcObj->call(args, thisObj, &rval);
                    if (!succeed) {
                        se::ScriptEngine::getInstance()->clearException();
                    }
                    HolderType<CrossApp::CAView *, false> result;
                    ok &= sevalue_to_native(rval, &result, nullptr);
                    SE_PRECONDITION2(ok, result.value(), "lambda function : Error processing return value with type CAView");
                    return result.value();
                };
                arg0.data = lambda;
            }
            else
            {
                arg0.data = nullptr;
            }
        } while(false)
        ;
        SE_PRECONDITION2(ok, false, "js_crossapp_CAPickerView_onViewForSelected : Error processing arguments");
        cobj->onViewForSelected(arg0.value());
        return true;
    }
    SE_REPORT_ERROR("wrong number of arguments: %d, was expecting %d", (int)argc, 1);
    return false;
}
SE_BIND_FUNC(js_crossapp_CAPickerView_onViewForSelected)

static bool js_crossapp_CAJSCall_call(se::State& s)
{
    CAJSCall* cobj = SE_THIS_OBJECT<CAJSCall>(s);
    SE_PRECONDITION2(cobj, false, "js_crossapp_CAJSCall_call : Invalid Native Object");
    const auto& args = s.args();
    size_t argc = args.size();
    CC_UNUSED bool ok = true;
    if (argc == 2) {
        HolderType<std::string, true> arg0 = {};
        CrossApp::CAValue value;
        ok &= sevalue_to_native(args[0], &arg0, s.thisObject());
        ok &= seval_to_cavalue(args[1], &value);
        SE_PRECONDITION2(ok, false, "js_crossapp_CAJSCall_call : Error processing arguments");
        const CrossApp::CAValue& result = cobj->call(arg0.value(), value);
        ok &= cavalue_to_seval(result, &s.rval());
        //nativevalue_to_se(result, s.rval(), nullptr /*ctx*/);
        SE_PRECONDITION2(ok, false, "js_crossapp_CAJSCall_call : Error processing arguments");
        SE_HOLD_RETURN_VALUE(result, s.thisObject(), s.rval());
        return true;
    }
    SE_REPORT_ERROR("wrong number of arguments: %d, was expecting %d", (int)argc, 2);
    return false;
}
SE_BIND_FUNC(js_crossapp_CAJSCall_call)

static bool js_crossapp_CAJSCall_listening_call(se::State& s)
{
    CAJSCall* cobj = SE_THIS_OBJECT<CAJSCall>(s);
    SE_PRECONDITION2(cobj, false, "js_crossapp_CAJSCall_listening_call : Invalid Native Object");
    const auto& args = s.args();
    size_t argc = args.size();
    CC_UNUSED bool ok = true;
    if (argc == 2) {
        HolderType<std::string, true> arg0 = {};
        HolderType<std::function<const CAValue& (const CAValue&)>, true> arg1 = {};
        ok &= sevalue_to_native(args[0], &arg0, s.thisObject());
        do {
            if (args[1].isObject() && args[1].toObject()->isFunction())
            {
                se::Value jsThis(s.thisObject());
                se::Value jsFunc(args[1]);
                jsThis.toObject()->attachObject(jsFunc.toObject());
                CAValue *result = new CAValue();
                auto lambda = [=](const CAValue& larg0) -> CAValue {
                    se::ScriptEngine::getInstance()->clearException();
                    se::AutoHandleScope hs;
        
                    CC_UNUSED bool ok = true;
                    se::ValueArray args;
                    args.resize(1);
                    ok &= cavalue_to_seval(larg0, &args[0]);
                    se::Value rval;
                    se::Object* thisObj = jsThis.isObject() ? jsThis.toObject() : nullptr;
                    se::Object* funcObj = jsFunc.toObject();
                    bool succeed = funcObj->call(args, thisObj, &rval);
                    if (!succeed) {
                        se::ScriptEngine::getInstance()->clearException();
                    }
                    ok &= seval_to_cavalue(rval, result);
                    SE_PRECONDITION2(ok, *result, "lambda function : Error processing return value with type const");
                    return *result;
                };
                arg1.data = lambda;
            }
            else
            {
                arg1.data = nullptr;
            }
        } while(false)
        ;
        SE_PRECONDITION2(ok, false, "js_crossapp_CAJSCall_listening_call : Error processing arguments");
        cobj->listening_call(arg0.value(), arg1.value());
        return true;
    }
    SE_REPORT_ERROR("wrong number of arguments: %d, was expecting %d", (int)argc, 2);
    return false;
}
SE_BIND_FUNC(js_crossapp_CAJSCall_listening_call)


static bool js_crossapp_CAJSCall_call_async(se::State& s)
{
    CAJSCall* cobj = SE_THIS_OBJECT<CAJSCall>(s);
    SE_PRECONDITION2(cobj, false, "js_crossapp_CAJSCall_call_async : Invalid Native Object");
    const auto& args = s.args();
    size_t argc = args.size();
    CC_UNUSED bool ok = true;
    if (argc == 2) {
        HolderType<std::string, true> arg0 = {};
        CAValue value;
        ok &= sevalue_to_native(args[0], &arg0, s.thisObject());
        ok &= seval_to_cavalue(args[1], &value);
        SE_PRECONDITION2(ok, false, "js_crossapp_CAJSCall_call_async : Error processing arguments");
        CAJSCallReceive* result = cobj->call_async(arg0.value(), value);
        ok &= nativevalue_to_se(result, s.rval(), nullptr /*ctx*/);
        SE_PRECONDITION2(ok, false, "js_crossapp_CAJSCall_call_async : Error processing arguments");
        SE_HOLD_RETURN_VALUE(result, s.thisObject(), s.rval());
        return true;
    }
    SE_REPORT_ERROR("wrong number of arguments: %d, was expecting %d", (int)argc, 2);
    return false;
}
SE_BIND_FUNC(js_crossapp_CAJSCall_call_async)

static bool js_crossapp_CAJSCallReceive_receive(se::State& s)
{
    CAJSCallReceive* cobj = SE_THIS_OBJECT<CAJSCallReceive>(s);
    SE_PRECONDITION2(cobj, false, "js_crossapp_CAJSCallReceive_receive : Invalid Native Object");
    const auto& args = s.args();
    size_t argc = args.size();
    CC_UNUSED bool ok = true;
    if (argc == 1) {
        CAValue value;
        ok &= seval_to_cavalue(args[0], &value);
        SE_PRECONDITION2(ok, false, "js_crossapp_CAJSCallReceive_receive : Error processing arguments");
        cobj->receive(value);
        return true;
    }
    SE_REPORT_ERROR("wrong number of arguments: %d, was expecting %d", (int)argc, 1);
    return false;
}
SE_BIND_FUNC(js_crossapp_CAJSCallReceive_receive)


//static bool js_crossapp_CAJSCall_listening_call_async(se::State& s)
//{
//    CAJSCall* cobj = SE_THIS_OBJECT<CAJSCall>(s);
//    SE_PRECONDITION2(cobj, false, "js_crossapp_CAJSCall_listening_call_async : Invalid Native Object");
//    const auto& args = s.args();
//    size_t argc = args.size();
//    CC_UNUSED bool ok = true;
////    if (argc == 2) {
////        HolderType<std::string, true> arg0 = {};
////        HolderType< std::function< void(const std::string, const std::function< void(const std::string) >) >, true >  arg1 = {};
////
////        ok &= sevalue_to_native(args[0], &arg0, s.thisObject());
////        do {
////            if (args[1].isObject() && args[1].toObject()->isFunction())
////            {
////                se::Value jsThis(s.thisObject());
////                se::Value jsFunc(args[1]);
////                jsThis.toObject()->attachObject(jsFunc.toObject());
////
////
////                auto lambda = [=](std::string larg0, const std::function<void (std::string)>& larg1) -> void {
////                    se::ScriptEngine::getInstance()->clearException();
////                    se::AutoHandleScope hs;
////
////                    CC_UNUSED bool ok = true;
////                    se::ValueArray args;
////                    args.resize(2);
////                    ok &= nativevalue_to_se(larg0, args[0], nullptr);
////                    ok &= nativevalue_to_se(larg1, args[1], nullptr /*ctx*/);
////
////                    se::Value rval;
////                    se::Object* thisObj = jsThis.isObject() ? jsThis.toObject() : nullptr;
////                    se::Object* funcObj = jsFunc.toObject();
////                    bool succeed = funcObj->call(args, thisObj, &rval);
////                    if (!succeed) {
////                        se::ScriptEngine::getInstance()->clearException();
////                    }
////                };
////                arg1.data = lambda;
////            }
////            else
////            {
////                arg1.data = nullptr;
////            }
////        } while(false)
////        ;
////        SE_PRECONDITION2(ok, false, "js_crossapp_CAJSCall_listening_call_async : Error processing arguments");
////        cobj->listening_call_async(arg0.value(), arg1.value());
////        return true;
////    }
//    SE_REPORT_ERROR("wrong number of arguments: %d, was expecting %d", (int)argc, 2);
//    return false;
//}
//SE_BIND_FUNC(js_crossapp_CAJSCall_listening_call_async)

//static bool js_crossapp_CAView_setTag(se::State& s)
//{
//    CrossApp::CAView* cobj = SE_THIS_OBJECT<CrossApp::CAView>(s);
//    SE_PRECONDITION2(cobj, false, "js_crossapp_CAView_setTag : Invalid Native Object");
//    const auto& args = s.args();
//    size_t argc = args.size();
//    CC_UNUSED bool ok = true;
//    if (argc == 1) {
//        if (args[0].isString()){
//            HolderType<std::string, false> arg0 = {};
//            ok &= sevalue_to_native(args[0], &arg0, s.thisObject());
//            SE_PRECONDITION2(ok, false, "js_crossapp_CAView_setTag : Error processing arguments");
//            CrossApp::CCLog("%s",arg0.value().c_str());
//            cobj->setTag(std::stoi(arg0.value()));
//        }
//        else{
//            HolderType<int, false> arg0 = {};
//            ok &= sevalue_to_native(args[0], &arg0, s.thisObject());
//            SE_PRECONDITION2(ok, false, "js_crossapp_CAView_setTag : Error processing arguments");
//            cobj->setTag(arg0.value());
//        }
//        return true;
//    }
//    SE_REPORT_ERROR("wrong number of arguments: %d, was expecting %d", (int)argc, 1);
//    return false;
//}
//SE_BIND_FUNC(js_crossapp_CAView_setTag)
//
//
//static bool js_crossapp_CAObject_setTag(se::State& s)
//{
//    CrossApp::CAObject* cobj = SE_THIS_OBJECT<CrossApp::CAObject>(s);
//    SE_PRECONDITION2(cobj, false, "js_crossapp_CAObject_setTag : Invalid Native Object");
//    const auto& args = s.args();
//    size_t argc = args.size();
//    CC_UNUSED bool ok = true;
//    if (argc == 1) {
//        if (args[0].isString()){
//            HolderType<std::string, false> arg0 = {};
//            ok &= sevalue_to_native(args[0], &arg0, s.thisObject());
//            SE_PRECONDITION2(ok, false, "js_crossapp_CAObject_setTag : Error processing arguments");
//            cobj->setTag(std::stoi(arg0.value()));
//            return true;
//        }else{
//            HolderType<int, false> arg0 = {};
//            ok &= sevalue_to_native(args[0], &arg0, s.thisObject());
//            SE_PRECONDITION2(ok, false, "js_crossapp_CAObject_setTag : Error processing arguments");
//            cobj->setTag(arg0.value());
//            return true;
//        }
//
//    }
//    SE_REPORT_ERROR("wrong number of arguments: %d, was expecting %d", (int)argc, 1);
//    return false;
//}
//SE_BIND_FUNC(js_crossapp_CAObject_setTag)

se::Object* __jsb_CrossApp_CACustomAnimation_proto = nullptr;
se::Class* __jsb_CrossApp_CACustomAnimation_class = nullptr;

static bool js_crossapp_CACustomAnimation_unschedule(se::State& s)
{
    const auto& args = s.args();
    size_t argc = args.size();
    CC_UNUSED bool ok = true;
    if (argc == 1) {
        HolderType<std::string, true> arg0 = {};
        ok &= sevalue_to_native(args[0], &arg0, nullptr);
        SE_PRECONDITION2(ok, false, "js_crossapp_CACustomAnimation_unschedule : Error processing arguments");
        CrossApp::CACustomAnimation::unschedule(arg0.value());
        return true;
    }
    SE_REPORT_ERROR("wrong number of arguments: %d, was expecting %d", (int)argc, 1);
    return false;
}
SE_BIND_FUNC(js_crossapp_CACustomAnimation_unschedule)

static bool js_crossapp_CACustomAnimation_schedule(se::State &s){
    const auto& args = s.args();
    size_t argc = args.size();
    bool ok = true;
    do {
        if (argc == 3){
            HolderType<std::function<void (const CrossApp::CACustomAnimation::Model &)>, true> arg0 = {};
            if (!ok) { ok = true; break; }
            do {
                if (args[0].toObject()->isFunction()){
                    se::Value jsThis(s.thisObject());
                    se::Value jsFunc(args[0]);
                    jsThis.toObject()->attachObject(jsFunc.toObject());
                    auto lambda = [=, &ok](const CrossApp::CACustomAnimation::Model & var) -> void {
                        se::AutoHandleScope se;
                        se::ScriptEngine::getInstance()->clearException();
                        CAValueMap larg0;
                        se::Value rval;
                        se::ValueArray argsv;
                        argsv.resize(1);
                        larg0["dt"]     = CAValue(var.dt);
                        larg0["now"]    = CAValue(var.now);
                        larg0["total"]  = CAValue(var.total);
                        larg0["end"]    = CAValue(var.end);
                        ok &= cavaluemap_to_seval(larg0, &argsv[0]);
                        se::Object* thisObj = jsThis.isObject() ? jsThis.toObject() : nullptr;
                        se::Object* funcObj = jsFunc.toObject();
                        bool succeed = funcObj->call(argsv, thisObj,&rval);
                        if (!succeed){
                            se::ScriptEngine::getInstance()->clearException();
                        }
                    };
                    arg0.data = lambda;

                }else{
                    arg0.data = nullptr;
                }
                if (!ok) { ok = true; break; }
                std::string arg1;
                ok &= seval_to_std_string(args[1], &arg1);
                if (!ok) { ok = true; break; }
                double arg2 = 0;
                ok &= seval_to_double(args[2], &arg2);
                if (!ok) { ok = true; break; }
                CrossApp::CACustomAnimation::schedule(arg0.value(), arg1, arg2);
                return true;

            } while (0);
        }
    } while (0);
    
    do {
        if (argc == 4){
            HolderType<std::function<void (const CrossApp::CACustomAnimation::Model &)>, true> arg0 = {};
            if (!ok) { ok = true; break; }
            do {
                if (args[0].toObject()->isFunction()){
                    se::Value jsThis(s.thisObject());
                    se::Value jsFunc(args[0]);
                    jsThis.toObject()->attachObject(jsFunc.toObject());
                    auto lambda = [=, &ok](const CrossApp::CACustomAnimation::Model & var) -> void {
                        se::AutoHandleScope se;
                        se::ScriptEngine::getInstance()->clearException();
                        CAValueMap larg0;
                        se::Value rval;
                        se::ValueArray argsv;
                        argsv.resize(1);
                        larg0["dt"]     = CAValue(var.dt);
                        larg0["now"]    = CAValue(var.now);
                        larg0["total"]  = CAValue(var.total);
                        larg0["end"]    = CAValue(var.end);
                        ok &= cavaluemap_to_seval(larg0, &argsv[0]);
                        se::Object* thisObj = jsThis.isObject() ? jsThis.toObject() : nullptr;
                        se::Object* funcObj = jsFunc.toObject();
                        bool succeed = funcObj->call(argsv, thisObj,&rval);
                        if (!succeed){
                            se::ScriptEngine::getInstance()->clearException();
                        }
                    };
                    arg0.data = lambda;

                }else{
                    arg0.data = nullptr;
                }
                if (!ok) { ok = true; break; }
                std::string arg1;
                ok &= seval_to_std_string(args[1], &arg1);
                if (!ok) { ok = true; break; }
                double arg2 = 0;
                ok &= seval_to_double(args[2], &arg2);
                if (!ok) { ok = true; break; }
                double arg3 = 0;
                ok &= seval_to_double(args[3], &arg3);
                if (!ok) { ok = true; break; }
                CrossApp::CACustomAnimation::schedule(arg0.value(), arg1, arg2,arg3);
                return true;

            } while (0);
        }
    } while (0);
    
    do {
        if (argc == 5){
            HolderType<std::function<void (const CrossApp::CACustomAnimation::Model &)>, true> arg0 = {};
            if (!ok) { ok = true; break; }
            do {
                if (args[0].toObject()->isFunction()){
                    se::Value jsFunc(args[0]);
                    auto lambda = [=, &ok](const CrossApp::CACustomAnimation::Model & var) -> void {
                        se::AutoHandleScope se;
                        se::ScriptEngine::getInstance()->clearException();
                        CAValueMap larg0;
                        se::Value rval;
                        se::ValueArray argsv;
                        argsv.resize(1);
                        larg0["dt"]     = CAValue(var.dt);
                        larg0["now"]    = CAValue(var.now);
                        larg0["total"]  = CAValue(var.total);
                        larg0["end"]    = CAValue(var.end);
                        
                        ok &= cavaluemap_to_seval(larg0, &argsv[0]);
                        se::Object* funcObj = jsFunc.toObject();
                        bool succeed = funcObj->call(argsv, nullptr,&rval);
                        if (!succeed){
                            se::ScriptEngine::getInstance()->clearException();
                        }
                    };
                    arg0.data = lambda;

                }else{
                    arg0.data = nullptr;
                }
                if (!ok) { ok = true; break; }
                std::string arg1;
                ok &= seval_to_std_string(args[1], &arg1);
                if (!ok) { ok = true; break; }
                double arg2 = 0;
                ok &= seval_to_double(args[2], &arg2);
                if (!ok) { ok = true; break; }
                double arg3 = 0;
                ok &= seval_to_double(args[3], &arg3);
                if (!ok) { ok = true; break; }
                double arg4 = 0;
                ok &= seval_to_double(args[4], &arg4);
                if (!ok) { ok = true; break; }
                CrossApp::CACustomAnimation::schedule(arg0.value(), arg1, arg2,arg3,arg4);
                return true;

            } while (0);
        }
    } while (0);
    SE_REPORT_ERROR("js_crossapp_CACustomAnimation_schedule : wrong number of arguments");
    return false;
}
SE_BIND_FUNC(js_crossapp_CACustomAnimation_schedule)

static bool js_crossapp_CACustomAnimation_isSchedule(se::State& s)
{
    const auto& args = s.args();
    size_t argc = args.size();
    CC_UNUSED bool ok = true;
    if (argc == 1) {
        HolderType<std::string, true> arg0 = {};
        ok &= sevalue_to_native(args[0], &arg0, nullptr);
        SE_PRECONDITION2(ok, false, "js_crossapp_CACustomAnimation_isSchedule : Error processing arguments");
        bool result = CrossApp::CACustomAnimation::isSchedule(arg0.value());
        ok &= nativevalue_to_se(result, s.rval(), nullptr /*ctx*/);
        SE_PRECONDITION2(ok, false, "js_crossapp_CACustomAnimation_isSchedule : Error processing arguments");
        SE_HOLD_RETURN_VALUE(result, s.thisObject(), s.rval());
        return true;
    }
    SE_REPORT_ERROR("wrong number of arguments: %d, was expecting %d", (int)argc, 1);
    return false;
}
SE_BIND_FUNC(js_crossapp_CACustomAnimation_isSchedule)




static bool js_CrossApp_CACustomAnimation_finalize(se::State& s)
{
    auto iter = se::NonRefNativePtrCreatedByCtorMap::find(SE_THIS_OBJECT<CrossApp::CACustomAnimation>(s));
    if (iter != se::NonRefNativePtrCreatedByCtorMap::end())
    {
        se::NonRefNativePtrCreatedByCtorMap::erase(iter);
    }
    return true;
}
SE_BIND_FINALIZE_FUNC(js_CrossApp_CACustomAnimation_finalize)

bool js_register_crossapp_CACustomAnimation(se::Object* obj)
{
    auto cls = se::Class::create("CACustomAnimation", obj, nullptr, nullptr);

    cls->defineStaticFunction("unschedule", _SE(js_crossapp_CACustomAnimation_unschedule));
    cls->defineStaticFunction("isSchedule", _SE(js_crossapp_CACustomAnimation_isSchedule));
    cls->defineStaticFunction("schedule", _SE(js_crossapp_CACustomAnimation_schedule));
    cls->defineFinalizeFunction(_SE(js_CrossApp_CACustomAnimation_finalize));
    cls->install();
    JSBClassType::registerClass<CrossApp::CACustomAnimation>(cls);

    __jsb_CrossApp_CACustomAnimation_proto = cls->getProto();
    __jsb_CrossApp_CACustomAnimation_class = cls;

    se::ScriptEngine::getInstance()->clearException();
    return true;
}


bool js_crossapp_retain(se::State &s){
    auto &args = s.args();
    se::Object *objFunc = args[0].toObject();
    CrossApp::CAObject* cobj = (CrossApp::CAObject *)objFunc;
    if (cobj == NULL){
        SE_PRECONDITION2(false, false, "type not supported!");
    }
    cobj->retain();
    
    
    return true;
}
SE_BIND_FUNC(js_crossapp_retain)

static bool register_cocos_manual_all(se::Object *obj){
//    __jsb_CrossApp_CAObject_proto->defineFunction("release", _SE(js_crossapp_release));
//    __jsb_CrossApp_CAObject_proto->defineFunction("retain", _SE(js_crossapp_retain));
    __jsb_CrossApp_CAMotionManager_proto->defineFunction("CAMotionManager_startGyroscope", _SE(js_crossapp_CAMotionManager_startGyroscope));
    __jsb_CrossApp_CANotificationCenter_proto->defineFunction("addObserver", _SE(js_crossapp_CANotificationCenter_addObserver));
    __jsb_CrossApp_CADatePickerView_proto->defineFunction("onSelectRow", _SE(js_crossapp_CADatePickerView_onSelectRow));
    __jsb_CrossApp_CAAddressBook_proto->defineFunction("getAddressBook", _SE(js_crossapp_CAAddressBook_getAddressBook));
    
    __jsb_CrossApp_CAScheduler_proto->defineFunction("isScheduled", _SE(js_crossapp_CAScheduler_isScheduled));
    __jsb_CrossApp_CAScheduler_proto->defineFunction("schedule", _SE(js_crossapp_CAScheduler_schedule));
    __jsb_CrossApp_CAScheduler_proto->defineFunction("unschedule", _SE(js_crossapp_CAScheduler_unschedule));
    __jsb_CrossApp_CAScheduler_proto->defineFunction("scheduleOnce", _SE(js_crossapp_CAScheduler_scheduleOnce));
    
    __jsb_CrossApp_CAListView_proto->defineFunction("onCellAtIndexPath", _SE(js_crossapp_CAListView_onCellAtIndexPath));
    
    __jsb_CrossApp_CATableView_proto->defineFunction("onCellAtIndexPath", _SE(js_crossapp_CATableView_onCellAtIndexPath));
    __jsb_CrossApp_CATableView_proto->defineFunction("onSectionViewForFooterInSection", _SE(js_crossapp_CATableView_onSectionViewForFooterInSection));
    __jsb_CrossApp_CATableView_proto->defineFunction("onSectionViewForHeaderInSection", _SE(js_crossapp_CATableView_onSectionViewForHeaderInSection));
    
    __jsb_CrossApp_CACollectionView_proto->defineFunction("onCellAtIndexPath", _SE(js_crossapp_CACollectionView_onCellAtIndexPath));
    __jsb_CrossApp_CACollectionView_proto->defineFunction("onSectionViewForFooterInSection", _SE(js_crossapp_CACollectionView_onSectionViewForFooterInSection));
    __jsb_CrossApp_CACollectionView_proto->defineFunction("onSectionViewForHeaderInSection", _SE(js_crossapp_CACollectionView_onSectionViewForHeaderInSection));
    
    __jsb_CrossApp_CAAutoCollectionView_proto->defineFunction("onCellAtIndexPath", _SE(js_crossapp_CAAutoCollectionView_onCellAtIndexPath));
    __jsb_CrossApp_CAAutoCollectionView_proto->defineFunction("onSectionViewForFooterInSection", _SE(js_crossapp_CAAutoCollectionView_onSectionViewForFooterInSection));
    __jsb_CrossApp_CAAutoCollectionView_proto->defineFunction("onSectionViewForHeaderInSection", _SE(js_crossapp_CAAutoCollectionView_onSectionViewForHeaderInSection));
    
    __jsb_CrossApp_CAWaterfallView_proto->defineFunction("onCellAtIndexPath", _SE(js_crossapp_CAWaterfallView_onCellAtIndexPath));
    
    __jsb_CrossApp_CAPickerView_proto->defineFunction("onViewForRow", _SE(js_crossapp_CAPickerView_onViewForRow));
    __jsb_CrossApp_CAPickerView_proto->defineFunction("onViewForSelected", _SE(js_crossapp_CAPickerView_onViewForSelected));
    
    
    __jsb_CAJSCall_proto->defineFunction("listening_call", _SE(js_crossapp_CAJSCall_listening_call));
    __jsb_CAJSCall_proto->defineFunction("call", _SE(js_crossapp_CAJSCall_call));
    __jsb_CAJSCall_proto->defineFunction("call_async", _SE(js_crossapp_CAJSCall_call_async));
    __jsb_CAJSCallReceive_proto->defineFunction("receive", _SE(js_crossapp_CAJSCallReceive_receive));
//    __jsb_CAJSCall_proto->defineFunction("listening_call_async", _SE(js_crossapp_CAJSCall_listening_call_async));

//    __jsb_CrossApp_CAObject_proto->defineFunction("setTag", _SE(js_crossapp_CAObject_setTag));
//    __jsb_CrossApp_CAView_proto->defineFunction("getSubviewByTextTag", _SE(js_crossapp_CAView_getSubviewByTextTag));
//    __jsb_CrossApp_CAView_proto->defineFunction("setTag", _SE(js_crossapp_CAView_setTag));

    se::ScriptEngine::getInstance()->clearException();
    return true;
}

void caLogCallback(const v8::FunctionCallbackInfo<v8::Value> &info) {
    if (info[0]->IsString()) {
        v8::String::Utf8Value utf8(v8::Isolate::GetCurrent(), info[0]);
        CCLog("%s\n",*utf8);
    }
}

bool register_all_crossapp_manual(se::Object *obj) {
    
    se::Value nsVal;
    if (!obj->getProperty("ca", &nsVal))
    {
        se::HandleObject jsobj(se::Object::createPlainObject());
        nsVal.setObject(jsobj);
        obj->setProperty("ca", nsVal);
        
    }
    
    se::Object* ns = nsVal.toObject();
    
    register_cocos_manual_all(ns);
    js_register_crossapp_CACustomAnimation(ns);
    register_se_setExceptionCallback(obj);
    return true;
}
