/****************************************************************************
 Copyright (c) 2017-2021 Xiamen Yaji Software Co., Ltd.

 http://www.cocos.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated engine source code (the "Software"), a limited,
 worldwide, royalty-free, non-assignable, revocable and non-exclusive license
 to use Cocos Creator solely to develop games on your target platforms. You shall
 not use Cocos Creator software for developing other software or tools that's
 used for developing games. You are not granted to publish, distribute,
 sublicense, and/or sell copies of Cocos Creator.

 The software or tools in this License Agreement are licensed, not sold.
 Xiamen Yaji Software Co., Ltd. reserves all rights not expressly granted to you.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
****************************************************************************/

#include "jsb_global.h"
#include "../base/CoreStd.h"
#include "../../platform/CAFileUtils.h"
#include "../../basics/CAApplication.h"
#include "jsb_conversions.h"
#include "../jswrapper/SeApi.h"


#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
//    #include "platform/android/jni/JniImp.h"
#endif

#include <chrono>
#include <regex>
#include <sstream>

using namespace ca; //NOLINT

namespace {

std::unordered_map<std::string, se::Value> gModuleCache;

static bool require(se::State &s) { //NOLINT
    const auto &args = s.args();
    int         argc = static_cast<int>(args.size());
    assert(argc >= 1);
    assert(args[0].isString());
//    printf("%s",args[0].toString().c_str());
    return jsb_run_script(args[0].toString(), &s.rval());
}
SE_BIND_FUNC(require)

static bool doModuleRequire(const std::string &path, se::Value *ret, const std::string &prevScriptFileDir) { //NOLINT
    se::AutoHandleScope hs;
    assert(!path.empty());

    const auto &fileOperationDelegate = se::ScriptEngine::getInstance()->getFileOperationDelegate();
    assert(fileOperationDelegate.isValid());

    std::string fullPath;

    std::string pathWithSuffix = path;
    if (pathWithSuffix.rfind(".js") != (pathWithSuffix.length() - 3)) {
        pathWithSuffix += ".js";
    }
    std::string scriptBuffer = fileOperationDelegate.onGetStringFromFile(pathWithSuffix);

    if (scriptBuffer.empty() && !prevScriptFileDir.empty()) {
        std::string secondPath = prevScriptFileDir;
        if (secondPath[secondPath.length() - 1] != '/') {
            secondPath += "/";
        }

        secondPath += path;

        if (FileUtils::getInstance()->isDirectoryExist(secondPath)) {
            if (secondPath[secondPath.length() - 1] != '/') {
                secondPath += "/";
            }
            secondPath += "index.js";
        } else {
            if (path.rfind(".js") != (path.length() - 3)) {
                secondPath += ".js";
            }
        }

        fullPath     = fileOperationDelegate.onGetFullPath(secondPath);
        scriptBuffer = fileOperationDelegate.onGetStringFromFile(fullPath);
    } else {
        fullPath = fileOperationDelegate.onGetFullPath(pathWithSuffix);
    }

    if (!scriptBuffer.empty()) {
        const auto &iter = gModuleCache.find(fullPath);
        if (iter != gModuleCache.end()) {
            *ret = iter->second;
            //                printf("Found cache: %s, value: %d\n", fullPath.c_str(), (int)ret->getType());
            return true;
        }
        std::string currentScriptFileDir = FileUtils::getInstance()->fullPathForFilename(fullPath);
        //CrossApp::CCFileUtils::getInstance()->getFileDir(fullPath);
        //

        // Add closure for evalutate the script
        char prefix[]    = "(function(currentScriptDir){ window.module = window.module || {}; var exports = window.module.exports = {}; ";
        char suffix[512] = {0};
        snprintf(suffix, sizeof(suffix), "\nwindow.module.exports = window.module.exports || exports;\n})('%s'); ", currentScriptFileDir.c_str());

        // Add current script path to require function invocation
        scriptBuffer = prefix + std::regex_replace(scriptBuffer, std::regex("([^A-Za-z0-9]|^)requireModule\\((.*?)\\)"), "$1requireModule($2, currentScriptDir)") + suffix;

        //            FILE* fp = fopen("/Users/james/Downloads/test.txt", "wb");
        //            fwrite(scriptBuffer.c_str(), scriptBuffer.length(), 1, fp);
        //            fclose(fp);

#if CC_TARGET_PLATFORM == CC_PLATFORM_MAC || CC_TARGET_PLATFORM == CC_PLATFORM_IOS
        std::string reletivePath = fullPath;
    #if CC_PLATFORM == CC_PLATFORM_MAC_OSX
        const std::string reletivePathKey = "/Contents/Resources";
    #else
        const std::string reletivePathKey = ".app";
    #endif

        size_t pos = reletivePath.find(reletivePathKey);
        if (pos != std::string::npos) {
            reletivePath = reletivePath.substr(pos + reletivePathKey.length() + 1);
        }
#else
        const std::string &reletivePath = fullPath;
#endif

        auto      se      = se::ScriptEngine::getInstance();
        bool      succeed = se->evalString(scriptBuffer.c_str(), scriptBuffer.length(), nullptr, reletivePath.c_str());
        se::Value moduleVal;
        if (succeed && se->getGlobalObject()->getProperty("module", &moduleVal) && moduleVal.isObject()) {
            se::Value exportsVal;
            if (moduleVal.toObject()->getProperty("exports", &exportsVal)) {
                if (ret != nullptr) {
                    *ret = exportsVal;
                }
                gModuleCache[fullPath] = std::move(exportsVal);
            } else {
                gModuleCache[fullPath] = se::Value::Undefined;
            }
            // clear module.exports
            moduleVal.toObject()->setProperty("exports", se::Value::Undefined);
        } else {
            gModuleCache[fullPath] = se::Value::Undefined;
        }
        assert(succeed);
        return succeed;
    }

    SE_LOGE("doModuleRequire %s, buffer is empty!\n", path.c_str());
    assert(false);
    return false;
}

static bool moduleRequire(se::State &s) { //NOLINT
    const auto &args = s.args();
    int         argc = static_cast<int>(args.size());
    assert(argc >= 2);
    assert(args[0].isString());
    assert(args[1].isString());

    return doModuleRequire(args[0].toString(), &s.rval(), args[1].toString());
}
SE_BIND_FUNC(moduleRequire)
} // namespace

bool jsb_run_script(const std::string &filePath, se::Value *rval /* = nullptr */) { //NOLINT
    se::AutoHandleScope hs;
    return se::ScriptEngine::getInstance()->runScript(filePath, rval);
}

bool jsb_run_script_module(const std::string &filePath, se::Value *rval /* = nullptr */) { //NOLINT
    return doModuleRequire(filePath, rval, "");
}

static bool jsc_garbageCollect(se::State &s) { //NOLINT
    se::ScriptEngine::getInstance()->garbageCollect();
    return true;
}
SE_BIND_FUNC(jsc_garbageCollect)

static bool jsc_dumpNativePtrToSeObjectMap(se::State &s) { //NOLINT
//    CC_LOG_DEBUG(">>> total: %d, Dump (native -> jsobj) map begin", (int)se::NativePtrToObjectMap::size());

    struct NamePtrStruct {
        const char *name;
        void *      ptr;
    };

    std::vector<NamePtrStruct> namePtrArray;

    for (const auto &e : se::NativePtrToObjectMap::instance()) {
        se::Object *jsobj = e.second;
        assert(jsobj->_getClass() != nullptr);
        NamePtrStruct tmp;
        tmp.name = jsobj->_getClass()->getName();
        tmp.ptr  = e.first;
        namePtrArray.push_back(tmp);
    }

    std::sort(namePtrArray.begin(), namePtrArray.end(), [](const NamePtrStruct &a, const NamePtrStruct &b) -> bool {
        std::string left  = a.name;
        std::string right = b.name;
        for (std::string::const_iterator lit = left.begin(), rit = right.begin(); lit != left.end() && rit != right.end(); ++lit, ++rit) {
            if (::tolower(*lit) < ::tolower(*rit)) {
                return true;
            }
            if (::tolower(*lit) > ::tolower(*rit)) {
                return false;
            }
        }
        return left.size() < right.size();
    });

    for (const auto &e : namePtrArray) {
//        CC_LOG_DEBUG("%s: %p", e.name, e.ptr);
    }
//    CC_LOG_DEBUG(">>> total: %d, nonRefMap: %d, Dump (native -> jsobj) map end", (int)se::NativePtrToObjectMap::size(), (int)se::NonRefNativePtrCreatedByCtorMap::size());
    return true;
}
SE_BIND_FUNC(jsc_dumpNativePtrToSeObjectMap)

static bool jsc_dumpRoot(se::State &s) { //NOLINT
    assert(false);
    return true;
}
SE_BIND_FUNC(jsc_dumpRoot)

static bool JSBCore_platform(se::State &s) { //NOLINT

//    Application::Platform platform = CAApplication::getApplication()->getPlatform();
//    s.rval().setInt32(static_cast<int32_t>(platform));
    return true;
}
SE_BIND_FUNC(JSBCore_platform)

static bool JSBCore_os(se::State &s) { //NOLINT
    se::Value os;

    // osx, ios, android, windows, linux, etc..
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    os.setString("iOS");
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    os.setString("Android");
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
    os.setString("Windows");
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_LINUX)
    os.setString("Linux");
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
    os.setString("OS X");
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_OHOS)
    os.setString("OHOS");
#endif

    s.rval() = os;
    return true;
}
SE_BIND_FUNC(JSBCore_os)

static bool JSBCore_getCurrentLanguage(se::State &s) { //NOLINT
    std::string               languageStr;
    LanguageType language = CAApplication::getApplication()->getCurrentLanguage();
    //CAApplication::getInstance()->getCurrentLanguage();
    switch (language) {
        case LanguageType::ENGLISH:
            languageStr = "en";
            break;
        case LanguageType::CHINESE:
            languageStr = "zh";
            break;
        case LanguageType::FRENCH:
            languageStr = "fr";
            break;
        case LanguageType::ITALIAN:
            languageStr = "it";
            break;
        case LanguageType::GERMAN:
            languageStr = "de";
            break;
        case LanguageType::SPANISH:
            languageStr = "es";
            break;
        case LanguageType::DUTCH:
            languageStr = "du";
            break;
        case LanguageType::RUSSIAN:
            languageStr = "ru";
            break;
        case LanguageType::KOREAN:
            languageStr = "ko";
            break;
        case LanguageType::JAPANESE:
            languageStr = "ja";
            break;
        case LanguageType::HUNGARIAN:
            languageStr = "hu";
            break;
        case LanguageType::PORTUGUESE:
            languageStr = "pt";
            break;
        case LanguageType::ARABIC:
            languageStr = "ar";
            break;
        case LanguageType::NORWEGIAN:
            languageStr = "no";
            break;
        case LanguageType::POLISH:
            languageStr = "pl";
            break;
        case LanguageType::TURKISH:
            languageStr = "tr";
            break;
        case LanguageType::UKRAINIAN:
            languageStr = "uk";
            break;
        case LanguageType::ROMANIAN:
            languageStr = "ro";
            break;
        case LanguageType::BULGARIAN:
            languageStr = "bg";
            break;
        default:
            languageStr = "unknown";
            break;
    }
    s.rval().setString(languageStr);
    return true;
}
SE_BIND_FUNC(JSBCore_getCurrentLanguage)

static bool JSB_core_restartVM(se::State &s) { //NOLINT
    //REFINE: release AudioEngine, waiting HttpClient & WebSocket threads to exit.
    CAApplication::getApplication()->restart();
    return true;
}
SE_BIND_FUNC(JSB_core_restartVM)

static bool JSB_isObjectValid(se::State &s) { //NOLINT
    const auto &args = s.args();
    int         argc = static_cast<int>(args.size());
    if (argc == 1) {
        void *nativePtr = nullptr;
        seval_to_native_ptr(args[0], &nativePtr);
        s.rval().setBoolean(nativePtr != nullptr);
        return true;
    }

    SE_REPORT_ERROR("Invalid number of arguments: %d. Expecting: 1", argc);
    return false;
}
SE_BIND_FUNC(JSB_isObjectValid)

static bool JSB_saveByteCode(se::State &s) { //NOLINT
    const auto &args = s.args();
    int         argc = static_cast<int>(args.size());
    SE_PRECONDITION2(argc == 2, false, "Invalid number of arguments");
    bool        ok = true;
    std::string srcfile;
    std::string dstfile;
    ok &= seval_to_std_string(args[0], &srcfile);
    ok &= seval_to_std_string(args[1], &dstfile);
    SE_PRECONDITION2(ok, false, "Error processing arguments");
    ok = se::ScriptEngine::getInstance()->saveByteCodeToFile(srcfile, dstfile);
    s.rval().setBoolean(ok);
    return true;
}
SE_BIND_FUNC(JSB_saveByteCode)

static bool getOrCreatePlainObject_r(const char *name, se::Object *parent, se::Object **outObj) { //NOLINT
    assert(parent != nullptr);
    assert(outObj != nullptr);
    se::Value tmp;

    if (parent->getProperty(name, &tmp) && tmp.isObject()) {
        *outObj = tmp.toObject();
        (*outObj)->incRef();
    } else {
        *outObj = se::Object::createPlainObject();
        parent->setProperty(name, se::Value(*outObj));
    }

    return true;
}

static bool js_performance_now(se::State &s) { //NOLINT
    auto now   = std::chrono::steady_clock::now();
    auto micro = std::chrono::duration_cast<std::chrono::microseconds>(now - se::ScriptEngine::getInstance()->getStartTime()).count();
    s.rval().setNumber(static_cast<double>(micro) * 0.001);
    return true;
}
SE_BIND_FUNC(js_performance_now)


bool jsb_register_global_variables(se::Object *global) { //NOLINT

    global->defineFunction("require", _SE(require));
    global->defineFunction("requireModule", _SE(moduleRequire));

    getOrCreatePlainObject_r("jsb", global, &__jsbObj);

    auto glContextCls = se::Class::create("WebGLRenderingContext", global, nullptr, nullptr);
    glContextCls->install();

    __jsbObj->defineFunction("garbageCollect", _SE(jsc_garbageCollect));
    __jsbObj->defineFunction("dumpNativePtrToSeObjectMap", _SE(jsc_dumpNativePtrToSeObjectMap));
    __jsbObj->defineFunction("saveByteCode", _SE(JSB_saveByteCode));
    global->defineFunction("__getPlatform", _SE(JSBCore_platform));
    global->defineFunction("__getOS", _SE(JSBCore_os));
    global->defineFunction("__getCurrentLanguage", _SE(JSBCore_getCurrentLanguage));

    global->defineFunction("__restartVM", _SE(JSB_core_restartVM));
    global->defineFunction("__isObjectValid", _SE(JSB_isObjectValid));

    se::HandleObject performanceObj(se::Object::createPlainObject());
    performanceObj->defineFunction("now", _SE(js_performance_now));
    global->setProperty("performance", se::Value(performanceObj));

    se::ScriptEngine::getInstance()->clearException();

    se::ScriptEngine::getInstance()->addBeforeCleanupHook([]() {

    });

    se::ScriptEngine::getInstance()->addAfterCleanupHook([]() {
        gModuleCache.clear();

        SAFE_DEC_REF(__jsbObj);
        SAFE_DEC_REF(__glObj);
    });

    return true;
}
