LOCAL_PATH := $(call my-dir)

#======================================
# include $(CLEAR_VARS)
# LOCAL_MODULE := v8_inspector
# LOCAL_SRC_FILES := $(TARGET_ARCH_ABI)/v8/libinspector.a
# include $(PREBUILT_STATIC_LIBRARY)

#======================================
include $(CLEAR_VARS)
LOCAL_MODULE := v8_static
LOCAL_SRC_FILES := $(TARGET_ARCH_ABI)/libv8_monolith.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/../../include/android
 ifeq ($(TARGET_ARCH),arm64)
    LOCAL_EXPORT_CPPFLAGS := -DV8_COMPRESS_POINTERS
    LOCAL_EXPORT_CFLAGS := -DV8_COMPRESS_POINTERS
 endif


include $(PREBUILT_STATIC_LIBRARY)

#======================================
