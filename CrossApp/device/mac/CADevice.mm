

#include "CADevice.h"
#import <Foundation/Foundation.h>
#import <AddressBook/AddressBook.h>
#import "Reachability.h"
#import <SystemConfiguration/CaptiveNetwork.h>
//#import <MediaPlayer/MediaPlayer.h>
#import <AppKit/AppKit.h>
#include <regex>
#import "../../platform/mac/CCApplication.h"

NS_CC_BEGIN

const CAValueMap& CADevice::getSystemVersion()
{
    NSTask *task;
    task = [[NSTask alloc] init];
    [task setLaunchPath: @"/usr/bin/sw_vers"];
    
    NSArray *arguments;
    arguments = [NSArray arrayWithObjects: @"-productVersion", nil];
    [task setArguments: arguments];
    
    NSPipe *pipe;
    pipe = [NSPipe pipe];
    [task setStandardOutput: pipe];
    
    NSFileHandle *file;
    file = [pipe fileHandleForReading];
    
    [task launch];
    
    NSData *data;
    data = [file readDataToEndOfFile];
    
    NSString *string;
    string = [[NSString alloc] initWithData: data
                                   encoding: NSUTF8StringEncoding];
    
    CAValueMap valueMap;
    valueMap["os"] = "OS X";
    valueMap["version"] = [string UTF8String];
    return valueMap;
}

const std::string& CADevice::getAppVersion()
{
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *app_Version = [infoDictionary objectForKey:@"CFBundleVersion"];
    return [app_Version UTF8String];
}

const std::string CADevice::getCurrentTimeZone()
{
    NSTimeZone *localZone = [NSTimeZone localTimeZone];
    return [localZone.name UTF8String];
}

void CADevice::setScreenBrightness(float brightness)
{
    
}

float CADevice::getScreenBrightness()
{
    return 0;
}

CADevice::NetWorkData CADevice::getNetWorkType()
{
    return CADevice::NetWorkData::Wifi;
}

bool CADevice::isNetWorkAvailble()
{
    return true;
}

//CADevice::WifiDate CADevice::getWifiConnectionInfo()
//{
//    CADevice::WifiDate wifiInfo;
//
//    return wifiInfo;
//}

void CADevice::setVolume(float sender, CADevice::VolumeData type)
{
}

float CADevice::getVolume(CADevice::VolumeData type)
{
    return 0;
}

float CADevice::getBatteryLevel()
{
    return 1.0f;
}

void CADevice::sendLocalNotification(const char* title, const char* content,int time)
{
}

void CADevice::openUrl(const std::string &url, std::function<void(bool)> callback)
{
    bool isOpen = [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:[NSString stringWithUTF8String:url.c_str()]]];
    if (callback)
    {
        callback(isOpen);
    }
}

void CADevice::setIdleTimerDisabled(bool isIdleTimerDisabled)
{
}


void CADevice::setLocaleIdentifiers(const std::string& idf){
    CCApplication::sharedApplication()->setLocaleIdentifiers(idf);
}


std::string CADevice::getTimeWihtTimeZone(const std::string& currentTimeZone,const std::string& timeStr,const std::string& convertTimeZone){
    // yyyy-MM-dd 长度10   HH:mm 长度5  HH:mm:ss长度8  yyyy-MM-dd HH:mm:ss长度19   yyyy-MM-dd HH:mm长度16
    int type = -1;
    std::regex reg;
    std::regex reg0("(\\d{4})-(0\\d{1}|1[0-2])-(0\\d{1}|[12]\\d{1}|3[01])\\s(0\\d{1}|1\\d{1}|2[0-3]):[0-5]\\d{1}:([0-5]\\d{1})");//yyyy-mm-dd hh:mm:ss
    std::regex reg1("(\\d{4})-(0\\d{1}|1[0-2])-(0\\d{1}|[12]\\d{1}|3[01])");//yyyy-mm-dd
    std::regex reg2("(0\\d{1}|1\\d{1}|2[0-3]):[0-5]\\d{1}:([0-5]\\d{1})");//hh:mm:ss
    std::regex reg3("(\\d{4})-(0\\d{1}|1[0-2])-(0\\d{1}|[12]\\d{1}|3[01])\\s(0\\d{1}|1\\d{1}|2[0-3]):[0-5]\\d{1}");//yyyy-mm-dd hh:mm
    std::regex reg4("(0\\d{1}|1\\d{1}|2[0-3]):[0-5]\\d{1}");//HH:mm
    switch (timeStr.length()) {
        case 10:
            reg = reg1;
            type = 1;
            break;
        case 16:
            type = 2;
            reg = reg3;
            break;
        case 19:
            type = 3;
            reg = reg0;
            break;
        case 8:
            type = 4;
            reg = reg2;
            break;
        case 5:
            type = 5;
            reg = reg4;
            break;
        default:
            break;
    }

    
    bool isDate = std::regex_match(timeStr, reg);
    
    if (!isDate){
        NSLog(@"is not Date");
        return timeStr;
    }
    
    NSString *time = [NSString stringWithCString:timeStr.c_str() encoding:NSUTF8StringEncoding];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    NSString *formatStr = @"yyyy-MM-dd HH:mm:ss";
    switch (type) {
        case 1:
            formatStr = @"yyyy-MM-dd";
            break;
        case 2:
            formatStr = @"yyyy-MM-dd HH:mm";
            break;
        case 3:
            formatStr = @"yyyy-MM-dd HH:mm:ss";
            break;
        case 4:
            formatStr = @"HH:mm:ss";
            break;
        case 5:
            formatStr = @"HH:mm";
            break;
        default:
            formatStr = @"yyyy-MM-dd HH:mm:ss";
            break;
    }
    format.dateFormat = formatStr;
    
    format.timeZone = [NSTimeZone timeZoneWithName:[NSString stringWithCString:currentTimeZone.c_str() encoding:NSUTF8StringEncoding]];

    NSDate* date = [format dateFromString:time] ;
    //[NSDate dateWithTimeIntervalSince1970:500];// 获得时间对象
//    @"Asia/Kashgar"
    NSDateFormatter *forMatter = [[NSDateFormatter alloc] init];
    NSTimeZone *zone = [NSTimeZone timeZoneWithName:[NSString stringWithCString:convertTimeZone.c_str() encoding:NSUTF8StringEncoding]]; // 获得当前系统的时区

    [forMatter setTimeZone:zone];// 设定时区

    [forMatter setDateFormat:formatStr];

    NSString* dateStr = [forMatter stringFromDate:date];
    return [dateStr cStringUsingEncoding:NSUTF8StringEncoding];
}

NS_CC_END
