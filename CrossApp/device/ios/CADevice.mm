//
//  CADevice.cpp
//  CrossApp
//
//  Created by 秦乐 on 2017/2/21.
//  Copyright © 2017年 CossApp-x. All rights reserved.
//

#include "CADevice.h"
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "CrossAppReachability.h"
#import <SystemConfiguration/CaptiveNetwork.h>
#import <MediaPlayer/MPMusicPlayerController.h>
#import <MediaPlayer/MediaPlayer.h>
#include <regex>
#import "Availability.h"
#import <UserNotifications/UserNotifications.h>
#import <UserNotificationsUI/UserNotificationsUI.h>
#import "../../platform/ios/CCApplication.h"


NS_CC_BEGIN

const CAValueMap& CADevice::getSystemVersion()
{
    CAValueMap valueMap;
    valueMap["os"] = "iOS";
    valueMap["version"] = [[[UIDevice currentDevice]systemVersion] UTF8String];
    return valueMap;
}

const std::string& CADevice::getAppVersion()
{
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *app_Version = [infoDictionary objectForKey:@"CFBundleVersion"];
    return [app_Version UTF8String];
}

const std::string CADevice::getCurrentTimeZone()
{
    NSTimeZone *localZone = [NSTimeZone localTimeZone];
    return [localZone.name UTF8String];
}

void CADevice::setScreenBrightness(float brightness)
{
    float brightnessPer = MIN(brightness, 1.0);
    [[UIScreen mainScreen] setBrightness:brightnessPer];
}

float CADevice::getScreenBrightness()
{
    float percent = [[UIScreen mainScreen] brightness];
    return percent;
}

CADevice::NetWorkData CADevice::getNetWorkType()
{
    CADevice::NetWorkData networkType = CADevice::NetWorkData::None;
    CrossAppReachability *reach = [CrossAppReachability reachabilityWithHostname:@"www.baidu.com"];
    switch ([reach currentReachabilityStatus])
    {
        case NotReachable:
            networkType = CADevice::NetWorkData::None;
            break;
        case ReachableViaWWAN:
            networkType = CADevice::NetWorkData::ReachableViaWWAN;
            break;
        case ReachableViaWiFi:
            networkType = CADevice::NetWorkData::Wifi;
            break;
    }
    return networkType;
}

bool CADevice::isNetWorkAvailble()
{
    CrossAppReachability *reach = [CrossAppReachability reachabilityWithHostname:@"www.baidu.com"];
    return [reach isReachable];
}

//CADevice::WifiDate CADevice::getWifiConnectionInfo()
//{
//    NSArray *array = (id)CNCopySupportedInterfaces();
//    id info = nil;
//    for (NSString *ifnam in array) {
//        info = (id)CNCopyCurrentNetworkInfo((CFStringRef)ifnam);
//        if (info && [info count]) {
//            
//            break;
//        }
//        [info release];
//    }
//    [array release];
//    NSDictionary *dic = info;
//    NSString *ssid = [[dic objectForKey:@"SSID"] lowercaseString];
//    NSString *mac = [[dic objectForKey:@"BSSID"] lowercaseString];
//    
//    CADevice::WifiDate wifiInfo;
//    wifiInfo.ssid = [ssid cStringUsingEncoding:NSUTF8StringEncoding];
//    wifiInfo.mac = [mac cStringUsingEncoding:NSUTF8StringEncoding];
//    wifiInfo.level = 0;
//    return wifiInfo;
//}

void CADevice::setVolume(float sender, CADevice::VolumeData type)
{
    [MPMusicPlayerController applicationMusicPlayer].volume = sender;
}

float CADevice::getVolume(CADevice::VolumeData type)
{
    return [MPMusicPlayerController applicationMusicPlayer].volume;
}

float CADevice::getBatteryLevel()
{
    [UIDevice currentDevice].batteryMonitoringEnabled = YES;
    return [UIDevice currentDevice].batteryLevel;
}

void CADevice::sendLocalNotification(const char* title, const char* message, int leftMessage)
{
    float version = [UIDevice currentDevice].systemVersion.floatValue;
    
    if (version >= 10)
    {
        //第二步：新建通知内容对象
        UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
        //content.title = @"手持矿机";
        content.subtitle = [NSString stringWithUTF8String:title];
        content.body = [NSString stringWithUTF8String:message];
        content.badge = @0;
        UNNotificationSound *sound = [UNNotificationSound soundNamed:@"caodi.m4a"];
        content.sound = sound;
        
        //第三步：通知触发机制。（重复提醒，时间间隔要大于60s）
        UNTimeIntervalNotificationTrigger *trigger1 = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:1 repeats:NO];
        
        //第四步：创建UNNotificationRequest通知请求对象
        NSString *requertIdentifier = @"RequestIdentifier";
        UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:requertIdentifier content:content trigger:trigger1];
        
        //第五步：将通知加到通知中心
        [[UNUserNotificationCenter currentNotificationCenter] addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
            NSLog(@"Error:%@",error);
            
        }];
    }
    else
    {
        // 1.创建通知
        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
        // 2.设置通知的必选参数
        // 设置通知显示的标题
        localNotification.alertTitle = [NSString stringWithUTF8String:title];
        // 设置通知显示的内容
        localNotification.alertBody = [NSString stringWithUTF8String:message];
        // 设置通知的发送时间,单位秒
        localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
        //解锁滑动时的事件
        localNotification.alertAction = @"";
        //收到通知时App icon的角标
        localNotification.applicationIconBadgeNumber = leftMessage;
        //推送是带的声音提醒，设置默认的字段为UILocalNotificationDefaultSoundName
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        // 3.发送通知(? : 根据项目需要使用)
        // 方式二: 立即发送通知
        [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
    }
}

void CADevice::openUrl(const std::string &url, std::function<void(bool)> callback)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
    bool isOpen = [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:[NSString stringWithUTF8String:url.c_str()]]];
    if (callback)
    {
        callback(isOpen);
    }
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithUTF8String:url.c_str()]] options:@{} completionHandler:^(BOOL success)
    {
        if (callback)
        {
            callback((success == YES) ? true : false);
        }
    }];
#endif
    
}

void CADevice::setIdleTimerDisabled(bool isIdleTimerDisabled)
{
    [[UIApplication sharedApplication] setIdleTimerDisabled:isIdleTimerDisabled];
}

void CADevice::setLocaleIdentifiers(const std::string &idf){
    CCApplication::sharedApplication()->setLocaleIdentifiers(idf);
}

std::string CADevice::getTimeWihtTimeZone(const std::string& currentTimeZone,const std::string& timeStr,const std::string& convertTimeZone){
    // yyyy-MM-dd 长度10   HH:mm 长度5  HH:mm:ss长度8  yyyy-MM-dd HH:mm:ss长度19   yyyy-MM-dd HH:mm长度16
    int type = -1;
    std::regex reg;
    std::regex reg0("(\\d{4})-(0\\d{1}|1[0-2])-(0\\d{1}|[12]\\d{1}|3[01])\\s(0\\d{1}|1\\d{1}|2[0-3]):[0-5]\\d{1}:([0-5]\\d{1})");//yyyy-mm-dd hh:mm:ss
    std::regex reg1("(\\d{4})-(0\\d{1}|1[0-2])-(0\\d{1}|[12]\\d{1}|3[01])");//yyyy-mm-dd
    std::regex reg2("(0\\d{1}|1\\d{1}|2[0-3]):[0-5]\\d{1}:([0-5]\\d{1})");//hh:mm:ss
    std::regex reg3("(\\d{4})-(0\\d{1}|1[0-2])-(0\\d{1}|[12]\\d{1}|3[01])\\s(0\\d{1}|1\\d{1}|2[0-3]):[0-5]\\d{1}");//yyyy-mm-dd hh:mm
    std::regex reg4("(0\\d{1}|1\\d{1}|2[0-3]):[0-5]\\d{1}");//HH:mm
    switch (timeStr.length()) {
        case 10:
            reg = reg1;
            type = 1;
            break;
        case 16:
            type = 2;
            reg = reg3;
            break;
        case 19:
            type = 3;
            reg = reg0;
            break;
        case 8:
            type = 4;
            reg = reg2;
            break;
        case 5:
            type = 5;
            reg = reg4;
            break;
        default:
            break;
    }

    
    bool isDate = std::regex_match(timeStr, reg);
    
    if (!isDate){
        NSLog(@"is not Date");
        return timeStr;
    }
    
    NSString *time = [NSString stringWithCString:timeStr.c_str() encoding:NSUTF8StringEncoding];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    NSString *formatStr = @"yyyy-MM-dd HH:mm:ss";
    switch (type) {
        case 1:
            formatStr = @"yyyy-MM-dd";
            break;
        case 2:
            formatStr = @"yyyy-MM-dd HH:mm";
            break;
        case 3:
            formatStr = @"yyyy-MM-dd HH:mm:ss";
            break;
        case 4:
            formatStr = @"HH:mm:ss";
            break;
        case 5:
            formatStr = @"HH:mm";
            break;
        default:
            formatStr = @"yyyy-MM-dd HH:mm:ss";
            break;
    }
    format.dateFormat = formatStr;
    
    format.timeZone = [NSTimeZone timeZoneWithName:[NSString stringWithCString:currentTimeZone.c_str() encoding:NSUTF8StringEncoding]];

    NSDate* date = [format dateFromString:time] ;
    //[NSDate dateWithTimeIntervalSince1970:500];// 获得时间对象
//    @"Asia/Kashgar"
    NSDateFormatter *forMatter = [[NSDateFormatter alloc] init];
    NSTimeZone *zone = [NSTimeZone timeZoneWithName:[NSString stringWithCString:convertTimeZone.c_str() encoding:NSUTF8StringEncoding]]; // 获得当前系统的时区

    [forMatter setTimeZone:zone];// 设定时区

    [forMatter setDateFormat:formatStr];

    NSString* dateStr = [forMatter stringFromDate:date];
    return [dateStr cStringUsingEncoding:NSUTF8StringEncoding];
}

NS_CC_END
