//
//  CASlider.cpp
//  project
//
//  Created by lh on 14-4-16.
//  Copyright (c) 2014 http://9miao.com All rights reserved.
//

#include "CAProgress.h"
#include "view/CAScale9ImageView.h"
#include "basics/CAApplication.h"
#include "basics/CAScheduler.h"
#include "basics/CAPointExtension.h"
#include "basics/CAApplication.h"
#include "animation/CAViewAnimation.h"
#include "support/CAThemeManager.h"
#include "support/ccUtils.h"
NS_CC_BEGIN

CAProgress::CAProgress(bool isScale9)
:m_fProgress(0.0f)
,m_cProgressTintColor(CAColor4B::WHITE)
,m_cProgressTrackColor(CAColor4B::WHITE)
,m_pProgressTintImage(nullptr)
,m_pProgressTrackImage(nullptr)
,m_pProgressImageView(nullptr)
,m_pTarckImageView(nullptr)
,m_iMarginLeft(0)
,m_iMarginRight(0)
,m_bIsScale9(isScale9)
{
    this->setHaveNextResponder(true);
    this->setMultipleTouchEnabled(true);
    
    m_pIndicator = CAView::create();
	m_pIndicator->setFrameOrigin(DPointZero);
    this->addSubview(m_pIndicator);
}

CAProgress::~CAProgress()
{
	CC_SAFE_RELEASE_NULL(m_pProgressTrackImage);
	CC_SAFE_RELEASE_NULL(m_pProgressTintImage);
}

CAProgress* CAProgress::create(bool isScale9)
{
    CAProgress *progress = new CAProgress(isScale9);
    if (progress && progress->init())
    {
        progress->autorelease();
        return progress;
    }
    CC_SAFE_DELETE(progress);
    return NULL;
}

CAProgress* CAProgress::createWithFrame(const DRect& rect, bool isScale9)
{
    CAProgress *progress = new CAProgress(isScale9);
    if (progress && progress->initWithFrame(rect))
    {
        progress->autorelease();
        return progress;
    }
    CC_SAFE_DELETE(progress);
    return NULL;
}

CAProgress* CAProgress::createWithCenter(const DRect& rect, bool isScale9)
{
    CAProgress *progress = new CAProgress(isScale9);
    if (progress && progress->initWithCenter(rect))
    {
        progress->autorelease();
        return progress;
    }
    CC_SAFE_DELETE(progress);
    return NULL;
}

CAProgress* CAProgress::createWithLayout(const DLayout& layout, bool isScale9)
{
    CAProgress *progress = new CAProgress(isScale9);
    if (progress && progress->initWithLayout(layout))
    {
        progress->autorelease();
        return progress;
    }
    CC_SAFE_DELETE(progress);
    return NULL;
}

bool CAProgress::init()
{
    if (!CAView::init())
    {
        return false;
    }
    CAView::setColor(CAColor4B::CLEAR);
    
    if (m_bIsScale9)
    {
        m_pTarckImageView = CAScale9ImageView::createWithLayout(DLayoutFill);
        m_pProgressImageView = CAScale9ImageView::createWithLayout(DLayoutFill);
    }
    else
    {
        m_pTarckImageView = CAImageView::createWithLayout(DLayoutFill);
        m_pProgressImageView = CAImageView::createWithLayout(DLayoutFill);
    }
    this->insertSubview(m_pTarckImageView, -1);
    this->addSubview(m_pProgressImageView);
    
    this->setProgress(0);

    return true;
}

void CAProgress::onEnterTransitionDidFinish()
{ 
    CAView::onEnterTransitionDidFinish();
    const CAThemeManager::stringMap& map = GETINSTANCE_THEMEMAP("CAProgress");
    if (m_pProgressTrackImage == NULL)
    {
        this->setProgressTrackImage(CAImage::create(map.at("trackImage")));
    }
    
    if (m_pProgressTintImage == NULL)
    {
        this->setProgressTintImage(CAImage::create(map.at("tintImage")));
    }
}

void CAProgress::onExitTransitionDidStart()
{
	CAView::onExitTransitionDidStart();
}

void CAProgress::setProgressTintColor(const CAColor4B &var)
{
    m_cProgressTintColor = var;
    if (m_bIsScale9)
    {
        ((CAScale9ImageView*)m_pProgressImageView)->setColor(m_cProgressTintColor);
    }
    else
    {
        ((CAImageView*)m_pProgressImageView)->setColor(m_cProgressTintColor);
    }
}

const CAColor4B& CAProgress::getProgressTintColor()
{
    return m_cProgressTintColor;
}

void CAProgress::setProgressTintImage(CrossApp::CAImage *var)
{
    CC_SAFE_RETAIN(var);
    CC_SAFE_RELEASE_NULL(m_pProgressTintImage);
    m_pProgressTintImage = var;
    if (m_bIsScale9)
    {
        ((CAScale9ImageView*)m_pProgressImageView)->setImage(m_pProgressTintImage);
    }
    else
    {
        ((CAImageView*)m_pProgressImageView)->setImage(m_pProgressTintImage);
    }
}

CAImage* CAProgress::getProgressTintImage()
{
    return m_pProgressTintImage;
}

void CAProgress::setProgressTrackColor(const CAColor4B &var)
{
    m_cProgressTrackColor = var;
    m_pTarckImageView->setColor(m_cProgressTrackColor);
}

const CAColor4B& CAProgress::getProgressTrackColor()
{
    return m_cProgressTrackColor;
}

void CAProgress::setProgressTrackImage(CrossApp::CAImage *var)
{
    CC_SAFE_RETAIN(var);
    CC_SAFE_RELEASE_NULL(m_pProgressTrackImage);
    m_pProgressTrackImage = var;
    if (m_bIsScale9)
    {
        ((CAScale9ImageView*)m_pTarckImageView)->setImage(m_pProgressTrackImage);
    }
    else
    {
        ((CAImageView*)m_pTarckImageView)->setImage(m_pProgressTrackImage);
    }
}

CAImage* CAProgress::getProgressTrackImage()
{
    return m_pProgressTrackImage;
}

void CAProgress::setColor(const CAColor4B& color)
{
	setProgressTintColor(color);
	setProgressTrackColor(color);
}

void CAProgress::setProgress(float progress, bool animated)
{
	progress = MIN(1.0f, progress);
	progress = MAX(0.0f, progress);
    
	DPoint point = DPoint(m_iMarginLeft + (m_obContentSize.width - m_iMarginLeft - m_iMarginRight) * progress, m_obContentSize.height * 0.5);

	if (animated)
	{
        this->animatedBegin();
		float time = fabsf(progress - m_fProgress) * 0.5f;
        CAViewAnimation::removeAnimations("CAProgress::Animation");
        CAViewAnimation::beginAnimations("CAProgress::Animation");
        CAViewAnimation::setAnimationDuration(time);
        CAViewAnimation::setAnimationDidStopSelector(std::bind(&CAProgress::animatedFinish, this));
        m_pIndicator->setFrameOrigin(point);
        CAViewAnimation::commitAnimations();
	}
	else
	{
        m_fProgress = progress;
		m_pIndicator->setFrameOrigin(point);

		this->update(0);
	}
}

float CAProgress::getProgress()
{
	return m_fProgress;
}

void CAProgress::update(float dt)
{
    m_fProgress = m_pIndicator->getFrameOrigin().x / m_obContentSize.width;
    
	DRect rect = DRect(0, 0, m_pIndicator->getFrameOrigin().x, m_obContentSize.height);

    if (m_bIsScale9)
    {
        ((CAScale9ImageView*)m_pProgressImageView)->setFrame(rect);
    }
    else
    {
        ((CAImageView*)m_pProgressImageView)->setFrame(rect);
        ((CAImageView*)m_pProgressImageView)->setImageRect(rect);
    }
}

void CAProgress::animatedBegin()
{
    this->retain();
	CAScheduler::getScheduler()->scheduleUpdate(this, CAScheduler::PRIORITY_SYSTEM, false);
}

void CAProgress::animatedFinish()
{
    this->update(0);
    CAScheduler::getScheduler()->unscheduleUpdate(this);
    this->release();
}

void CAProgress::setContentSize(const DSize & var)
{
    DSize size = var;
    if (m_bRecSpe)
    {
        const CAThemeManager::stringMap& map = GETINSTANCE_THEMEMAP("CAProgress");
        int h = atoi(map.at("height").c_str());
        size.height = (h == 0) ? size.height : h;
    }
    CAView::setContentSize(size);
    
    m_pTarckImageView->setFrame(this->getBounds());
    DRect rect = DRect(0, 0, m_pIndicator->getFrameOrigin().x, this->getBounds().size.height);
    if (m_bIsScale9)
    {
        ((CAScale9ImageView*)m_pProgressImageView)->setFrame(rect);
    }
    else
    {
        ((CAImageView*)m_pProgressImageView)->setFrame(rect);
        ((CAImageView*)m_pProgressImageView)->setImageRect(rect);
    }
    this->setProgress(m_fProgress);
}

CAView* CAProgress::getIndicatorView()
{
    return m_pIndicator;
}

void CAProgress::setMarginLeft(int var)
{
    m_iMarginLeft = var;
    this->setProgress(m_fProgress);
}

int CAProgress::getMarginLeft()
{
    return m_iMarginLeft;
}

void CAProgress::setMarginRight(int var)
{
    m_iMarginRight = var;
    this->setProgress(m_fProgress);
}

int CAProgress::getMarginRight()
{
    return m_iMarginRight;
}

NS_CC_END
