//
//  CASlider.h
//  project
//
//  Created by lh on 14-4-16.
//  Copyright (c) 2014 http://9miao.com All rights reserved.
//

#ifndef __project__CASlider__
#define __project__CASlider__

#include <iostream>
#include "control/CAControl.h"
#include "view/CAScale9ImageView.h"
#include "view/CARenderImage.h"

NS_CC_BEGIN

class CC_DLL CAProgress : public CAControl
{
public:
    
    CAProgress(bool isScale9 = true);
    
    virtual ~CAProgress();
    
    static CAProgress* create(bool isScale9 = true);
    static CAProgress* createWithFrame(const DRect& rect, bool isScale9 = true);
    static CAProgress* createWithCenter(const DRect& rect, bool isScale9 = true);
    static CAProgress* createWithLayout(const DLayout& layout, bool isScale9 = true);
    
    bool init();
    
	virtual void setColor(const CAColor4B& color);
    
	void setProgress(float progress, bool animated = false);
	float getProgress();
    
    CAView* getIndicatorView();

public:
    
    CC_PROPERTY_PASS_BY_REF(CAColor4B, m_cProgressTintColor, ProgressTintColor);
    
    CC_PROPERTY_PASS_BY_REF(CAColor4B, m_cProgressTrackColor, ProgressTrackColor);
    
	CC_PROPERTY(CAImage*, m_pProgressTintImage, ProgressTintImage);
    
	CC_PROPERTY(CAImage*, m_pProgressTrackImage, ProgressTrackImage);
    
    CC_PROPERTY(int, m_iMarginLeft, MarginLeft);
    
    CC_PROPERTY(int, m_iMarginRight, MarginRight);

public:
    
    virtual void onEnterTransitionDidFinish();
    virtual void onExitTransitionDidStart();
    
protected:

    void setContentSize(const DSize & var);
    
	void update(float dt);
    
	void animatedBegin();

	void animatedFinish();

    CAView* m_pTarckImageView;
    
    CAView* m_pProgressImageView;
    
	float m_fProgress;

    CAView* m_pIndicator;
    
    bool m_bIsScale9;
};

NS_CC_END
#endif /* defined(__project__CASlider__) */
