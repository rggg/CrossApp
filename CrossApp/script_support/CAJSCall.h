#ifndef  _JS_CALL_
#define  _JS_CALL_

#include <stdio.h>
#include "../basics/CAObject.h"
#include "basics/CAValue.h"
class CAJSCallReceive;

class CAJSCall : public CrossApp::CAObject
{
public:

    typedef std::function< CrossApp::CAValue(const CrossApp::CAValue& parameter) > Call;

    typedef std::function< void(const CrossApp::CAValue& parameter, CAJSCallReceive* receive) > CallAsync;

public:
    
    static CAJSCall* getInstance();
    
    static void destroyInstance();
    
    void listening_call(const std::string& key, const Call& function);

    CrossApp::CAValue call(const std::string& key, const CrossApp::CAValue& parameter);
    
    void listening_call_async(const std::string& key, const CallAsync& function);

    CAJSCallReceive* call_async(const std::string& key, const CrossApp::CAValue& parameter);

private:
    
    CAJSCall();
    
    virtual ~CAJSCall();
    
    std::map<std::string, Call> m_mCalls;
    
    std::map<std::string, CallAsync> m_mCallAsyncs;
    
    std::map<std::string, CAJSCallReceive*> m_mReceive;
};

class CAJSCallReceive : public CrossApp::CAObject
{
public:

    typedef std::function< void(const CrossApp::CAValue&) > Receive;

public:
    
    CAJSCallReceive();
    
    virtual ~CAJSCallReceive();
    
    void onReceive(const Receive& receive);
    
    void receive(CrossApp::CAValue result);
    
protected:
    
    void onRunning(const std::function<void()>& callback);

    void onReceiveEnd(const std::function<void()>& callback);

    friend class CAJSCall;
    
private:
    
    Receive m_oReceive;
    
    std::function<void()> m_oOnRunning;
    
    std::function<void()> m_oOnReceiveEnd;

};

#endif // _JS_CALL_

