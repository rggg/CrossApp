#include "CAJSCall.h"

static CAJSCall* _jsCall = nullptr;

CAJSCall* CAJSCall::getInstance()
{
    if (_jsCall == NULL)
    {
        _jsCall = new CAJSCall();
    }
    return _jsCall;
}

void CAJSCall::destroyInstance()
{
    if (_jsCall)
    {
        delete _jsCall;
        _jsCall = nullptr;
    }
}

CAJSCall::CAJSCall()
{
    
}

CAJSCall::~CAJSCall()
{
    for (auto it : m_mReceive)
    {
        it.second->release();
    }
    m_mReceive.clear();
}

void CAJSCall::listening_call(const std::string& key, const Call& function)
{
    m_mCalls[key] = function;
}

CrossApp::CAValue CAJSCall::call(const std::string& key, const CrossApp::CAValue& parameter)
{
    if (m_mCalls.find(key) == m_mCalls.end())
        return CrossApp::CAValue::Null;
    return m_mCalls[key](parameter);
}

void CAJSCall::listening_call_async(const std::string& key, const CallAsync& function)
{
    m_mCallAsyncs[key] = function;
}

CAJSCallReceive* CAJSCall::call_async(const std::string& key, const CrossApp::CAValue& parameter)
{
    CAJSCallReceive* receive = new CAJSCallReceive();
    m_mReceive[key] = receive;
    receive->onRunning([=](){
        m_mCallAsyncs[key](parameter, receive);
        
    });
    receive->onReceiveEnd([=](){
        m_mReceive.erase(key);
        receive->release();
    });
    return receive;
}

CAJSCallReceive::CAJSCallReceive()
: m_oReceive(nullptr)
, m_oOnRunning(nullptr)
{
    
}

CAJSCallReceive::~CAJSCallReceive()
{
    
}

void CAJSCallReceive::onRunning(const std::function<void()>& callback)
{
    m_oOnRunning = callback;
}

void CAJSCallReceive::onReceive(const Receive& receive)
{
    m_oReceive = receive;
    if (m_oOnRunning) m_oOnRunning();
}

void CAJSCallReceive::onReceiveEnd(const std::function<void()>& callback)
{
    m_oOnReceiveEnd = callback;
}

void CAJSCallReceive::receive(CrossApp::CAValue result)
{
    if (m_oReceive)
    {
        m_oReceive(result);
        m_oReceive = nullptr;
        m_oOnReceiveEnd();
    }
}



