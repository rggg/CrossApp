package org.CrossApp.lib;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.webkit.ValueCallback;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Author: liuguoyan
 * DateTime: 2022-02-24  23:21
 * Company: http://www.everjiankang.com.cn
 * Illustration:
 */
public class CADateTimePicker extends View {

    /**
     * 时间选择样式
     */
    public enum DatePickerMode{
        //0.Time 时间类型 1.Date日期类型  2.DateAndTime 日期和时间类型 3.CountDownTimer倒计时类型
        Time(0),
        Date(1),
        DateAndTime(2);

        private int value;
        DatePickerMode(int value) {
            this.value = value;
        }
    }
    public static Boolean isShowing = false;
    /** 标题，确定，取消 */
    private String title,submitStr,cancelStr ;
    /** 样式类型 */
    private DatePickerMode mode = DatePickerMode.DateAndTime ;
    /** 时区 */
    private String timeZome ;
    /** 结果回调 */
    private ValueCallback<String> resultCallback,cancelCallback;

    /** activityContext */
    private Context mContext ;
    /** view */
    private ViewGroup rootView ;
    private View view ;
    private View backView ;

    /** 标题*/
    private TextView pickertitle ;
    /** 日期容器，时间容器*/
    private RelativeLayout calenderlayout,timepickerlayout ;
    //用于遮挡日期控件，防止选择时间时被点击。
    private View datapicker_shadow ;

    private CalendarView calendarView ;
    private TimePicker timePicker ;

    //
    private Button datetime_shower,datecancel,dateconfirm;
    private Button timecancel,timeconfirm ;

    private int initHour,initMinutes;
    private String selectedDateStr ;

    public CADateTimePicker(Context context,String title,String submitStr,String cancelStr) {
        this(context,null);
        this.mContext = context ;
        this.title = title !=null ? title : "请选择日期";
        this.submitStr = submitStr!=null ? submitStr :"确定" ;
        this.cancelStr = cancelStr!=null ? cancelStr : "取消" ;
        this.initView();
    }

    public CADateTimePicker(Context context, @Nullable AttributeSet attrs) {
        this(context,attrs, R.style.AppTheme);
    }

    public CADateTimePicker(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs,defStyleAttr);
    }


    public static CADateTimePicker create(Context context , String title,String submitStr,String cancelStr){
        CADateTimePicker picker = new CADateTimePicker(context,title,submitStr,cancelStr) ;
        return picker ;
    }

    /**
     * 设置默认的日期和时间，如果只选择日期，为"2020-02-01 00:00"即可
     * 设置日期 格式：yyyy-MM-dd HH:mm
     * @param date
     */
    public void setDate(String date){

        Calendar calendar = Calendar.getInstance();
        this.datetime_shower.setText(calendar.get(Calendar.HOUR_OF_DAY)+":"+calendar.get(Calendar.MINUTE));

        if(date !=null) {
            long timelong = 0;
            if (date.compareTo("") == 0){
                timelong = calendar.getTimeInMillis();
            }else{
                timelong = Long.parseLong(this.date2TimeStamp(date, "yyyy-MM-dd HH:mm")) * 1000L;

                calendar.setTimeInMillis(timelong);
            }


            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int minutes = calendar.get(Calendar.MINUTE);

            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
                this.timePicker.setHour(hour);
                this.timePicker.setMinute(minutes);
                this.initHour = hour ;
                this.initMinutes = minutes ;
            }
            this.calendarView.setDate(timelong);
            this.datetime_shower.setText(calendar.get(Calendar.HOUR_OF_DAY) + ":" + calendar.get(Calendar.MINUTE));

        }

    }

    /**
     * 设置日期选择模式
     * @param mode
     */
    public void setDatePickerMode(DatePickerMode mode){
        this.mode = mode ;
        if (this.mode == DatePickerMode.Date){
            this.datetime_shower.setVisibility(View.GONE);
        }
    }

    public void setTimeZone(String timeZome){
        this.timeZome = timeZome ;
        //TODO
    }

    /**
     * 设置回调，如果是日期类型，会返回 yyyy-MM-dd 如果是日期&时间格式，返回 yyyy-MM-dd HH:mm
     * 取消不会触发回调
     * @param callback
     */
    public void setCallBack(ValueCallback<String> callback){
        this.resultCallback = callback ;
    }
    public void setCancelCallBack(ValueCallback<String> callback){
        this.cancelCallback = callback ;
    }

    public void show(){

        if(this.mContext instanceof Activity){
            Activity activity = (Activity)this.mContext ;
            FrameLayout root = (FrameLayout )activity.getWindow().getDecorView();
            RelativeLayout rlayout = new RelativeLayout(mContext) ;
            this.rootView = root ;
            rlayout.setBackgroundColor(Color.parseColor("#66000000"));
            FrameLayout.LayoutParams rootp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            rlayout.setClickable(true);
            root.addView(rlayout , rootp);
            this.backView = rlayout ;

            RelativeLayout.LayoutParams rp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT) ;
            rp.addRule(RelativeLayout.CENTER_IN_PARENT);
            rp.leftMargin = this.dpToPx(20) ;
            rp.rightMargin = this.dpToPx(20) ;
            rlayout.addView(this.view,rp);
            isShowing = true;
//            this.backView.startAnimation(this.alphaAnimation(true,null));

        }

    }

    /**
     * 关闭
     */
    public void dismiss(){
        if (this.cancelCallback!=null){
            this.cancelCallback.onReceiveValue("");
        }
        isShowing = false;
        CADateTimePicker.this.rootView.removeView(CADateTimePicker.this.backView);
//        this.backView.startAnimation(this.alphaAnimation(false,new ValueCallback<Boolean>(){
//            @Override
//            public void onReceiveValue(Boolean value) {
//                //CADateTimePicker.this.backView.setVisibility(View.GONE);
//
//            }
//        }));
    }

    //0.Automatic，1.Wheels，2.Compact，3.Inline
    public void setDatePickerStyle(){
        //TODO
    }

    private void initView(){
        this.view = View.inflate(mContext , R.layout.cadatepicker,null) ;
        this.calendarView = (CalendarView) this.view.findViewById(R.id.calenderview) ;
        this.timePicker = (TimePicker) this.view.findViewById(R.id.timepicker) ;
        this.calenderlayout  = (RelativeLayout) this.view.findViewById(R.id.calenderlayout) ;
        this.timepickerlayout = (RelativeLayout) this.view.findViewById(R.id.timepickerlayout) ;

        this.pickertitle = (TextView)this.view.findViewById(R.id.pickertitle) ;

        this.datetime_shower = (Button)this.view.findViewById(R.id.datetime_shower) ;
        this.datecancel = (Button)this.view.findViewById(R.id.datecancel) ;
        this.dateconfirm = (Button)this.view.findViewById(R.id.dateconfirm) ;
        this.timecancel = (Button)this.view.findViewById(R.id.timecancel) ;
        this.timeconfirm = (Button)this.view.findViewById(R.id.timeconfirm) ;
        this.datapicker_shadow = this.view.findViewById(R.id.datapicker_shadow) ;

        this.dateconfirm.setText(this.submitStr);
        this.timeconfirm.setText(this.submitStr);
        this.datecancel.setText(this.cancelStr);
        this.timecancel.setText(this.cancelStr);
        this.pickertitle.setText(this.title);

        timePicker.setIs24HourView(true);

        this.setListeners();

    }



    private void setListeners(){
        this.datetime_shower.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //只有日期时间同时选择时才有效
                if (CADateTimePicker.this.mode == DatePickerMode.DateAndTime){
                    CADateTimePicker.this.visibleTimePicker(true);
                }
            }
        });
        //时间确认
        this.timeconfirm.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //只有日期时间同时选择时才有效
                if (CADateTimePicker.this.mode == DatePickerMode.DateAndTime){
                    CADateTimePicker.this.visibleTimePicker(false);
                }
                CADateTimePicker.this.confirmTimePicker();
            }
        });
        //时间取消
        this.timecancel.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //只有日期时间同时选择时才有效
                if (CADateTimePicker.this.mode == DatePickerMode.DateAndTime){
                    CADateTimePicker.this.visibleTimePicker(false);
                }
                CADateTimePicker.this.resetTimePicker();
            }
        });
        //日期取消，
        this.datecancel.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                CADateTimePicker.this.dismiss();
            }
        });
        //日期确认
        this.dateconfirm.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                CADateTimePicker.this.onResultConfirm();
                CADateTimePicker.this.dismiss();
            }
        });
        //临听时间选择器的选择事件
        this.timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                String timeStr = CADateTimePicker.this.formateTime(hourOfDay,minute) ;
                CADateTimePicker.this.datetime_shower.setText(timeStr);
            }
        });
        this.calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                //yyyy-MM-dd

                CADateTimePicker.this.selectedDateStr = String.format("%04d-%02d-%02d",year,month+1,dayOfMonth);

            }
        });

    }

    /**
     * 最终确认，结果的方法
     */
    private void onResultConfirm(){

        long datemillion  = this.calendarView.getDate();
        String date = this.selectedDateStr!=null ? this.selectedDateStr :  timeStamp2Date(datemillion,"yyyy-MM-dd");

        if (this.resultCallback!=null){
            if(this.mode == DatePickerMode.DateAndTime){
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
                    int hour = this.timePicker.getHour();
                    int minutes = this.timePicker.getMinute();
                    String timeStr = this.formateTime(hour,minutes) ;
                    String result = String.format("%s %s",date , timeStr) ;
                    this.resultCallback.onReceiveValue(result);
                }

            }else if(this.mode == DatePickerMode.Date){
                this.resultCallback.onReceiveValue(date);
            }
        }

    }

    private void resetTimePicker(){
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
            this.timePicker.setHour(this.initHour);
            this.timePicker.setMinute(this.initMinutes);
        }
    }

    private void confirmTimePicker(){
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
            this.initHour = this.timePicker.getHour();
            this.initMinutes = this.timePicker.getMinute();
        }
    }

    private void visibleTimePicker(boolean show){
        this.timepickerlayout.setVisibility(show ? View.VISIBLE : View.GONE);
        this.datapicker_shadow.setVisibility(show ? View.VISIBLE : View.GONE);

//        this.timepickerlayout.startAnimation(this.alphaAnimation(show, null));
    }

    /**
     * 工具方法
     */


    private String formateTime(int hour,int minutes){
        String str = String.format("%02d:%02d", hour, minutes);
        return str ;
    }

    private int dpToPx(int dps) {
        return Math.round(mContext.getResources().getDisplayMetrics().density * dps);
    }


    private Animation alphaAnimation(boolean show,final ValueCallback<Boolean> callback){

        AlphaAnimation alphaAnimation = show ? new AlphaAnimation(0.0f,1.0f) : new AlphaAnimation(1.0f,0.0f);
        alphaAnimation.setDuration(200);
        alphaAnimation.setFillAfter(false);
        alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }
            @Override
            public void onAnimationEnd(Animation animation) {
                if(callback!=null){
                    callback.onReceiveValue(true);
                }
            }
            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        return alphaAnimation ;
    }

    //时间戳转换日期格式字符串
    public static String timeStamp2Date(long time, String format) {
        if (format == null || format.isEmpty()) {
            format = "yyyy-MM-dd HH:mm";
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(new Date(time));
    }

    //日期格式字符串转换时间戳
    public static String date2TimeStamp(String date, String format) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            return String.valueOf(sdf.parse(date).getTime() / 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

}
