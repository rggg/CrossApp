package org.CrossApp.lib;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.Toast;

import java.lang.reflect.Method;
import java.net.URI;
import java.nio.ByteBuffer;
import java.util.ArrayList;

//import android.webkit.SslErrorHandler;
//import android.webkit.WebChromeClient;
//import android.webkit.WebView;
//import android.webkit.WebViewClient;

public class CrossAppWebViewNative extends WebView {
    private static final String TAG = CrossAppWebViewHelper.class.getSimpleName();

    private int viewTag;
    private View videoView;
    private WebChromeClient.CustomViewCallback videoCallback;
    private String jsScheme;
    private String szWebViewRect;
    private ArrayList<String> userScriptDataList;
    private boolean injected;
    private int progress;
    FrameLayout.LayoutParams layoutParams;
    FrameLayout.LayoutParams hideLayoutParams;

    public CrossAppWebViewNative(Context context) {
        this(context, -1);

    }

    @SuppressLint("SetJavaScriptEnabled")
    public CrossAppWebViewNative(Context context, int viewTag) {
        super(context);
        this.viewTag = viewTag;
        this.jsScheme = "";
        this.szWebViewRect = "0-0-0-0";
        this.userScriptDataList = new ArrayList<String>();
        this.injected = false;
        this.layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT);
        this.layoutParams.gravity = Gravity.LEFT | Gravity.TOP;

        this.hideLayoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT);
        this.hideLayoutParams.gravity = Gravity.LEFT | Gravity.TOP;
        this.hideLayoutParams.leftMargin = 5000;
        this.hideLayoutParams.topMargin = 5000;
        this.hideLayoutParams.width = 0;
        this.hideLayoutParams.height = 0;

        this.setFocusable(true);
        this.setFocusableInTouchMode(true);

        this.getSettings().setAllowFileAccess(true);
        this.getSettings().setAllowContentAccess(true);
        this.getSettings().setJavaScriptEnabled(true);
        this.getSettings().setAppCacheEnabled (true);
        this.getSettings().setSupportZoom(true);
        this.getSettings().setBuiltInZoomControls(true);
        this.getSettings().setUseWideViewPort(true);
        this.getSettings().setDomStorageEnabled(true);
        this.getSettings().setLoadWithOverviewMode(true);

        this.addJavascriptInterface(new InJavaScriptLocalObj(), "local_obj");

        // `searchBoxJavaBridge_` has big security risk. http://jvn.jp/en/jp/JVN53768697
        try {
            Method method = this.getClass().getMethod("removeJavascriptInterface", new Class[]{String.class});
            method.invoke(this, "searchBoxJavaBridge_");
        } catch (Exception e) {
            Log.d(TAG, "This API level do not support `removeJavascriptInterface`");
        }

        this.setWebViewClient(new CrossAppWebViewClient());
        this.setWebChromeClient(new MyWebChromeClient());

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return CrossAppGLSurfaceView.getInstance().onKeyDown(keyCode, event);
        }
        return super.onKeyDown(keyCode, event);
    }

    public void setJavascriptInterfaceScheme(String scheme) {
        this.jsScheme = scheme != null ? scheme : "";
    }

    public void setProgress(int var)
    {
        this.progress = var;
        this.updateLayoutParams();
    }

    public void setInjected(boolean var) { this.injected = var; }

    public ArrayList<String> getUserScriptDataList() { return this.userScriptDataList; }

    public void addUserScriptData(String js) {

        if (this.injected)
        {
            this.evaluateJavascript(js, null);
        }
        else
        {
            this.userScriptDataList.add(js);
        }
    }

    public void setScalesPageToFit(boolean scalesPageToFit) {
        this.getSettings().setSupportZoom(scalesPageToFit);
    }

    private Bitmap bmp = null;
    private ByteBuffer imageData = null;

    public void getWebViewImage() {
        bmp = this.getDrawingCache();
        if (bmp != null && imageData == null) {
            imageData = ByteBuffer.allocate(bmp.getRowBytes() * bmp.getHeight());
            bmp.copyPixelsToBuffer(imageData);
            this.destroyDrawingCache();

            CrossAppActivity.getContext().runOnGLThread(new Runnable() {
                @Override
                public void run() {
                    CrossAppWebViewHelper.onSetByteArrayBuffer(viewTag, imageData.array(), bmp.getWidth(), bmp.getHeight());
                    imageData = null;
                }
            });
        }
    }

    class CrossAppWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, final String urlString) {

            URI uri = URI.create(urlString);
            if (uri != null && uri.getScheme().equals(jsScheme)) {
                CrossAppActivity.getContext().runOnGLThread(new Runnable() {
                    @Override
                    public void run() {
                        CrossAppWebViewHelper._onJsCallback(viewTag, urlString);
                    }
                });
                return true;
            }
            return CrossAppWebViewHelper._shouldStartLoading(viewTag, urlString);
        }

        @Override
        public void onPageFinished(final WebView view, final String url) {
            super.onPageFinished(view, url);

            CrossAppWebViewNative webview = (CrossAppWebViewNative)view;
            ArrayList<String> userScriptDataList = webview.getUserScriptDataList();
            for(int i=0; i<userScriptDataList.size(); i++)
            {
                String v = userScriptDataList.get(i);
                webview.evaluateJavascript(v, null);
            }
            webview.setInjected(true);

            CrossAppActivity.getContext().runOnGLThread(new Runnable() {
                @Override
                public void run() {
                    CrossAppWebViewHelper._didFinishLoading(viewTag, url);
                }
            });

        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, final String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            CrossAppActivity.getContext().runOnGLThread(new Runnable() {
                @Override
                public void run() {
                    CrossAppWebViewHelper._didFailLoading(viewTag, failingUrl);
                }
            });

        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {

            final AlertDialog.Builder builder = new AlertDialog.Builder(CrossAppActivity.getContext());
             String message = "SSL Certificate error.";
             switch (error.getPrimaryError()) {
                     case SslError.SSL_UNTRUSTED:
                             message = "The certificate authority is not trusted.";
                             break;
                     case SslError.SSL_EXPIRED:
                             message = "The certificate has expired.";
                             break;
                     case SslError.SSL_IDMISMATCH:
                             message = "The certificate Hostname mismatch.";
                             break;
                     case SslError.SSL_NOTYETVALID:
                             message = "The certificate is not yet valid.";
                             break;
                     case SslError.SSL_DATE_INVALID:
                             message = "The date of the certificate is invalid";
                             break;
                     case SslError.SSL_INVALID:
                         default:
                             message = "A generic error occurred";
                             break;
                 }
             message += " Do you want to continue anyway?";
             builder.setTitle("SSL Certificate Error");
             builder.setMessage(message);

             builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                 @Override
                 public void onClick(DialogInterface dialog, int which) {
                                         handler.proceed();
                 }
             });
             builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                 @Override
                 public void onClick(DialogInterface dialog, int which) {
                                         handler.cancel();
                 }
             });
             final AlertDialog dialog = builder.create();
             dialog.show();
        }

    }

    final class InJavaScriptLocalObj {
        @JavascriptInterface
        public void showSource(String html) {
            CrossAppWebViewHelper.didLoadHtmlSource(html);
            CrossAppWebViewHelper.s_bWaitGetHemlSource = false;
        }
    }

    public void updateLayoutParams()
    {
        if (this.progress == 100)
        {
            this.setLayoutParams(this.layoutParams);
        }
        else
        {
            this.setLayoutParams(this.hideLayoutParams);
        }
        this.requestLayout();
    }

    public void setWebViewRect(int left, int top, int maxWidth, int maxHeight) {

        this.layoutParams.leftMargin = left;
        this.layoutParams.topMargin = top;
        this.layoutParams.width = maxWidth;
        this.layoutParams.height = maxHeight;
        this.hideLayoutParams.width = maxWidth;
        this.hideLayoutParams.height = maxHeight;
        this.updateLayoutParams();

        this.szWebViewRect = String.format("%d-%d-%d-%d", left, top, maxWidth, maxHeight);
    }

    public String getWebViewRectString() {
        return szWebViewRect;
    }

    public int getViewTag() {
        return viewTag;
    }

    public class MyWebChromeClient extends WebChromeClient {

        @Override
        public void onProgressChanged(WebView view, final int newProgress) {
            // 0 - 100
            CrossAppActivity.getContext().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    CrossAppWebViewNative webview = (CrossAppWebViewNative)view;
                    webview.setProgress(newProgress);
                }
            });
            CrossAppActivity.getContext().runOnGLThread(new Runnable() {
                @Override
                public void run() {
                    CrossAppWebViewHelper.onProgressChanged(viewTag, newProgress);
                }
            });

        }
        //For Android  >= 4.1
        public void openFileChooser(ValueCallback<Uri> valueCallback, String acceptType, String capture) {
            Log.d("WebChrome","WebChrome");
        }
        // Android 5.0以上
        @Override
        public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
            CrossAppActivity.getContext().getOnValueNativeCallbackListenner().OnValueCallback(webView, filePathCallback, fileChooserParams);
            return true;
        }

        @Override
        public void onShowCustomView(final View view, final CustomViewCallback callback) {
            super.onShowCustomView(view, callback);
            CrossAppActivity.getContext().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    videoCallback = callback;
                    if (videoView != null) {
                        videoCallback.onCustomViewHidden();
                        return;
                    }
                    view.setTag("videoView");
                    videoView = view;
                    setVisibility(View.GONE);
                    videoView.setVisibility(VISIBLE);
                    CrossAppActivity.getFrameLayout().addView(view);

                    videoView.bringToFront();
                    // 横屏显示
                    CrossAppActivity.getContext().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    //设置全屏
                    CrossAppActivity.getContext().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                            WindowManager.LayoutParams.FLAG_FULLSCREEN);

                }
            });
        }


        @Override
        public void onHideCustomView() {
            super.onHideCustomView();
            CrossAppActivity.getContext().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (videoView == null) {
                        return;
                    }

                    setVisibility(VISIBLE);
                    videoView.setVisibility(GONE);
                    CrossAppActivity.getFrameLayout().removeView(videoView);
                    videoView = null;

                    try {
                        videoCallback.onCustomViewHidden();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    //换成竖屏
                    CrossAppActivity.getContext().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }
            });
        }
    }

    public interface OnValueCallbackListenner {
        void OnValueCallback(ValueCallback<Uri> valueCallback);

        void OnValueCallback(ValueCallback valueCallback, String acceptType);

        void OnValueCallback(ValueCallback<Uri> valueCallback, String acceptType, String capture);

        void OnValueCallback(WebView webView, ValueCallback<Uri[]> filePathCallback, WebChromeClient.FileChooserParams fileChooserParams);

    }

}
