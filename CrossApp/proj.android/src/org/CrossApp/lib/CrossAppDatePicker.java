package org.CrossApp.lib;

import android.util.Log;
import android.webkit.ValueCallback;

public final class CrossAppDatePicker {
    private static CrossAppActivity context = CrossAppActivity.getContext();

    public static CADateTimePicker picker = null;
    private static String m_title;
    private static String m_submitStr;
    private static String m_cancelStr;
    private static String m_Date = "";
    private static CADateTimePicker.DatePickerMode m_mode = CADateTimePicker.DatePickerMode.Date;

    private static native void getDate(final String date);
    private static native void cancelSelect();

    public static void createDatePicker(String title,String submitStr,String cancelStr){
        m_title = title;
        m_submitStr = submitStr;
        m_cancelStr = cancelStr;
    }

    public static void setCallBack(){

    }

    public static void setDatePickerMode(int mode){
        m_mode = CADateTimePicker.DatePickerMode.values()[mode];
    }

    public static void dismiss(){
        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                picker.dismiss();
            }
        });
    }

    public static void show(){
        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                picker = CADateTimePicker.create(context,m_title,m_submitStr,m_cancelStr);
                picker.setDate(m_Date);
                picker.setDatePickerMode(m_mode);
                picker.setCancelCallBack(new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String value) {
                        context.runOnGLThread(new Runnable() {
                            @Override
                            public void run() {
                                cancelSelect();
                            }
                        });
                    }
                });
                picker.setCallBack(new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String value) {
                        context.runOnGLThread(new Runnable() {
                            @Override
                            public void run() {
                                getDate(value);
                            }
                        });


                        Log.d("liuguoyan","得到日期为"+value) ;
                    }
                });
                picker.show();
            }
        });
    }

    public static void setDate(String date){
        m_Date = date;
    }
}
