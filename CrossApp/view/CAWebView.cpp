
#include "CAWebView.h"
#include "basics/CAScheduler.h"
#include "basics/CAApplication.h"
#include "platform/CCPlatformConfig.h"
#include "animation/CAViewAnimation.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
#include "platform/apple/CAWebViewImpl.h"
#endif

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "platform/android/CAWebViewImpl.h"
#endif

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#include "platform/win32/CAWebViewImpl.h"
#endif

#include "basics/CANotificationCenter.h"

NS_CC_BEGIN

CAWebView::CAWebView()
: _impl(new CAWebViewImpl(this))
, m_pImageView(nullptr)
, m_pProgress(nullptr)
, m_pBackground(nullptr)
, m_obLastPoint(DPointZero)
, m_obLastContentSize(DSizeZero)
{
}

CAWebView::~CAWebView()
{
	CC_SAFE_DELETE(_impl);
}

CAWebView *CAWebView::createWithFrame(const DRect& rect)
{
	CAWebView* webView = new CAWebView();
	if (webView && webView->initWithFrame(rect))
	{
		webView->autorelease();
		return webView;
	}
	CC_SAFE_DELETE(webView);
	return NULL;
}

CAWebView *CAWebView::createWithCenter(const DRect& rect)
{
	CAWebView* webView = new CAWebView();
	if (webView && webView->initWithCenter(rect))
	{
		webView->autorelease();
		return webView;
	}
	CC_SAFE_DELETE(webView);
	return NULL;
}

CAWebView *CAWebView::createWithLayout(const CrossApp::DLayout &layout)
{
    CAWebView* webView = new CAWebView();
    if (webView && webView->initWithLayout(layout))
    {
        webView->autorelease();
        return webView;
    }
    CC_SAFE_DELETE(webView);
    return NULL;
}

bool CAWebView::init()
{
    m_pProgress = CAProgress::create();
    m_pProgress->setLayout(DLayout(DHorizontalLayoutFill, DVerticalLayout_T_H(0, 8)));
    m_pProgress->setRecSpe(false);
    m_pProgress->setProgressTintImage(CAImage::createWithColor4B(CAColor4B(80, 220, 0, 255)));
    m_pProgress->setProgressTrackImage(CAImage::createWithColor4B(CAColor4B::GRAY));
	this->addSubview(m_pProgress);
        
    return true;
}

void CAWebView::onEnterTransitionDidFinish()
{
    CAView::onEnterTransitionDidFinish();
    CAScheduler::getScheduler()->schedule(schedule_selector(CAWebView::update), this, 1/60.0f);
}

void CAWebView::onExitTransitionDidStart()
{
    CAView::onExitTransitionDidStart();
    CAScheduler::getScheduler()->unschedule(schedule_selector(CAWebView::update), this);
}

void CAWebView::onProgressChanged(float progress)
{
    m_pProgress->setProgress(progress, true);

    if (m_obProgressChanged)
        m_obProgressChanged(progress);
}

void CAWebView::loadHTMLString(const std::string &string, const std::string &baseURL)
{
	_impl->loadHTMLString(string, baseURL);
}

void CAWebView::loadURL(const std::string &url)
{
	_impl->loadURL(url);
}

void CAWebView::loadFile(const std::string &fileName)
{
    string::size_type index;
       
    index=fileName.find("?");
    if(index == string::npos )//不存在。
    {
        _impl->loadFile(fileName);
    }
    else
    {
        const std::string &params = fileName.substr(index, fileName.length());
        const std::string &subStrFileName = fileName.substr(0, index);
        _impl->loadFileWithParams(subStrFileName, params);
    }
}

void CAWebView::stopLoading()
{
	_impl->stopLoading();
}

void CAWebView::reload()
{
	_impl->reload();
}

bool CAWebView::canGoBack()
{
	return _impl->canGoBack();
}

bool CAWebView::canGoForward()
{
	return _impl->canGoForward();
}

void CAWebView::goBack()
{
	_impl->goBack();
}

void CAWebView::goForward()
{
	_impl->goForward();
}

std::string CAWebView::evaluateJavaScript(const std::string &js)
{
	return _impl->evaluateJavaScript(js);
}

std::string CAWebView::getHTMLSource()
{
#if( CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID )
	return evaluateJavaScript(std::string("window.local_obj.showSource('<head>'+") + "document.getElementsByTagName('html')[0].innerHTML+'</head>');");
#endif

#if( CC_TARGET_PLATFORM == CC_PLATFORM_IOS )
	return evaluateJavaScript("document.documentElement.innerHTML");
#endif

	return "";
}

void CAWebView::addUserScriptData(const std::string & data)
{
    _impl->addUserScriptData(data);
}

void CAWebView::addScriptMessageHandler(const std::string & name, const std::function<void(const std::string &)>& function)
{
    _impl->addScriptMessageHandler(name, function);
}

void CAWebView::removeScriptMessageHandler(const std::string & name)
{
    _impl->removeScriptMessageHandler(name);

}

void CAWebView::setScalesPageToFit(bool const scalesPageToFit)
{
	_impl->setScalesPageToFit(scalesPageToFit);
}

void CAWebView::hideNativeWebAndShowImage()
{
    _impl->getWebViewImage([=](CAImage* image){
    
        this->removeSubview(m_pImageView);
        m_pImageView = nullptr;
        if (m_pImageView == nullptr)
        {
            m_pImageView= CAImageView::createWithLayout(DLayoutFill);
            this->insertSubview(m_pImageView, 1);
        }
        m_pImageView->setImage(image);
        _impl->setVisible(false);
        
    });
	
}

void CAWebView::showNativeWeb()
{
	_impl->setVisible(true);
}

void CAWebView::draw(Renderer* renderer, const Mat4 &transform, uint32_t flags)
{
	CAView::draw(renderer, transform, flags);
}

void CAWebView::setVisible(bool visible)
{
	CAView::setVisible(visible);
	_impl->setVisible(visible);
}

void CAWebView::setBackgroundView(CAView* background)
{
	if (m_pBackground)
	{
        m_pBackground->removeFromSuperview();
	}
    m_pBackground = background;

	if (m_pBackground)
	{
        m_pBackground->setLayout(DLayoutFill);
		this->insertSubview(m_pBackground, -1);
	}
}

CAView* CAWebView::getBackgroundView()
{
    return m_pBackground;
}

CAProgress* CAWebView::getProgress()
{
    return m_pProgress;
}

void CAWebView::update(float dt)
{
    do
    {
        CC_BREAK_IF(!CAApplication::getApplication()->isDrawing());
        DPoint point = this->convertToWorldSpace(m_obPoint);
        DSize contentSize = this->convertToWorldSize(m_obContentSize);
        CC_BREAK_IF(m_obLastPoint.equals(point) && m_obLastContentSize.equals(contentSize));
        m_obLastPoint = point;
        m_obLastContentSize = contentSize;

        _impl->update(dt);
    }
    while (0);
}


NS_CC_END




