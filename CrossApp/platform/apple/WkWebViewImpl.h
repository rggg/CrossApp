
#if !TARGET_OS_OSX
#import <UIKit/UIKit.h>
#else
#import <Cocoa/Cocoa.h>

#endif

#import <WebKit/WebKit.h>
#import <JavaScriptCore/JavaScriptCore.h>
#include <string>
#include <map>
#include <functional>

@interface WkWebViewWrapper : NSObject<WKUIDelegate,WKNavigationDelegate,WKScriptMessageHandler>
{
    std::map<std::string, std::function<void(const std::string &)>> _scriptMessageHandlerMap;
}
@property(nonatomic, retain) WKWebView* webView;
@property(nonatomic, retain) NSURL* requestUrl;

@property(nonatomic, readonly, getter=canGoBack) BOOL canGoBack;
@property(nonatomic, readonly, getter=canGoForward) BOOL canGoForward;

#if !TARGET_OS_OSX
@property(nonatomic) CGRect            frame;
#else
@property(nonatomic) NSRect            frame;

#endif
@property(nonatomic) bool injected;

+ (instancetype)webViewWrapper;

- (void)setVisible:(bool)visible;

- (NSData*)getWebViewImageData;

- (void)setFrameWith_X:(float)x Y:(float)y Width:(float)width Height:(float)height;

- (void)loadHTMLString:(const std::string &)string baseURL:(const std::string &)baseURL;

- (void)loadUrl:(const std::string &)urlString;

- (void)loadFile:(const std::string &)filePath;

- (void)loadFile:(const std::string &)filePath params:(const std::string &)params;

- (void)stopLoading;

- (void)reload;

- (void)goBack;

- (void)goForward;

- (std::string)evaluateJavaScript:(const std::string &)js;

- (void)addUserScriptData:(const std::string &)data;

- (void)addScriptMessageHandler:(const std::string &)name On:(const std::function<void(const std::string &)>&)function;

- (void)removeScriptMessageHandler:(const std::string &)name;

- (void)removeAllScriptMessageHandlers;

@end
