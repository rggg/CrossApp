
#include "CAWebViewImpl.h"
#include "basics/CAApplication.h"
#include "platform/CAFileUtils.h"
#include "platform/CADensityDpi.h"
#include "WkWebViewImpl.h"

USING_NS_CC;

#define WebViewWrapper ((WkWebViewWrapper*)m_pWebViewWrapper)

NS_CC_BEGIN

std::map<WkWebViewWrapper*, CAWebViewImpl*> s_WebViewImpls;

CAWebViewImpl::CAWebViewImpl(CAWebView *webView)
:m_pWebViewWrapper([WkWebViewWrapper webViewWrapper])
,m_bVisible(true)
,m_pWebView(webView)
{
    [WebViewWrapper retain];
    [WebViewWrapper setVisible:YES];
    s_WebViewImpls [WebViewWrapper] = this;
}

CAWebViewImpl::~CAWebViewImpl()
{
    [WebViewWrapper removeAllScriptMessageHandlers];
    s_WebViewImpls.erase(WebViewWrapper);
    [WebViewWrapper release];
    m_pWebViewWrapper = NULL;
}

typedef std::map<WkWebViewWrapper*, CAWebViewImpl*>::iterator WEB_MAP;

bool CAWebViewImpl::shouldStartLoading(void* pWebViewWrapper, const std::string &url)
{
    WEB_MAP it = s_WebViewImpls.find((WkWebViewWrapper*)pWebViewWrapper);
    if (it != s_WebViewImpls.end())
    {
        CAWebView* webView = it->second->m_pWebView;
        if (webView && webView->m_obStartLoading)
        {
            if (!webView->m_obStartLoading(url))
                return false;
        }
    }
    return true;
}

void CAWebViewImpl::didFinishLoading(void* pWebViewWrapper, const std::string &url)
{
    WEB_MAP it=s_WebViewImpls.find((WkWebViewWrapper*)pWebViewWrapper);
    
    if (it != s_WebViewImpls.end()) {
        
        CAWebView* webView = it->second->m_pWebView;
        if (webView && webView->m_obFinishLoading)
        {
            webView->m_obFinishLoading(url);
        }
    }
}

void CAWebViewImpl::didFailLoading(void* pWebViewWrapper, const std::string &url)
{
    WEB_MAP it=s_WebViewImpls.find((WkWebViewWrapper*)pWebViewWrapper);
    if (it != s_WebViewImpls.end())
    {
        CAWebView* webView = it->second->m_pWebView;
        if (webView && webView->m_obFailLoading)
        {
            webView->m_obFailLoading(url);
        }
    }
}

void CAWebViewImpl::onProgressChanged(void* pWebViewWrapper, float progress)
{
    WEB_MAP it=s_WebViewImpls.find((WkWebViewWrapper*)pWebViewWrapper);
    if (it != s_WebViewImpls.end())
    {
        CAWebView* webView = it->second->m_pWebView;
        if (webView)
        {
            webView->onProgressChanged(progress);
        }
    }
}

void CAWebViewImpl::onJsCallback(void* pWebViewWrapper, const std::string &message)
{
    WEB_MAP it=s_WebViewImpls.find((WkWebViewWrapper*)pWebViewWrapper);
    
    if (it != s_WebViewImpls.end())
    {
        CAWebView* webView = it->second->m_pWebView;
        if (webView && webView->m_obJSCallback)
        {
            webView->m_obJSCallback(message);
        }
    }
}


void CAWebViewImpl::loadHTMLString(const std::string &string, const std::string &baseURL)
{
    [WebViewWrapper loadHTMLString:string baseURL:baseURL];
}

void CAWebViewImpl::loadURL(const std::string &url)
{
    [WebViewWrapper loadUrl:url];
}

void CAWebViewImpl::loadFile(const std::string &fileName)
{
    std::string fullPath = FileUtils::getInstance()->fullPathForFilename(fileName);
    [WebViewWrapper loadFile:fullPath];
}

void CAWebViewImpl::loadFileWithParams(const std::string &fileName, const std::string &params)
{
    std::string fullPath = FileUtils::getInstance()->fullPathForFilename(fileName);
    [WebViewWrapper loadFile:fullPath params:params];
}

void CAWebViewImpl::stopLoading()
{
    [WebViewWrapper stopLoading];
}

void CAWebViewImpl::reload()
{
    [WebViewWrapper reload];
}

bool CAWebViewImpl::canGoBack()
{
    return WebViewWrapper.canGoBack;
}

bool CAWebViewImpl::canGoForward()
{
    return WebViewWrapper.canGoForward;
}

void CAWebViewImpl::goBack()
{
    [WebViewWrapper goBack];
}

void CAWebViewImpl::goForward()
{
    [WebViewWrapper goForward];
}

std::string CAWebViewImpl::evaluateJavaScript(const std::string &js)
{
    return [WebViewWrapper evaluateJavaScript:js];
}

void CAWebViewImpl::addUserScriptData(const std::string & data)
{
    [WebViewWrapper addUserScriptData:data];
}

void CAWebViewImpl::addScriptMessageHandler(const std::string & name, const std::function<void(const std::string &)>& function)
{
    [WebViewWrapper addScriptMessageHandler:name On:function];
}

void CAWebViewImpl::removeScriptMessageHandler(const std::string & name)
{
    [WebViewWrapper removeScriptMessageHandler:name];
}

void CAWebViewImpl::setScalesPageToFit(const bool scalesPageToFit)
{
}

void CAWebViewImpl::update(float dt)
{
    DRect rect = m_pWebView->convertRectToWorldSpace(m_pWebView->getBounds());
    
#if CC_TARGET_PLATFORM == CC_PLATFORM_MAC
    
    rect.origin.y = CAApplication::getApplication()->getWinSize().height - (rect.size.height + rect.origin.y);
    
    CGFloat scale = 1;
    
#elif CC_TARGET_PLATFORM == CC_PLATFORM_IOS
    
    CGFloat scale = [[UIScreen mainScreen] scale];
#endif
    
    rect.origin.x = s_dip_to_px(rect.origin.x);
    rect.origin.y = s_dip_to_px(rect.origin.y);
    rect.size.width = s_dip_to_px(rect.size.width);
    rect.size.height = s_dip_to_px(rect.size.height);
    
    CGFloat x = std::round(rect.origin.x / scale);
    CGFloat y = std::round(rect.origin.y / scale)-1;
    CGFloat width = std::round(rect.size.width / scale);
    CGFloat height = std::round(rect.size.height / scale);
    CCLog("x %f   y %f   scale %f",x, y, scale);
    [WebViewWrapper setFrameWith_X:x Y:y Width:width Height:height];
    
}

void CAWebViewImpl::setVisible(bool visible)
{
    m_bVisible = visible;
    [WebViewWrapper setVisible:visible];
}

void CAWebViewImpl::getWebViewImage(const std::function<void(CAImage*)>& callback)
{
	NSData* apple_data = [WebViewWrapper getWebViewImageData];
    if (apple_data != nil)
    {
        unsigned char* data = (unsigned char*)malloc([apple_data length]);
        [apple_data getBytes:data length:[apple_data length]];
        
        CAData* ca_data = CAData::create();
        ca_data->fastSet(data, [apple_data length]);
        CAImage *image = CAImage::createWithImageDataNoCache(ca_data);

        if (callback)
        {
            callback(image);
        }
    }
}

NS_CC_END



