
#import "WkWebViewImpl.h"
#include "EAGLView.h"
#include "CACommon.h"
#include "CAWebViewImpl.h"
#include "view/CAAlertView.h"
@implementation WkWebViewWrapper

+ (instancetype)webViewWrapper
{
    return [[[self alloc] init] autorelease];
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.injected = NO;
        //创建网页配置对象
        WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
        // 创建设置对象
        WKPreferences *preference = [[WKPreferences alloc]init];
        //最小字体大小 当将javaScriptEnabled属性设置为NO时，可以看到明显的效果
        preference.minimumFontSize = 0;
        //设置是否支持javaScript 默认是支持的
        preference.javaScriptEnabled = YES;
        // 在iOS上默认为NO，表示是否允许不经过用户交互由javaScript自动打开窗口
        preference.javaScriptCanOpenWindowsAutomatically = YES;
        config.preferences = preference;
        [preference setValue:@TRUE forKey:@"allowFileAccessFromFileURLs"];
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
        // 是使用h5的视频播放器在线播放, 还是使用原生播放器全屏播放
        config.allowsInlineMediaPlayback = YES;
        //设置是否允许画中画技术 在特定设备上有效
        config.allowsPictureInPictureMediaPlayback = YES;
#endif
//        config.applicationNameForUserAgent = @"ChinaDailyForiPad";
        
        //这个类主要用来做native与JavaScript的交互管理
        WKUserContentController * wkUController = [[WKUserContentController alloc] init];
        config.userContentController = wkUController;

        CGRect rect =  [[EAGLView sharedEGLView] bounds];
        rect.origin.x = rect.size.width;
        self.webView = [[WKWebView alloc] initWithFrame:rect configuration:config];
        self.webView.translatesAutoresizingMaskIntoConstraints = false;
        self.webView.allowsBackForwardNavigationGestures = YES;
        self.webView.UIDelegate = self;
        self.webView.navigationDelegate = self;

//#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
//        self.initialProgress = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] ;
//        self.initialProgress.center = CGPointMake(rect.size.width / 2, rect.size.height / 2);
//
//        [self.webView addSubview:self.initialProgress] ;
//        [self.initialProgress startAnimating] ;
//#endif

        //添加监测网页加载进度的观察者
        [self.webView addObserver:self
                           forKeyPath:@"estimatedProgress"
                              options:0
                              context:nil];
        //添加监测网页标题title的观察者
        [self.webView addObserver:self
                           forKeyPath:@"title"
                              options:NSKeyValueObservingOptionNew
                              context:nil];
        
        NSURLCache *sharedCache = [[[NSURLCache alloc] initWithMemoryCapacity:0 diskCapacity:0 diskPath:nil] autorelease];
        [NSURLCache setSharedURLCache:sharedCache];
    }
    return self;
}


- (void)setupWebView
{
    if (!self.webView.superview)
    {
        EAGLView * eaglview = [EAGLView sharedEGLView];
        [eaglview addSubview:self.webView];
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
        [eaglview bringSubviewToFront: self.webView];
#endif
    }
}


- (void)dealloc
{
    //移除观察者
    [self.webView removeObserver:self
                      forKeyPath:NSStringFromSelector(@selector(estimatedProgress))];
    [self.webView removeObserver:self
                      forKeyPath:NSStringFromSelector(@selector(title))];
    
    [self.webView setUIDelegate:nil];
    [self.webView removeFromSuperview];
    [self.webView loadHTMLString:@"" baseURL:nil];
    [self.webView stopLoading];
    [self.webView release];
//#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
//    [self.initialProgress release];
//#endif
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [super dealloc];
}

- (void)setVisible:(bool)visible
{
    [self setupWebView];
    self.webView.hidden = !visible;
}

- (NSData*)getWebViewImageData;{
    [self setupWebView];
    NSData* data = nil;
#if CC_TARGET_PLATFORM == CC_PLATFORM_MAC
    
    NSImage* image = [[[NSImage alloc]initWithData:[self.webView dataWithPDFInsideRect:[self.webView bounds]]]autorelease];
    data = [image TIFFRepresentationUsingCompression:NSTIFFCompressionNone factor:1];
    
#elif CC_TARGET_PLATFORM == CC_PLATFORM_IOS
    
    static float scale = [UIScreen mainScreen].scale;
    UIGraphicsBeginImageContextWithOptions(self.webView.bounds.size, NO, scale);
    [self.webView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage* image = UIGraphicsGetImageFromCurrentImageContext();
    data = UIImagePNGRepresentation(image);
    
#endif
    
    return data;
}

- (void)setFrameWith_X:(float)x Y:(float)y Width:(float)width Height:(float)height
{
    [self setupWebView];

#if CC_TARGET_PLATFORM == CC_PLATFORM_MAC
    self.frame = NSMakeRect(x, y, width, height);
#elif CC_TARGET_PLATFORM == CC_PLATFORM_IOS
    self.frame = CGRectMake(x, y, width, height);
#endif
    [self updateFrame];
}

- (void)updateFrame
{
    if (self.injected == YES)
    {
        self.webView.frame = self.frame;
    }
    else
    {
#if CC_TARGET_PLATFORM == CC_PLATFORM_MAC
        NSRect rect = self.frame;
#elif CC_TARGET_PLATFORM == CC_PLATFORM_IOS
        CGRect rect = self.frame;
#endif
        rect.origin.x += 5000;
        rect.origin.y += 5000;
        self.webView.frame = rect;
    }
}

- (void)loadHTMLString:(const std::string &)string baseURL:(const std::string &)baseURL
{
    [self setupWebView];
    [self.webView loadHTMLString:@(string.c_str()) baseURL:[NSURL URLWithString:@(baseURL.c_str())]];
}

- (void)addCookie:(NSMutableURLRequest*)request
{
    // 在此处获取返回的cookie
    NSMutableDictionary *cookieDic = [NSMutableDictionary dictionary];
 
    NSMutableString *cookieValue = [NSMutableString stringWithFormat:@""];
    NSHTTPCookieStorage *cookieJar = [NSHTTPCookieStorage sharedHTTPCookieStorage];
 
    for (NSHTTPCookie *cookie in [cookieJar cookies]) {
        [cookieDic setObject:cookie.value forKey:cookie.name];
    }
 
    // cookie重复，先放到字典进行去重，再进行拼接
    for (NSString *key in cookieDic) {
        NSString *appendString = [NSString stringWithFormat:@"%@=%@;", key, [cookieDic valueForKey:key]];
        [cookieValue appendString:appendString];
    }
    [request addValue:cookieValue forHTTPHeaderField:@"Cookie"];
}

- (NSURLRequest *)getRequest:(NSString *)urlString
{

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    return request;
}


- (void)loadUrl:(const std::string &)urlString
{
    [self setupWebView];
    self.requestUrl = [NSURL URLWithString:@(urlString.c_str())];
    NSURLRequest *request = [self getRequest:@(urlString.c_str())];
    [self.webView loadRequest:request];
    
}

- (void)loadFile:(const std::string &)filePath
{
    [self setupWebView];
    
    NSURL *url = [NSURL fileURLWithPath:@(filePath.c_str())];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:request]; 
    self.requestUrl = url;
}

- (void)loadFile:(const std::string &)filePath params:(const std::string &)params
{
    [self setupWebView];
    
    NSURL *url = [NSURL fileURLWithPath:@(filePath.c_str())];
    NSString *pathString = [NSString stringWithCString:filePath.c_str() encoding:[NSString defaultCStringEncoding]];
    NSString *paramStr = [NSString stringWithCString:params.c_str() encoding:[NSString defaultCStringEncoding]];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:paramStr relativeToURL:[NSURL fileURLWithPath:pathString]]];
    [self.webView loadRequest:request];
    self.requestUrl = url;
}

- (void)stopLoading
{
    [self.webView stopLoading];
}

- (void)reload
{
    [self.webView reload];
}

- (BOOL)canGoForward
{
    return self.webView.canGoForward;
}

- (BOOL)canGoBack
{
    return self.webView.canGoBack;
}

- (void)goBack
{
    [self.webView goBack];
}

- (void)goForward
{
    [self.webView goForward];
}

- (std::string)evaluateJavaScript:(const std::string &)js
{
    NSString* script = [NSString stringWithUTF8String:js.c_str()];
    [self.webView evaluateJavaScript:script completionHandler:^(id result, NSError *error) {
        if (error) {
            NSLog(@"JavaScript Error: %@ (%@)", error, script);
        }
    }];
    return "";
}

- (void)addUserScriptData:(const std::string &)data
{
    //这里我直接把我需要注入的js用文件的方式引入
    NSString *content = [NSString stringWithUTF8String:data.c_str()];
    WKUserScript* script = [[WKUserScript alloc] initWithSource:content injectionTime:WKUserScriptInjectionTimeAtDocumentStart forMainFrameOnly:false];
    [self.webView.configuration.userContentController addUserScript:script];
}

- (void)addScriptMessageHandler:(const std::string &)name On:(const std::function<void(const std::string &)>&)function
{
    if (_scriptMessageHandlerMap.find(name) == _scriptMessageHandlerMap.end())
    {
        [self.webView.configuration.userContentController addScriptMessageHandler:self name:[NSString stringWithUTF8String:name.c_str()]];
    }
    _scriptMessageHandlerMap[name] = function;
}

- (void)removeScriptMessageHandler:(const std::string &)name
{
    [[self.webView configuration].userContentController removeScriptMessageHandlerForName:[NSString stringWithUTF8String:name.c_str()]];
    _scriptMessageHandlerMap.erase(name);
}

- (void)removeAllScriptMessageHandlers
{
    for (auto& pair : _scriptMessageHandlerMap) {
        [[self.webView configuration].userContentController removeScriptMessageHandlerForName:[NSString stringWithUTF8String:pair.first.c_str()]];
    }
    _scriptMessageHandlerMap.clear();
}

//kvo 监听进度 必须实现此方法
-(void)observeValueForKeyPath:(NSString *)keyPath
                  ofObject:(id)object
                    change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                   context:(void *)context{
 if ([keyPath isEqualToString:NSStringFromSelector(@selector(estimatedProgress))]
     && object == self.webView)
 {
     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
         
         self.injected = self.webView.estimatedProgress >= 1.0f ? YES : NO;
         [self updateFrame];
         CrossApp::CAWebViewImpl::onProgressChanged(self, self.webView.estimatedProgress);
        NSLog(@"网页加载进度 = %f", self.webView.estimatedProgress);
         
     });
 }
 else if([keyPath isEqualToString:@"title"]
          && object == self.webView)
 {
     
 }
 else
 {
     [super observeValueForKeyPath:keyPath
                          ofObject:object
                            change:change
                           context:context];
 }
}

//通过接收JS传出消息的name进行捕捉的回调方法
- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message
{
//    NSLog(@"------------------------------------");
//    NSLog(@"WKWebView: js 回调");
//    NSLog(@"-----------------------------");
//    NSLog(@"name:%@", message.name);
//    NSLog(@"-----------------------------");
//    NSLog(@"body:%@\n", message.body);
//    NSLog(@"-----------------------------");
    
    std::string name = [message.name UTF8String];
    if (message.body != nil && _scriptMessageHandlerMap.find(name) != _scriptMessageHandlerMap.end())
    {
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:message.body
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&error];
        if (! jsonData) {
            NSLog(@"Got an error: %@", error);
        } else {
            NSString *jsonString = [[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding] autorelease];
            std::string json = [jsonString UTF8String];
            _scriptMessageHandlerMap[name](json);
        }
    }
}

// 页面开始加载时调用
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation
{
    CrossApp::CCLog("didStartProvisionalNavigation");
}

// 页面加载失败时调用
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error
{
    CrossApp::CCLog("didFailProvisionalNavigation");
    NSString *url = error.userInfo[NSURLErrorFailingURLStringErrorKey];
    if (url) {
        CrossApp::CAWebViewImpl::didFailLoading(self, [url UTF8String]);
    }
}

   // 当内容开始返回时调用
- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation
{
    NSString *url = [self.requestUrl absoluteString];
    CrossApp::CAWebViewImpl::shouldStartLoading(self, [url UTF8String]);
}
   // 页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
//#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
//    [self.initialProgress removeFromSuperview] ;
//#endif
    
    NSString *url = [self.requestUrl absoluteString];
    CrossApp::CAWebViewImpl::didFinishLoading(self, [url UTF8String]);
}

   //提交发生错误时调用
- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error {
//#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
//    [self.initialProgress removeFromSuperview] ;
//#endif
    CrossApp::CCLog("didFailNavigation");
}
  // 接收到服务器跳转请求即服务重定向时之后调用
- (void)webView:(WKWebView *)webView didReceiveServerRedirectForProvisionalNavigation:(WKNavigation *)navigation {
    CrossApp::CCLog("didReceiveServerRedirectForProvisionalNavigation");
}

   // 根据WebView对于即将跳转的HTTP请求头信息和相关信息来决定是否跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
   
   NSString * urlStr = navigationAction.request.URL.absoluteString;
   NSLog(@"发送跳转请求：%@",urlStr);
    
    if (!CrossApp::CAWebViewImpl::shouldStartLoading(self, [urlStr UTF8String]) && urlStr)
    {
        decisionHandler(WKNavigationActionPolicyCancel);
    }
    else
    {
        decisionHandler(WKNavigationActionPolicyAllow);
    }
    
   /*
    // 自己定义的协议头
   NSString *htmlHeadString = @"github://";
   if([urlStr hasPrefix:htmlHeadString])
   {
       UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"通过截取URL调用OC" message:@"你想前往我的Github主页?" preferredStyle:UIAlertControllerStyleAlert];
       [alertController addAction:([UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
       }])];
       [alertController addAction:([UIAlertAction actionWithTitle:@"打开" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
           NSURL * url = [NSURL URLWithString:[urlStr stringByReplacingOccurrencesOfString:@"github://callName_?" withString:@""]];
           [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
       }])];
       UIWindow * window = [UIApplication sharedApplication].keyWindow;
       [window.rootViewController presentViewController:alertController animated:YES completion:nil];
       decisionHandler(WKNavigationActionPolicyCancel);
   }
    */
}
   
   // 根据客户端受到的服务器响应头以及response相关信息来决定是否可以跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler{
    
   NSString * urlStr = navigationResponse.response.URL.absoluteString;
   NSLog(@"当前跳转地址：%@",urlStr);

   decisionHandler(WKNavigationResponsePolicyAllow);
   //decisionHandler(WKNavigationResponsePolicyCancel);
}

   //需要响应身份验证时调用 同样在block中需要传入用户身份凭证
- (void)webView:(WKWebView *)webView didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential * _Nullable credential))completionHandler{
    CrossApp::CCLog("didReceiveAuthenticationChallenge");

    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
            if ([challenge previousFailureCount] == 0) {
                NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
                completionHandler(NSURLSessionAuthChallengeUseCredential, credential);
//                completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge, nil);
            } else {
                completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge, nil);
            }
    } else {
        completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge, nil);
    }

//
//   //用户身份信息
//   NSURLCredential * newCred = [[NSURLCredential alloc] initWithUser:@"user123" password:@"123" persistence:NSURLCredentialPersistenceNone];
//   //为 challenge 的发送方提供 credential
//   [challenge.sender useCredential:newCred forAuthenticationChallenge:challenge];
//   completionHandler(NSURLSessionAuthChallengeUseCredential,newCred);
}
   //进程被终止时调用
- (void)webViewWebContentProcessDidTerminate:(WKWebView *)webView
{
    CrossApp::CCLog("webViewWebContentProcessDidTerminate");
    [self.webView reload];
}

/**
    *  web界面中有弹出警告框时调用
    *
    *  @param webView           实现该代理的webview
    *  @param message           警告框中的内容
    *  @param completionHandler 警告框消失调用
    */
- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler {
    
    CrossApp::CCLog("runJavaScriptAlertPanelWithMessage");
    
    CrossApp::CAAlertView::create([message UTF8String], "")->show([=](int index){
        
        completionHandler();
    });
}

   // 确认框
   //JavaScript调用confirm方法后回调的方法 confirm是js中的确定框，需要在block中把用户选择的情况传递进去
- (void)webView:(WKWebView *)webView runJavaScriptConfirmPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(BOOL))completionHandler{
    
    CrossApp::CCLog("runJavaScriptConfirmPanelWithMessage");
    
    CrossApp::CAAlertView::create("", "", {"Cancel", "OK"})->show([=](int index){
        
        completionHandler(index == 1 ? YES : NO);
    });
}

   // 输入框
   //JavaScript调用prompt方法后回调的方法 prompt是js中的输入框 需要在block中把用户输入的信息传入
- (void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(NSString *)defaultText initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSString * _Nullable))completionHandler{
    
    CrossApp::CCLog("runJavaScriptTextInputPanelWithPrompt");
}

   // 页面是弹出窗口 _blank 处理
- (WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures {

   if (!navigationAction.targetFrame.isMainFrame) {

       NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[navigationAction.request URL] cachePolicy: NSURLRequestReturnCacheDataElseLoad timeoutInterval:30.0f];
       [self addCookie:request];
       [webView loadRequest:request];
   }
    return nil;
}

@end

