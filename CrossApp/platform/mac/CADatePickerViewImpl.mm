//
//  CADatePickerViewNative.m
//  pickerviewtest
//
//  Created by 李辉 on 2022/2/22.
//

#import "platform/CADatePickerViewImpl.h"
#include "EAGLView.h"
#import <Cocoa/Cocoa.h>
#include "CCApplication.h"

typedef NS_ENUM(NSUInteger, NaitiveDatePickerMode) {
    NaitiveDatePickerModeTime = 0,
    NaitiveDatePickerModeDate = 1,
    NaitiveDatePickerModeDateAndTime = 2
};

@interface CADatePickerViewImpl : NSView{
    
}
@property(nonatomic,copy)NSString *contryCode;
@property(nonatomic,copy)NSString *title;
@property(nonatomic,copy)NSString *submitStr;
@property(nonatomic,copy)NSString *cancelStr;
@property(nonatomic,assign)NaitiveDatePickerMode nativePickerMode;
@property(nonatomic,assign)NSDatePickerStyle pickStyle;
@property(nonatomic,assign)NSDatePickerMode datePickerMode;
@property(nonatomic,assign)NSDatePickerElementFlags elementFlags;
@property(nonatomic,strong)NSTimeZone *timeZone;
@property(nonatomic)NSTimeInterval countDownDuration;
@property(nonatomic,assign)std::function<void(const std::string&)> callback;
@property(nonatomic,assign)std::function<void()> cancelcallback;
-(void)setDate:(NSDate*)date animated:(BOOL)animated;

-(void)show;

-(void)setStartFrame:(CGRect)frame;
@end


@interface CADatePickerViewImpl()<NSAnimationDelegate>

@property(nonatomic,strong)NSDatePicker *datePicker;
@property(nonatomic,strong)NSView *backgroundView;
@property(nonatomic,strong)NSButton *submitBtn;
@property(nonatomic,strong)NSButton *closeBtn;
@property(nonatomic,strong)NSTextField *titleLabel;
@property(nonatomic,copy)NSString *selectDate;
@end

@implementation CADatePickerViewImpl

-(instancetype)init{
    if (self = [super init]){
        self.wantsLayer = YES;
        [self.layer setBackgroundColor:[NSColor colorWithWhite:0 alpha:0.4].CGColor];
        [self addDatePicker];
        return self;
    }
    return nil;
}

-(void)setStartFrame:(CGRect)frame{
    [self.titleLabel setFrame:CGRectMake(frame.size.width/2 - 75, 380, 150, 50)];
    [self.backgroundView setFrame:CGRectMake(0, -450, frame.size.width, 450)];
    [self.datePicker setFrame:CGRectMake(frame.size.width/2 - 150, 150, 300, 150)];
    [self.submitBtn setFrame:CGRectMake(0, 450 - 55, 100, 55)];
    [self.closeBtn setFrame:CGRectMake(frame.size.width - 100, 450 - 55, 100, 55)];
    if (_nativePickerMode == NaitiveDatePickerModeDate){
        [self.datePicker setFrameOrigin:CGPointMake(self.datePicker.frame.origin.x + 70, self.datePicker.frame.origin.y)];
    }
    
}

-(void)setFrame:(CGRect)frame
{
    [super setFrame:frame];

}


-(void)mouseDown:(NSEvent *)event{
    
}

-(void)rightMouseDown:(NSEvent *)event{
    
}
-(void)addDatePicker{

    
    [self setAcceptsTouchEvents:YES];
    NSColor *bgColor;
    
    NSAppearance *appearance = [NSApp effectiveAppearance];
    if (@available(macOS 10.14,*)){
        if ([appearance bestMatchFromAppearancesWithNames:@[NSAppearanceNameAqua,NSAppearanceNameDarkAqua]] == NSAppearanceNameDarkAqua){
            //深色模式
            bgColor = [NSColor darkGrayColor];
        }else{
            bgColor = [NSColor whiteColor];
        }
        
    }else{
        bgColor = [NSColor whiteColor];
    }
    
    self.backgroundView = [[NSView alloc] initWithFrame:CGRectZero];
    self.backgroundView.wantsLayer = YES;
    [self.backgroundView.layer setBackgroundColor:bgColor.CGColor];
    self.backgroundView.layer.cornerRadius = 10;
    self.backgroundView.layer.masksToBounds = YES;
    self.backgroundView.layer.borderWidth = 0.3;
    self.backgroundView.layer.borderColor = [bgColor CGColor];
    [self addSubview:self.backgroundView];
    
    NSString *language = [NSString stringWithCString:CrossApp::CCApplication::sharedApplication()->getLocaleIdentifiers().c_str() encoding:NSUTF8StringEncoding];
    
    self.datePicker = [[NSDatePicker alloc] initWithFrame:CGRectMake(0, 0, 300, 150)];
    NSCalendar *currentCalendar = [NSCalendar currentCalendar];
    currentCalendar.locale = [[NSLocale alloc] initWithLocaleIdentifier:language];
    self.datePicker.locale = [[NSLocale alloc] initWithLocaleIdentifier:language];
    self.datePicker.calendar = currentCalendar;
    [self.datePicker setDatePickerStyle:NSClockAndCalendarDatePickerStyle];
    self.datePicker.datePickerMode = NSDatePickerModeRange;
    self.datePicker.wantsLayer = YES;
    [self.datePicker setTarget:self];
    [self.datePicker setDateValue:[NSDate date]];
    self.datePicker.datePickerElements = NSDatePickerElementFlagHourMinute | NSDatePickerElementFlagYearMonthDay;
    [self.datePicker setAction:@selector(dateChange:)];
    self.datePicker.layer.backgroundColor = bgColor.CGColor;
    [self.datePicker setDateValue:[NSDate date]];
    
    [self.backgroundView addSubview:self.datePicker];
    
    self.submitBtn = [NSButton buttonWithTitle:@"确定" target:self action:@selector(submitCallBack:)];
    self.submitBtn.layer.masksToBounds = YES;
    [self.backgroundView addSubview:self.submitBtn];
    
    self.closeBtn = [NSButton buttonWithTitle:@"取消" target:self action:@selector(cancelCallBack:)];
    self.closeBtn.layer.masksToBounds = YES;
    [self.backgroundView addSubview:self.closeBtn];
    
    self.titleLabel = [[NSTextField alloc] init];
    self.titleLabel.wantsLayer = YES;
    [self.titleLabel.layer setBackgroundColor:bgColor.CGColor];
    [self.titleLabel setBackgroundColor:bgColor];
    [self.titleLabel setBordered:NO];
    [self.titleLabel setEditable:NO];
    [self.titleLabel setAlignment:NSTextAlignmentCenter];
    [self.titleLabel setStringValue:@"标题"];
    [self.titleLabel setFont:[NSFont systemFontOfSize:20]];
    [self.backgroundView addSubview:self.titleLabel];
    
}

-(void)setNativePickerMode:(NaitiveDatePickerMode)nativePickerMode{
    _nativePickerMode = nativePickerMode;
    switch (nativePickerMode) {
        case NaitiveDatePickerModeTime:
            self.datePicker.datePickerElements = NSDatePickerElementFlagHourMinuteSecond;
            
            break;
        case NaitiveDatePickerModeDate:
            self.datePicker.datePickerElements = NSDatePickerElementFlagYearMonthDay;
            [self.datePicker setFrameOrigin:CGPointMake(self.datePicker.frame.origin.x + 70, self.datePicker.frame.origin.y)];
            break;
        case NaitiveDatePickerModeDateAndTime:
            self.datePicker.datePickerElements = NSDatePickerElementFlagYearMonthDay | NSDatePickerElementFlagHourMinuteSecond;
            break;
        default:
            break;
    }
}

- (void)setCountDownDuration:(NSTimeInterval)countDownDuration{
    _countDownDuration = countDownDuration;
}

-(void)setTimeZone:(NSTimeZone *)timeZone{
    _timeZone = timeZone;
    self.datePicker.timeZone = timeZone;
}

-(void)setContryCode:(NSString *)contryCode{
    _contryCode= contryCode;
}

-(void)setTitle:(NSString *)title{
    _title = title;
    
    [self.titleLabel setStringValue:title];
}

-(void)setSubmitStr:(NSString *)submitStr{
    _submitStr = submitStr;
    [self.submitBtn setTitle:submitStr];
}

-(void)setDatePickerMode:(NSDatePickerMode)datePickerMode{
    _datePickerMode = datePickerMode;
}

-(void)setPickStyle:(NSDatePickerStyle)pickStyle{
    _pickStyle = pickStyle;
//    [self.datePicker setPreferredDatePickerStyle:pickStyle];
}

-(void)setCancelStr:(NSString *)cancelStr{
    _cancelStr = cancelStr;
    [self.closeBtn setTitle:cancelStr];
}

- (void)dateChange:(NSDatePicker *)datePicker {

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd HH:mm";
    //设置时间格式
    switch (self.nativePickerMode) {
        case NaitiveDatePickerModeTime:
            formatter.dateFormat = @"HH:mm";
            break;
        case NaitiveDatePickerModeDate:
            formatter.dateFormat = @"yyyy-MM-dd";
            break;

        case NaitiveDatePickerModeDateAndTime:
            formatter.dateFormat = @"yyyy-MM-dd HH:mm";
            break;
        default:
            break;
    }

    NSString *dateStr = [formatter  stringFromDate:self.datePicker.dateValue];
    NSLog(@"%@",dateStr);
    self.selectDate = dateStr;
}

//点击确定按钮
-(void)submitCallBack:(id)sender{
    if(self.callback && self.selectDate){
        self.callback(std::string([self.selectDate cStringUsingEncoding:NSUTF8StringEncoding]));
    }
//    [self animationHide:self.backgroundView];
    [self removeFromSuperview];
}
//点击取消按钮
-(void)cancelCallBack:(id)sender{
    
    if (self.cancelcallback){
        self.cancelcallback();
    }
    [self animationHide:self.backgroundView];
}

-(void)animationHide:(NSView *)view{
    NSRect newViewFrame;
    NSMutableDictionary* firstViewDict = [NSMutableDictionary dictionaryWithCapacity:3];
    NSRect firstViewFrame = [view frame];
    
    
    [firstViewDict setObject:view forKey:NSViewAnimationTargetKey];
    
   //设置视图的起始位置
    [firstViewDict setObject:[NSValue valueWithRect:firstViewFrame]
                      forKey:NSViewAnimationStartFrameKey];
    
    // 改变视图1的位置，并把改变之后的位置变为视图结束位置
    newViewFrame = firstViewFrame;
    newViewFrame.origin.x += 0; //x不变，
    newViewFrame.origin.y = -450; //y加50
    [firstViewDict setObject:[NSValue valueWithRect:newViewFrame]
                      forKey:NSViewAnimationEndFrameKey];

    NSMutableDictionary* secondViewDict = [NSMutableDictionary dictionaryWithCapacity:2];
    [secondViewDict setObject:self forKey:NSViewAnimationTargetKey];
    [secondViewDict setObject:NSViewAnimationFadeOutEffect forKey:NSViewAnimationEffectKey];

    NSViewAnimation *animation = [[NSViewAnimation alloc] initWithViewAnimations:[NSArray arrayWithObjects:firstViewDict,secondViewDict,nil]];
    [animation setDelegate:self];
    [animation setDuration:0.2];    // a half seconds.
    [animation setAnimationCurve:NSAnimationEaseOut];
    [animation startAnimation];
    [animation release];
}

-(void)animationDidEnd:(NSAnimation *)animation{
    [self removeFromSuperview];
}



-(void)show{
    [self animationAlert:self.backgroundView];
}

- (void)animationAlert:(NSView *)view
{
    NSRect newViewFrame;
    NSMutableDictionary* firstViewDict = [NSMutableDictionary dictionaryWithCapacity:3];
    NSRect firstViewFrame = [self.backgroundView frame];
    
    
    [firstViewDict setObject:self.backgroundView forKey:NSViewAnimationTargetKey];
    
   //设置视图的起始位置
    [firstViewDict setObject:[NSValue valueWithRect:firstViewFrame]
                      forKey:NSViewAnimationStartFrameKey];
    
    // 改变视图1的位置，并把改变之后的位置变为视图结束位置
    newViewFrame = firstViewFrame;
    newViewFrame.origin.x += 0; //x不变，
    newViewFrame.origin.y = 0; //y加50
    [firstViewDict setObject:[NSValue valueWithRect:newViewFrame]
                      forKey:NSViewAnimationEndFrameKey];
    
    NSMutableDictionary* secondViewDict = [NSMutableDictionary dictionaryWithCapacity:2];
    [secondViewDict setObject:self forKey:NSViewAnimationTargetKey];
    [secondViewDict setObject:NSViewAnimationFadeInEffect forKey:NSViewAnimationEffectKey];

    NSViewAnimation *animation1 = [[NSViewAnimation alloc] initWithViewAnimations:[NSArray arrayWithObjects:firstViewDict,secondViewDict,nil]];
    [animation1 setDuration:0.2];    // a half seconds.
    [animation1 setAnimationCurve:NSAnimationEaseIn];
    [animation1 startAnimation];
    [animation1 release];
    
//    NSViewAnimation *animation2 = [[NSViewAnimation alloc] initWithViewAnimations:[NSArray arrayWithObjects:secondViewDict,nil]];
//    [animation2 setDuration:0.2];    // a half seconds.
//    [animation2 setAnimationCurve:NSAnimationEaseIn];
//    [animation2 startAnimation];
//    [animation2 release];
}

-(void)setDate:(NSDate *)date animated:(BOOL)animated{
    [self.datePicker setDateValue:date];
    [self dateChange:self.datePicker];
}
@end


USING_NS_CC;



CADatePickerViewNative::CADatePickerViewNative(const std::string& title, const std::string& submitStr, const std::string& cancelStr)
:m_sTitle(title)
,m_sSubmitStr(submitStr)
,m_sCancelStr(cancelStr)
,m_pDatePicker(nullptr)
,m_sMode(DatePickerMode::Date)
,m_sStyle(DatePickerStyle::Automatic)
,m_sTimeZone("Asia/Shanghai")
,m_sCountDownDuration(0)
,m_callback(nullptr)
,m_sAnimated(false)
,m_sDate("")
{
    m_pDatePicker = [[CADatePickerViewImpl alloc] init];

}

CADatePickerViewNative::~CADatePickerViewNative(){
    m_pDatePicker = nullptr;
    m_callback = nullptr;
}

CADatePickerViewNative* CADatePickerViewNative::create(const std::string& title, const std::string& submitStr, const std::string& cancelStr){
    CADatePickerViewNative *pickerView = new CADatePickerViewNative(title,submitStr,cancelStr);
    pickerView->autorelease();
    return pickerView;
}


void CADatePickerViewNative::setDate(std::string date, bool animated){
    m_sDate = date;
    m_sAnimated = animated;
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    format.locale = [NSLocale localeWithLocaleIdentifier:[NSString stringWithCString:CCApplication::sharedApplication()->getLocaleIdentifiers().c_str() encoding:NSUTF8StringEncoding]];
     [format setDateFormat:@"yyyy-MM-dd HH:mm"];
     NSDate *ndate = [format dateFromString:[NSString stringWithCString:m_sDate.c_str() encoding:NSUTF8StringEncoding]];
    [(CADatePickerViewImpl*)m_pDatePicker setDate:ndate animated:animated];
}

void CADatePickerViewNative::show(){
    initNative();
    EAGLView * eaglview = [EAGLView sharedEGLView];
    [(CADatePickerViewImpl*)m_pDatePicker setFrame:CGRectMake(0, 0, eaglview.frame.size.width, eaglview.frame.size.height)];
    [eaglview addSubview:(CADatePickerViewImpl*)m_pDatePicker];
    [(CADatePickerViewImpl*)m_pDatePicker show];
}

void CADatePickerViewNative::initNative(){
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    format.locale = [NSLocale localeWithLocaleIdentifier:[NSString stringWithCString:CCApplication::sharedApplication()->getLocaleIdentifiers().c_str() encoding:NSUTF8StringEncoding]];
     [format setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSDate *date;
    if (m_sDate.compare("") == 0){
        date = [NSDate date];
    }else{
        date = [format dateFromString:[NSString stringWithCString:m_sDate.c_str() encoding:NSUTF8StringEncoding]];
    }
     
    
    CADatePickerViewImpl *temp = (CADatePickerViewImpl*)m_pDatePicker;
    [temp setTitle:[NSString stringWithCString:m_sTitle.c_str() encoding:NSUTF8StringEncoding]];
    [temp setCancelStr:[NSString stringWithCString:m_sCancelStr.c_str() encoding:NSUTF8StringEncoding]];
    [temp setSubmitStr:[NSString stringWithCString:m_sSubmitStr.c_str() encoding:NSUTF8StringEncoding]];
    if (m_sMode == DatePickerMode::Date){
        [(CADatePickerViewImpl*)m_pDatePicker setNativePickerMode:NaitiveDatePickerModeDate];
    }else{
        [(CADatePickerViewImpl*)m_pDatePicker setNativePickerMode:NaitiveDatePickerModeDateAndTime];
    }
    [temp setDate:date animated:m_sAnimated];
    [temp setTimeZone:[NSTimeZone timeZoneWithName:[NSString stringWithCString:m_sTimeZone.c_str() encoding:NSUTF8StringEncoding]]];
    [temp setCountDownDuration:m_sCountDownDuration];
}

void CADatePickerViewNative::setDatePickerMode(DatePickerMode mode){
    m_sMode = mode;
    if (mode == DatePickerMode::Date){
        [(CADatePickerViewImpl*)m_pDatePicker setNativePickerMode:NaitiveDatePickerModeDate];
    }else{
        [(CADatePickerViewImpl*)m_pDatePicker setNativePickerMode:NaitiveDatePickerModeDateAndTime];
    }
}

void CADatePickerViewNative::setCallBack(std::function<void(std::string)> callback){
    m_callback = callback;
}

void CADatePickerViewNative::show(const std::function<void(const std::string&)>& callback,const std::function<void()>& cancelCallback){
    m_cancelCallBack = cancelCallback;
    initNative();
    m_callback = callback;
    ((CADatePickerViewImpl*)m_pDatePicker).callback = m_callback;
    ((CADatePickerViewImpl*)m_pDatePicker).cancelcallback = m_cancelCallBack;
    EAGLView * eaglview = [EAGLView sharedEGLView];
    [(CADatePickerViewImpl*)m_pDatePicker setStartFrame:CGRectMake(0, 0, eaglview.frame.size.width, eaglview.frame.size.height)];
    [(CADatePickerViewImpl*)m_pDatePicker setFrame:CGRectMake(0, 0, eaglview.frame.size.width, eaglview.frame.size.height)];
    [eaglview addSubview:(CADatePickerViewImpl*)m_pDatePicker];
    [(CADatePickerViewImpl*)m_pDatePicker show];
}


