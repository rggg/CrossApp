//
//  CADatePickerViewNative.m
//  pickerviewtest
//
//  Created by 李辉 on 2022/2/22.
//

#import "../CADatePickerViewImpl.h"
#include "../CCPlatformMacros.h"
#include "../../basics/CANotificationCenter.h"
#include <map>
#include <string>
#include <android/log.h>
#include <jni.h>
#include "jni/JniHelper.h"

NS_CC_BEGIN

static std::function<void(std::string)> m_scallback;
static std::function<void()> m_scancelcallback;
#define CLASS_NAME "org/CrossApp/lib/CrossAppDatePicker"

void createDatePickerJNI(const char* title, const char* submitStr, const char* cancelStr)
{
    JniMethodInfo t;
    if (JniHelper::getStaticMethodInfo(t, CLASS_NAME, "createDatePicker", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V"))
    {
        jstring stringArg1 = t.env->NewStringUTF(title);
        jstring stringArg2 = t.env->NewStringUTF(submitStr);
        jstring stringArg3 = t.env->NewStringUTF(cancelStr);
        t.env->CallStaticVoidMethod(t.classID, t.methodID, stringArg1, stringArg2, stringArg3);

        t.env->DeleteLocalRef(stringArg1);
        t.env->DeleteLocalRef(stringArg2);
        t.env->DeleteLocalRef(stringArg3);
        t.env->DeleteLocalRef(t.classID);
    }
}

void setDatePickerModeNative(int mode){
    JniMethodInfo t;
    if (JniHelper::getStaticMethodInfo(t, CLASS_NAME, "setDatePickerMode", "(I)V"))
    {
        t.env->CallStaticVoidMethod(t.classID, t.methodID, mode);
        t.env->DeleteLocalRef(t.classID);
    }
}

void setDateNative(const char* dateStr)
{
    JniMethodInfo t;
    if (JniHelper::getStaticMethodInfo(t, CLASS_NAME, "setDate", "(Ljava/lang/String;)V"))
    {
        jstring stringArg1 = t.env->NewStringUTF(dateStr);

        t.env->CallStaticVoidMethod(t.classID, t.methodID, stringArg1);

        t.env->DeleteLocalRef(stringArg1);
        t.env->DeleteLocalRef(t.classID);
    }
}

void showNative()
{
    JniMethodInfo t;
    if (JniHelper::getStaticMethodInfo(t, CLASS_NAME, "show", "()V"))
    {

        t.env->CallStaticVoidMethod(t.classID, t.methodID);

        t.env->DeleteLocalRef(t.classID);
    }
}

extern "C"
{
    JNIEXPORT void JNICALL Java_org_CrossApp_lib_CrossAppDatePicker_getDate(JNIEnv *env, jclass cls, jstring date)
    {
        if (m_scallback)
        {
            const char* str = env->GetStringUTFChars(date, NULL);
            m_scallback(std::string(str));
        }

    }

    JNIEXPORT void JNICALL Java_org_CrossApp_lib_CrossAppDatePicker_cancelSelect(JNIEnv *env, jclass cls, jstring date)
    {

//        CrossApp::CANotificationCenter::getInstance()->postNotification("cancelSelect");
        if (m_scancelcallback){
            m_scancelcallback();
        }
        

    }
}




NS_CC_END


USING_NS_CC;


CADatePickerViewNative::CADatePickerViewNative(const std::string& title, const std::string& submitStr, const std::string& cancelStr)
:m_sTitle(title)
,m_sSubmitStr(submitStr)
,m_sCancelStr(cancelStr)
,m_pDatePicker(nullptr)
,m_sMode(DatePickerMode::Date)
,m_sStyle(DatePickerStyle::Automatic)
,m_sTimeZone("Asia/Shanghai")
,m_sCountDownDuration(0)
,m_callback(nullptr)
,m_sAnimated(false)
{
    createDatePickerJNI(title.c_str(),submitStr.c_str(),cancelStr.c_str());
}

CADatePickerViewNative::~CADatePickerViewNative(){
    m_pDatePicker = nullptr;
    m_callback = nullptr;
}

void CADatePickerViewNative::setDatePickerMode(DatePickerMode var){
    m_sMode = var;
    setDatePickerModeNative((int)m_sMode);
}

CADatePickerViewNative* CADatePickerViewNative::create(const std::string& title, const std::string& submitStr, const std::string& cancelStr){
    CADatePickerViewNative *pickerView = new CADatePickerViewNative(title,submitStr,cancelStr);
    pickerView->autorelease();
    return pickerView;
}


void CADatePickerViewNative::setDate(std::string date, bool animated){
    m_sDate = date;
    m_sAnimated = animated;
    setDateNative(date.c_str());
}

void CADatePickerViewNative::setCallBack(std::function<void(std::string)> callback){
    m_callback = callback;
}

void CADatePickerViewNative::show(){
    if(m_callback){
        m_scallback = m_callback;
    }
    showNative();
}

void CADatePickerViewNative::initNative(){
    
}



void CADatePickerViewNative::show(const std::function<void(const std::string&)>& callback,const std::function<void()>& cancelCallback){
    m_cancelCallBack = cancelCallback;
    m_callback = callback;
    if(m_callback){
        m_scallback = m_callback;
    }
    if(m_cancelCallBack)
    {
        m_scancelcallback = m_cancelCallBack;
    }
    showNative();
}


