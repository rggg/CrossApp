//
//  CADatePickerViewNative.m
//  pickerviewtest
//
//  Created by 李辉 on 2022/2/22.
//

#import "platform/CADatePickerViewImpl.h"
#include "EAGLView.h"


USING_NS_CC;


CADatePickerViewNative::CADatePickerViewNative(const std::string& title, const std::string& submitStr, const std::string& cancelStr)
:m_sTitle(title)
,m_sSubmitStr(submitStr)
,m_sCancelStr(cancelStr)
,m_pDatePicker(nullptr)
,m_sContryCode("zh")
,m_sMode(DatePickerMode::Time)
,m_sStyle(DatePickerStyle::Automatic)
,m_sTimeZone("Asia/Shanghai")
,m_sCountDownDuration(0)
,m_callback(nullptr)
,m_sAnimated(false)
{
    

}

CADatePickerViewNative::~CADatePickerViewNative(){
    m_pDatePicker = nullptr;
    m_callback = nullptr;
}

CADatePickerViewNative* CADatePickerViewNative::create(const std::string& title, const std::string& submitStr, const std::string& cancelStr){
    CADatePickerViewNative *pickerView = new CADatePickerViewNative(title,submitStr,cancelStr);
    pickerView->autorelease();
    return pickerView;
}


void CADatePickerViewNative::setDatePickerMode(DatePickerMode mode){
    
}

void CADatePickerViewNative::setCallBack(std::function<void(std::string)> callback){
    
}

void CADatePickerViewNative::setDate(std::string date, bool animated){

}

void CADatePickerViewNative::show(){

}

void CADatePickerViewNative::initNative(){
    
}

void CADatePickerViewNative::show(const std::function<void(const std::string&)>& callback,const std::function<void()>& cancelCallback){


}


