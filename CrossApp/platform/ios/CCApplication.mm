

#import "CCApplication.h"
#import <UIKit/UIKit.h>
#import "CAApplicationCaller.h"


NS_CC_BEGIN

CCApplication* CCApplication::sm_pSharedApplication = 0;

CCApplication::CCApplication()
{
    CC_ASSERT(! sm_pSharedApplication);
    sm_pSharedApplication = this;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *languages = [defaults objectForKey:@"AppleLanguages"];
    NSString *currentLanguage = [languages objectAtIndex:0];
    NSDictionary* temp = [NSLocale componentsFromLocaleIdentifier:currentLanguage];
    NSString * languageCode = [temp objectForKey:NSLocaleLanguageCode];
    setLocaleIdentifiers([languageCode cStringUsingEncoding:NSUTF8StringEncoding]);
}

CCApplication::~CCApplication()
{
    CC_ASSERT(this == sm_pSharedApplication);
    sm_pSharedApplication = 0;
}

int CCApplication::run()
{
    if (applicationDidFinishLaunching()) 
    {
        [[CAApplicationCaller sharedApplicationCaller] startMainLoop];
    }
    return 0;
}

void CCApplication::setAnimationInterval(double interval)
{
    [[CAApplicationCaller sharedApplicationCaller] setAnimationInterval: interval ];
}

/////////////////////////////////////////////////////////////////////////////////////////////////
// static member function
//////////////////////////////////////////////////////////////////////////////////////////////////

CCApplication* CCApplication::sharedApplication()
{
    CC_ASSERT(sm_pSharedApplication);
    return sm_pSharedApplication;
}

LanguageType CCApplication::getCurrentLanguage()
{
    // get the current language and country config
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *languages = [defaults objectForKey:@"AppleLanguages"];
    NSString *currentLanguage = [languages objectAtIndex:0];
    
    // get the current language code.(such as English is "en", Chinese is "zh" and so on)
    NSDictionary* temp = [NSLocale componentsFromLocaleIdentifier:currentLanguage];
    NSString * languageCode = [temp objectForKey:NSLocaleLanguageCode];
    
    if ([languageCode isEqualToString:@"zh"]) return LanguageType::CHINESE;
    if ([languageCode isEqualToString:@"en"]) return LanguageType::ENGLISH;
    if ([languageCode isEqualToString:@"fr"]) return LanguageType::FRENCH;
    if ([languageCode isEqualToString:@"it"]) return LanguageType::ITALIAN;
    if ([languageCode isEqualToString:@"de"]) return LanguageType::GERMAN;
    if ([languageCode isEqualToString:@"es"]) return LanguageType::SPANISH;
    if ([languageCode isEqualToString:@"nl"]) return LanguageType::DUTCH;
    if ([languageCode isEqualToString:@"ru"]) return LanguageType::RUSSIAN;
    if ([languageCode isEqualToString:@"ko"]) return LanguageType::KOREAN;
    if ([languageCode isEqualToString:@"ja"]) return LanguageType::JAPANESE;
    if ([languageCode isEqualToString:@"hu"]) return LanguageType::HUNGARIAN;
    if ([languageCode isEqualToString:@"pt"]) return LanguageType::PORTUGUESE;
    if ([languageCode isEqualToString:@"ar"]) return LanguageType::ARABIC;
    if ([languageCode isEqualToString:@"nb"]) return LanguageType::NORWEGIAN;
    if ([languageCode isEqualToString:@"pl"]) return LanguageType::POLISH;
    if ([languageCode isEqualToString:@"tr"]) return LanguageType::TURKISH;
    if ([languageCode isEqualToString:@"uk"]) return LanguageType::UKRAINIAN;
    if ([languageCode isEqualToString:@"ro"]) return LanguageType::ROMANIAN;
    if ([languageCode isEqualToString:@"bg"]) return LanguageType::BULGARIAN;
    if ([languageCode isEqualToString:@"cy"]) return LanguageType::CYPRUS;
    if ([languageCode isEqualToString:@"ir"]) return LanguageType::IRAN;
    if ([languageCode isEqualToString:@"sy"]) return LanguageType::SYRIA;
    if ([languageCode isEqualToString:@"ge"]) return LanguageType::GEORGIA;
    if ([languageCode isEqualToString:@"af"]) return LanguageType::AFGHANISTAN;
    if ([languageCode isEqualToString:@"tj"]) return LanguageType::TAJIKISTAN;
    if ([languageCode isEqualToString:@"pk"]) return LanguageType::PAKISTAN;
    if ([languageCode isEqualToString:@"iq"]) return LanguageType::IRAQ;
    if ([languageCode isEqualToString:@"qa"]) return LanguageType::QATAR;
    if ([languageCode isEqualToString:@"ars"]) return LanguageType::SAUDI_ARABIA;
    if ([languageCode isEqualToString:@"ae"]) return LanguageType::UNITED_ARAB_EMIRATES;
    if ([languageCode isEqualToString:@"fa"]) return LanguageType::FARSI;
    if ([languageCode isEqualToString:@"el"]) return LanguageType::GREEK;

    return LanguageType::ENGLISH;
}

TargetPlatform CCApplication::getTargetPlatform()
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) // idiom for iOS <= 3.2, otherwise: [UIDevice userInterfaceIdiom] is faster.
    {
        return kTargetIpad;
    }
    else 
    {
        return kTargetIphone;
    }
}

void CCApplication::setStatusBarStyle(const CAStatusBarStyle& var)
{
    switch (var)
    {
        case CAStatusBarStyle::Default:
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
            break;
        case CAStatusBarStyle::LightContent:
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
            break;
        default:
            break;
    }
}

void CCApplication::setStatusBarHidden(bool isStatusBarHidden)
{
    [[UIApplication sharedApplication] setStatusBarHidden:isStatusBarHidden];
}

bool CCApplication::isStatusBarHidden()
{
    return [[UIApplication sharedApplication] isStatusBarHidden];
}

NS_CC_END
