
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface CAApplicationCaller : NSObject {
        CADisplayLink *displayLink;
}
-(void) startMainLoop;
-(void) doCaller: (id) sender;
-(void) setAnimationInterval :(float)sender;
+(id) sharedApplicationCaller;
+(void) destroy;
@end
