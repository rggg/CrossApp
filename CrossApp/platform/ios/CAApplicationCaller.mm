
#import <Foundation/Foundation.h>
#import <OpenGLES/EAGL.h>
#import "CAApplicationCaller.h"
#import "CAApplication.h"
#import "EAGLView.h"

static id s_sharedDirectorCaller;

@implementation CAApplicationCaller

+(id) sharedApplicationCaller
{
    if (s_sharedDirectorCaller == nil)
    {
        s_sharedDirectorCaller = [CAApplicationCaller new];
    }
    
    return s_sharedDirectorCaller;
}

+(void) destroy
{
    [s_sharedDirectorCaller release];
    s_sharedDirectorCaller = nil;
}

-(void) alloc
{
        
}

-(void) dealloc
{
    [displayLink release];
    [super dealloc];
}

-(void) startMainLoop
{
        // CCDirector::setAnimationInterval() is called, we should invalidate it first
    [displayLink invalidate];
    displayLink = nil;
    
    displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(doCaller:)];
    if (@available(iOS 15.0, *)) {
        [displayLink setPreferredFrameRateRange:CAFrameRateRangeMake(60, 120, 120)];
    } else {
        // Fallback on earlier versions
        displayLink.preferredFramesPerSecond = 60;
    }
//    CFTimeInterval time = [displayLink duration];
//    NSLog(@" %f",time);
    [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
}
-(void) setAnimationInterval:(float)sender{
    
}
                      
-(void) doCaller: (id) sender
{
    if ([EAGLView sharedEGLView])
    {
        [EAGLContext setCurrentContext: [[EAGLView sharedEGLView] context]];
    }
    else
    {
        [displayLink release];
    }

    CrossApp::CAApplication::getApplication()->mainLoop();
}

@end
