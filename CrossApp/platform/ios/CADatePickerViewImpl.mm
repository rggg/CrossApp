//
//  CADatePickerViewNative.m
//  pickerviewtest
//
//  Created by 李辉 on 2022/2/22.
//

#import "platform/CADatePickerViewImpl.h"
#import <UIKit/UIKit.h>
#include "EAGLView.h"
#include "CCApplication.h"
#include "../../basics/CANotificationCenter.h"

@interface CADatePickerViewImpl : UIView{
    
}
@property(nonatomic,copy)NSString *contryCode;
@property(nonatomic,copy)NSString *title;
@property(nonatomic,copy)NSString *submitStr;
@property(nonatomic,copy)NSString *cancelStr;
@property(nonatomic,assign)UIDatePickerStyle pickStyle
API_AVAILABLE(ios(13.4)) API_UNAVAILABLE(tvos, watchos);
@property(nonatomic,assign)UIDatePickerMode datePickerMode;
@property(nonatomic,strong)NSTimeZone *timeZone;
@property(nonatomic)NSTimeInterval countDownDuration;
@property(nonatomic,assign)std::function<void(const std::string&)> callback;
@property(nonatomic,assign)std::function<void()> cancelcallback;

-(void)setDate:(NSDate*)date animated:(BOOL)animated;

-(void)show;

@end

@interface CADatePickerViewImpl()<UIGestureRecognizerDelegate>

@property(nonatomic,strong)UIDatePicker *datePicker;
@property(nonatomic,strong)UIView *backgroundView;
@property(nonatomic,strong)UIButton *submitBtn;
@property(nonatomic,strong)UIButton *closeBtn;
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,copy)NSString *selectDate;
@property(nonatomic,strong)UIControl *control;
@property(nonatomic,strong)UIControl *bgcontrol;
@end

@implementation CADatePickerViewImpl

-(instancetype)init{
    if (self = [super init]){
        [self setBackgroundColor:[UIColor colorWithWhite:0 alpha:0]];
        [self addDatePicker];
        return self;
    }
    return nil;
}

-(void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    [self.control setFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
    [self.titleLabel setFrame:CGRectMake(0, 0, frame.size.width-40, 30)];
    [self.titleLabel setCenter:CGPointMake((frame.size.width)/2, 30)];
    [self.backgroundView setFrame:CGRectMake(0, frame.size.height, frame.size.width, 450)];
    [self.bgcontrol setFrame:CGRectMake(0, 0, frame.size.width, 300)];
    [self.datePicker setCenter:CGPointMake((frame.size.width )/2, 220)];
    [self.submitBtn setFrame:CGRectMake(0, 0, 100, 55)];
    [self.closeBtn setFrame:CGRectMake(frame.size.width - 100, 0, 100, 55)];
}

- (void)traitCollectionDidChange:(UITraitCollection *)previousTraitCollection{
    [super traitCollectionDidChange:previousTraitCollection];
}
-(void)addDatePicker{
    self.control = [[UIControl alloc] init];
    [self addSubview:self.control];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    self.backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.backgroundView setBackgroundColor:[UIColor whiteColor]];
    
    self.backgroundView.layer.cornerRadius = 10;
    self.backgroundView.layer.masksToBounds = YES;
    self.backgroundView.layer.borderWidth = 0.3;
    self.backgroundView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    [self.backgroundView setUserInteractionEnabled:YES];
    self.bgcontrol = [[UIControl alloc] init];
    self.datePicker = [[UIDatePicker alloc] init];
    
    [self.backgroundView addSubview:self.bgcontrol];
    [self addSubview:self.backgroundView];

    NSString *language = [NSString stringWithCString:CrossApp::CCApplication::sharedApplication()->getLocaleIdentifiers().c_str() encoding:NSUTF8StringEncoding];
    NSCalendar *currentCalendar = [NSCalendar currentCalendar];
    currentCalendar.locale = [[NSLocale alloc] initWithLocaleIdentifier:language];
    
    self.datePicker.locale = [[NSLocale alloc] initWithLocaleIdentifier:language];
    self.datePicker.calendar = currentCalendar;
    [self.datePicker setFrame:CGRectMake(0, 0,280,200)];
    self.datePicker.backgroundColor = [UIColor clearColor];
    self.datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    self.datePicker.date = [NSDate date];
    [self.datePicker setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    [self.datePicker setContentVerticalAlignment:UIControlContentVerticalAlignmentFill];
    self.datePicker.minuteInterval = 1;
    [self.datePicker addTarget:self action:@selector(dateChange:) forControlEvents:UIControlEventValueChanged];
    [self.backgroundView addSubview:self.datePicker];
    
    if (@available(iOS 13.0, *)) {
        if( [UITraitCollection currentTraitCollection].userInterfaceStyle == UIUserInterfaceStyleDark){
            [self.backgroundView setBackgroundColor:[UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1]];
        }
    } else {
        // Fallback on earlier versions
        [self.backgroundView setBackgroundColor:[UIColor whiteColor]];
    }
    
    if (@available(iOS 14.0, *)) {
        [self.datePicker setPreferredDatePickerStyle:UIDatePickerStyleInline];
    } else {
        // Fallback on earlier versions
        if (@available(iOS 13.4, *)) {
            [self.datePicker setPreferredDatePickerStyle:UIDatePickerStyleWheels];
        } else {
            // Fallback on earlier versions
        }
    }
    
    self.submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.submitBtn setTitle:self.submitStr forState:UIControlStateNormal];
    [self.submitBtn setTitleColor:[UIColor systemBlueColor] forState:UIControlStateNormal];
    [self.submitBtn setTitleColor:[UIColor systemGrayColor] forState:UIControlStateHighlighted];
    self.submitBtn.layer.masksToBounds = YES;
    [self.submitBtn addTarget:self action:@selector(submitCallBack:) forControlEvents:UIControlEventTouchUpInside];
    [self.backgroundView addSubview:self.submitBtn];
    
    self.closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.closeBtn setTitle:self.cancelStr forState:UIControlStateNormal];
    [self.closeBtn setTitleColor:[UIColor systemBlueColor] forState:UIControlStateNormal];
    [self.closeBtn setTitleColor:[UIColor systemGrayColor] forState:UIControlStateHighlighted];
    [self.closeBtn addTarget:self action:@selector(cancelCallBack:) forControlEvents:UIControlEventTouchUpInside];
    self.closeBtn.layer.masksToBounds = YES;
    [self.backgroundView addSubview:self.closeBtn];
    
    self.titleLabel = [[UILabel alloc] init];
    [self.titleLabel setText:self.title];
    [self.titleLabel setFont:[UIFont systemFontOfSize:20]];
    [self.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [self.backgroundView addSubview:self.titleLabel];
    
}

- (void)keyboardWillShow:(NSNotification *)notification{
    
    NSDictionary *userInfo = [notification userInfo];
    NSValue *value = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [value CGRectValue];
    int height = keyboardRect.size.height;
    
    [UIView animateWithDuration:0.3 animations:^{
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        self.backgroundView.transform = CGAffineTransformMakeTranslation(0, -height - self.backgroundView.frame.size.height);
    } completion:^(BOOL finished) {
    }];
}

- (void)keyboardWillHide:(NSNotification *)notification{
    [UIView animateWithDuration:0.3 animations:^{
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        self.backgroundView.transform = CGAffineTransformMakeTranslation(0, -self.backgroundView.frame.size.height);
    } completion:^(BOOL finished) {
    }];
}

- (void)setCountDownDuration:(NSTimeInterval)countDownDuration{
    _countDownDuration = countDownDuration;
    self.datePicker.countDownDuration = countDownDuration;
}

-(void)setTimeZone:(NSTimeZone *)timeZone{
    _timeZone = timeZone;
    self.datePicker.timeZone = timeZone;
}

-(void)setContryCode:(NSString *)contryCode{
    _contryCode= contryCode;
}

-(void)setTitle:(NSString *)title{
    _title = title;
    [self.titleLabel setText:title];
}

-(void)setSubmitStr:(NSString *)submitStr{
    _submitStr = submitStr;
    [self.submitBtn setTitle:submitStr forState:UIControlStateNormal];
}

-(void)setDatePickerMode:(UIDatePickerMode)datePickerMode{
    _datePickerMode = datePickerMode;
    [self.datePicker setDatePickerMode:datePickerMode];
}

-(void)setPickStyle:(UIDatePickerStyle)pickStyle{
    _pickStyle = pickStyle;
}

-(void)setCancelStr:(NSString *)cancelStr{
    _cancelStr = cancelStr;
    [self.closeBtn setTitle:cancelStr forState:UIControlStateNormal];
}

- (void)dateChange:(UIDatePicker *)datePicker {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    switch (self.datePickerMode) {
        case UIDatePickerModeDateAndTime:
            formatter.dateFormat = @"yyyy-MM-dd HH:mm";
            break;
        case UIDatePickerModeDate:
            formatter.dateFormat = @"yyyy-MM-dd";
            break;
            
        case UIDatePickerModeTime:
            formatter.dateFormat = @"HH:mm";
            break;
            
        case UIDatePickerModeCountDownTimer:
            formatter.dateFormat = @"hh:mm";
            break;
        default:
            break;
    }
    
    NSString *dateStr = [formatter  stringFromDate:datePicker.date];
    self.selectDate = dateStr;
}

//点击确定按钮
-(void)submitCallBack:(id)sender{
    if(self.callback && self.selectDate){
        self.callback(std::string([self.selectDate cStringUsingEncoding:NSUTF8StringEncoding]));
    }
    [self animationHide:self.backgroundView];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}
//点击取消按钮
-(void)cancelCallBack:(id)sender{
    if (self.cancelcallback){
        self.cancelcallback();
    }
    [self animationHide:self.backgroundView];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(void)animationHide:(UIView *)view{
    [UIView animateWithDuration:0.3 animations:^{
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        view.transform = CGAffineTransformMakeTranslation(0, self.backgroundView.frame.size.height);
        self.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

-(void)show{
    [self animationAlert:self.backgroundView];
}

- (void)animationAlert:(UIView *)view
{
    [UIView animateWithDuration:0.2 animations:^{
        [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
        self.backgroundView.transform = CGAffineTransformMakeTranslation(0, -self.backgroundView.frame.size.height);
        [self setBackgroundColor:[UIColor colorWithWhite:0 alpha:.6]];
    } completion:^(BOOL finished) {
        
    }];
}

-(void)setDate:(NSDate *)date animated:(BOOL)animated{
    [self.datePicker setDate:date animated:animated];
    [self dateChange:self.datePicker];
}
@end

USING_NS_CC;


CADatePickerViewNative::CADatePickerViewNative(const std::string& title, const std::string& submitStr, const std::string& cancelStr)
:m_sTitle(title)
,m_sSubmitStr(submitStr)
,m_sCancelStr(cancelStr)
,m_pDatePicker(nullptr)
,m_sMode(DatePickerMode::Date)
,m_sStyle(DatePickerStyle::Automatic)
,m_sTimeZone("Asia/Shanghai")
,m_sCountDownDuration(0)
,m_callback(nullptr)
,m_sAnimated(false)
{
    m_pDatePicker = [[CADatePickerViewImpl alloc] init];
}

CADatePickerViewNative::~CADatePickerViewNative(){
    m_pDatePicker = nullptr;
    m_callback = nullptr;
}

CADatePickerViewNative* CADatePickerViewNative::create(const std::string& title, const std::string& submitStr, const std::string& cancelStr){
    CADatePickerViewNative *pickerView = new CADatePickerViewNative(title,submitStr,cancelStr);
    pickerView->autorelease();
    return pickerView;
}


void CADatePickerViewNative::setDate(std::string date, bool animated){
    m_sDate = date;
    m_sAnimated = animated;
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    format.locale = [NSLocale localeWithLocaleIdentifier:[NSString stringWithCString:CCApplication::sharedApplication()->getLocaleIdentifiers().c_str() encoding:NSUTF8StringEncoding]];
     [format setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    NSDate *ndate = [format dateFromString:[NSString stringWithCString:m_sDate.c_str() encoding:NSUTF8StringEncoding]];
    
    [(CADatePickerViewImpl*)m_pDatePicker setDate:ndate animated:animated];
}

void CADatePickerViewNative::show(){
    initNative();

    EAGLView * eaglview = [EAGLView sharedEGLView];
    [(CADatePickerViewImpl*)m_pDatePicker setFrame:CGRectMake(0, 0, eaglview.frame.size.width, eaglview.frame.size.height)];
    [eaglview addSubview:(CADatePickerViewImpl*)m_pDatePicker];
    
    [(CADatePickerViewImpl*)m_pDatePicker show];
}

void CADatePickerViewNative::setCallBack(std::function<void(std::string)> callback){
    m_callback = callback;
}

void CADatePickerViewNative::setDatePickerMode(DatePickerMode var){
    m_sMode = var;
}

void CADatePickerViewNative::initNative(){
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    format.locale = [NSLocale localeWithLocaleIdentifier:[NSString stringWithCString:CCApplication::sharedApplication()->getLocaleIdentifiers().c_str() encoding:NSUTF8StringEncoding]];
     [format setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    NSDate *date;
    if(m_sDate.compare("") == 0){
        date = [NSDate date];
    }else{
        date = [format dateFromString:[NSString stringWithCString:m_sDate.c_str() encoding:NSUTF8StringEncoding]];
    }

    CADatePickerViewImpl *temp = (CADatePickerViewImpl*)m_pDatePicker;
    [temp setTitle:[NSString stringWithCString:m_sTitle.c_str() encoding:NSUTF8StringEncoding]];
    [temp setCancelStr:[NSString stringWithCString:m_sCancelStr.c_str() encoding:NSUTF8StringEncoding]];
    [temp setSubmitStr:[NSString stringWithCString:m_sSubmitStr.c_str() encoding:NSUTF8StringEncoding]];
    [temp setDatePickerMode:(UIDatePickerMode)m_sMode];
    [temp setDate:date animated:m_sAnimated];
    [temp setTimeZone:[NSTimeZone timeZoneWithName:[NSString stringWithCString:m_sTimeZone.c_str() encoding:NSUTF8StringEncoding]]];
    [temp setCountDownDuration:m_sCountDownDuration];
    if (@available(iOS 13.4, *)) {
        [temp setPickStyle:(UIDatePickerStyle)m_sStyle];
    } else {
        // Fallback on earlier versions
    }
}

void CADatePickerViewNative::show(const std::function<void(const std::string&)>& callback,const std::function<void()>& cancelCallback){
    m_cancelCallBack = cancelCallback;
    initNative();
    m_callback = callback;
    ((CADatePickerViewImpl*)m_pDatePicker).callback = m_callback;
    ((CADatePickerViewImpl*)m_pDatePicker).cancelcallback = m_cancelCallBack;
    EAGLView * eaglview = [EAGLView sharedEGLView];
    [(CADatePickerViewImpl*)m_pDatePicker setFrame:CGRectMake(0, 0, eaglview.frame.size.width, eaglview.frame.size.height)];
    [eaglview addSubview:(CADatePickerViewImpl*)m_pDatePicker];
    [(CADatePickerViewImpl*)m_pDatePicker show];
    
}


