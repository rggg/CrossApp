//
//  CADatePickerViewNative.h
//  CrossApp
//
//  Created by 李辉 on 2022/2/23.
//  Copyright © 2022 cocos2d-x. All rights reserved.
//

#ifndef __CADatePickerViewImpl_H__
#define __CADatePickerViewImpl_H__
#include <stdio.h>
#include <string>
#include <functional>
#include "ccMacros.h"

#include "basics/CAObject.h"
#include "basics/CASTLContainer.h"
#include <functional>

NS_CC_BEGIN

class CC_DLL CADatePickerViewNative : public CAObject
{
public:
    enum class DatePickerMode : int
    {
        Date = 1,           // Displays month, day, and year depending on the locale setting (e.g. November | 15 | 2007)
        DateAndTime,    // Displays date, hour, minute, and optionally AM/PM designation depending on the locale setting (e.g. Wed Nov 15 | 6 | 53 | PM)
    };
    
    enum class DatePickerStyle : int//iOS13.4 later
    {
        Automatic = 0,           //iOS13.4 later
        Wheels,
        Compact,
        Inline, //iOS 14.0 later
    };
    
public:

    static CADatePickerViewNative* create(const std::string& title, const std::string& submitStr, const std::string& cancelStr);

    void setDate(std::string date, bool animated);
    
    void setDatePickerMode(DatePickerMode mode);
    
    void setCallBack(std::function<void(std::string)> callback);

    void show();

    void show(const std::function<void(const std::string&)>& callback,const std::function<void()>& cancelCallback = nullptr);

private:
    
    CC_SYNTHESIZE(DatePickerStyle, m_sStyle, DatePickerStyle);
    
    CC_SYNTHESIZE(std::string, m_sTimeZone, TimeZone);
    
    CC_SYNTHESIZE(std::string, m_sDate, mDate);
    
    CC_SYNTHESIZE(double, m_sCountDownDuration, CountDownDuration);

    CADatePickerViewNative(const std::string& title, const std::string& submitStr, const std::string& cancelStr);
    
    ~CADatePickerViewNative();
    
    void initNative();
    
private:
    
    
    std::string m_sTitle;
    std::string m_sSubmitStr;
    std::string m_sCancelStr;
    void*       m_pDatePicker;
    bool        m_sAnimated;
    DatePickerMode  m_sMode;
    std::function<void(std::string)> m_callback;
    std::function<void()> m_cancelCallBack;
};

NS_CC_END

#endif /* CADatePickerViewImpl_h */
