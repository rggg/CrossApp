APP_STL := c++_static
APP_CPPFLAGS := -fexceptions -frtti -DCC_ENABLE_CHIPMUNK_INTEGRATION=1 -std=c++1y -stdlib=libc++ -fsigned-char -Wno-extern-c-compat -fPIC -w

APP_LDFLAGS := -latomic

APP_ABI := armeabi-v7a arm64-v8a
APP_SHORT_COMMANDS := true

ifeq ($(NDK_DEBUG),1)
  APP_CPPFLAGS += -DCROSS_DEBUG=1
  APP_OPTIM := debug
else
  APP_CPPFLAGS += -DNDEBUG
  APP_OPTIM := release
endif