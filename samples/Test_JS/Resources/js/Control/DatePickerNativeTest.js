
var DatePickerNativeTest = ca.CAViewController.extend({
    ctor: function () {
        this._super();
    },
    viewDidLoad: function() {

//        ca.CADevice.setLocaleIdentifiers("zh");

        var btn1 = ca.CAButton.create(ca.CAButton.Type.Custom);
        btn1.setLayout(ca.DLayout.set(ca.DHorizontalLayout_W_C(240, 0.5), ca.DVerticalLayout_H_C(54, 0.16)));
        //设置Button标题的显示文本和字体大小
        btn1.setTitleForState(ca.CAControl.State.Normal, "Date");
        btn1.setTitleFontSize(40);
        //设置Button的标题颜色和状态
        btn1.setTitleColorForState(ca.CAControl.State.Normal, ca.CAColor4B.set(51,204,255,255));
        //添加回调事件
            
            
            
        btn1.addTarget(function(){
            let datepicker = ca.CADatePickerViewNative.create("Date","确认","取消");
            datepicker.setDatePickerMode(ca.DatePickerMode.Date);
            datepicker.show(function(date){
                ca.log("JS =======>>>>" + date);
            });
        }, ca.CAButton.Event.TouchUpInSide );
        this.getView().addSubview(btn1);




        var btn2 = ca.CAButton.create(ca.CAButton.Type.Custom);
        btn2.setLayout(ca.DLayout.set(ca.DHorizontalLayout_W_C(350, 0.5), ca.DVerticalLayout_H_C(54, 0.32)));
        //设置Button标题的显示文本
        btn2.setTitleForState(ca.CAControl.State.Normal, "DateAndTime");
        btn2.setTitleFontSize(40);
        btn2.setTitleColorForState(ca.CAControl.State.Normal, ca.CAColor4B.set(51,204,255,255));
        //添加回调事件
        btn2.addTarget(function(){
            let datepicker = ca.CADatePickerViewNative.create("DateAndTime","确认","取消");
            datepicker.setDate("2020-04-08 19:35",false);
            datepicker.setDatePickerMode(ca.DatePickerMode.DateAndTime);
            datepicker.show(function(date){
                ca.log("JS =======>>>>" + date);
            });
        
        }, ca.CAButton.Event.TouchUpInSide );
        this.getView().addSubview(btn2);

    },


});
