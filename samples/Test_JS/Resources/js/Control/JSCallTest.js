var JSCallTest = ca.CAViewController.extend({

    ctor: function () {
        this._super();
    },
    
    viewDidLoad: function() {
        
        {
            var btn1 = ca.CAButton.create(ca.CAButton.Type.SquareRect);
            btn1.setLayout(ca.DLayout.set(ca.DHorizontalLayout_W_C(240, 0.5), ca.DVerticalLayout_H_C(54, 0.16)));
            btn1.setTitleForState(ca.CAControl.State.Normal, "JS同步调用C++");
            btn1.addTarget(function() {
                
                ca.log("JS同步调用C++：");
                var result = ca.CAJSCall.getInstance().call("JSKEY_SYNC", "<JS端参数>");
                ca.log("<JS打印> " + result + "\n");
            }, ca.CAButton.Event.TouchUpInSide );
            this.getView().addSubview(btn1);
        }
        
        {
            var btn2 = ca.CAButton.create(ca.CAButton.Type.SquareRect);
            btn2.setLayout(ca.DLayout.set(ca.DHorizontalLayout_W_C(240, 0.5), ca.DVerticalLayout_H_C(54, 0.32)));
            btn2.setTitleForState(ca.CAControl.State.Normal, "JS可异步调用C++");
            btn2.addTarget(function() {
                
                ca.log("JS异步调用C++：");
                ca.CAJSCall.getInstance().call_async("JSKEY_ASYNC", "<JS端参数>").onReceive(function(result) {
                    ca.log("<JS打印> " + result + "\n");
                });
            }, ca.CAButton.Event.TouchUpInSide );
            this.getView().addSubview(btn2);
        }

        {
            var js_res = "<JS端返回>";
            var cRtnTxt = ca.CAJSCall.getInstance().listening_call("C++KEY_SYNC", function(parameter) {
                ca.log("<JS打印> " + parameter);
                return js_res;
            });
            
            var btn3 = ca.CAButton.create(ca.CAButton.Type.RoundedRect);
            btn3.setLayout(ca.DLayout.set(ca.DHorizontalLayout_W_C(240, 0.5), ca.DVerticalLayout_H_C(54, 0.48)));
            btn3.setTitleForState(ca.CAControl.State.Normal, "C++同步调用JS");
            btn3.addTarget(function(){
                ca.log("C++同步调用JS：");
                ca.CANotificationCenter.getInstance().postNotification("C++同步调用");
            }, ca.CAButton.Event.TouchUpInSide );
            this.getView().addSubview(btn3);
        }
        
        {
            var js_res = "<JS端返回>";
            ca.CAJSCall.getInstance().listening_call_async("C++KEY_ASYNC", function(parameter, receive) {
                ca.log("<JS打印> " + parameter);
                receive.receive(js_res);
            });
            
            var btn4 = ca.CAButton.create(ca.CAButton.Type.SquareRect);
            btn4.setLayout(ca.DLayout.set(ca.DHorizontalLayout_W_C(240, 0.5), ca.DVerticalLayout_H_C(54, 0.64)));
            btn4.setTitleForState(ca.CAControl.State.Normal, "C++可异步调用JS");
            btn4.addTarget(function() {
                
                ca.log("C++异步调用JS：");
                ca.CANotificationCenter.getInstance().postNotification("C++异步调用");
            }, ca.CAButton.Event.TouchDown );
            this.getView().addSubview(btn4);
        }
    },


});
