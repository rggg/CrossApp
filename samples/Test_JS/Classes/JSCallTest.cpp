//
//  JSCallTest.cpp
//  Test-js
//
//  Created by 李辉 on 2022/1/5.
//

#include "JSCallTest.h"
#include "CrossApp.h"

void JSCallTest::init()
{
    CAValue res = CAValue("<C++端返回>");
    CAJSCall::getInstance()->listening_call("JSKEY_SYNC", [=](const CAValue &parameter)-> CAValue {
        
        CCLog("<C++打印> %s", parameter.asString().c_str());
        return res;
    });
    
    CAJSCall::getInstance()->listening_call_async("JSKEY_ASYNC", [=](const CAValue& parameter, CAJSCallReceive* receive )-> void {
        
        CCLog("<C++打印> %s", parameter.asString().c_str());
        receive->receive(res);
    });
    
    CANotificationCenter::getInstance()->addObserver([=](CrossApp::CAObject *obj)-> void {
        
        
        
        const CAValue result = CAJSCall::getInstance()->call("C++KEY_SYNC", CAValue("<C++端参数>"));
        CCLog("<C++打印> %s\n", result.asString().c_str());
    }, NULL, "C++同步调用");
    
    CANotificationCenter::getInstance()->addObserver([=](CrossApp::CAObject *obj)-> void {

        CAJSCall::getInstance()->call_async("C++KEY_ASYNC", CAValue("<C++端参数>"))->onReceive([=](const CAValue& parameter)-> void {
            CCLog("<C++打印> %s\n", parameter.asString().c_str());
        });
    }, NULL, "C++异步调用");
    
}
