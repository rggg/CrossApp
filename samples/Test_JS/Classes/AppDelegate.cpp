#include "AppDelegate.h"
#include <js-bindings/jswrapper/v8/ScriptEngine.h>
#include <js-bindings/manual/jsb_global_init.h>
#include <js-bindings/manual/jsb_classtype.h>
#include <js-bindings/auto/jsb_crossapp_auto.h>
#include <js-bindings/manual/jsb_crossapp_manual.h>
#include <js-bindings/manual/jsb_global.h>
#include <js-bindings/event/EventDispatcher.h>
#include <js-bindings/manual/jsb_xmlhttprequest.h>
#include <js-bindings/manual/jsb_websocket.h>
#include "JSCallTest.h"

USING_NS_CC;

AppDelegate::AppDelegate()
{

}

AppDelegate::~AppDelegate()
{
    
}

bool AppDelegate::applicationDidFinishLaunching()
{
    CAApplication* applicationn = CAApplication::getApplication();
    applicationn->setClearColor(CAColor4F(0, 0, 0, 0));
    CCEGLView* pEGLView = CCEGLView::sharedOpenGLView();
    
    applicationn->setOpenGLView(pEGLView);
    
    se::ScriptEngine *se = se::ScriptEngine::getInstance();
    se->addBeforeInitHook([]() {
        JSBClassType::init();
    });
    se->addAfterCleanupHook([]() {
        JSBClassType::destroy();
    });

    jsb_init_file_operation_delegate();
    se->addRegisterCallback(jsb_register_global_variables);
    se->addRegisterCallback(register_all_crossapp);
    se->addRegisterCallback(register_all_crossapp_manual);
    se->addRegisterCallback(register_all_xmlhttprequest);
    se->addRegisterCallback(register_all_websocket);
    se->start();
    
    CANotificationCenter::getInstance()->addObserver([this](CAObject* obj){
        CCLog("121222");
    }, NULL, "abc");
    //(CALLBACK_BIND_1(, this), this, "123");
    
    
    cc::EventDispatcher::init();
    //Engine JS Load
    
    jsb_run_script("script/jsb_prepare.js");
    jsb_run_script("script/jsb_boot.js");
    jsb_run_script("script/jsb_crossapp.js");
    jsb_run_script("script/jsb-builtin.js");

    CAApplication::getApplication()->setJSEngine(true);
    CCLog("%d",se::ScriptEngine::getInstance()->isLoadFinished());
    //Logic JS Load
    
    JSCallTest::init();
    
    
    jsb_run_script("js/main.js");
    
    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground()
{
    CAApplication::getApplication()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
    // SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground()
{
    CAApplication::getApplication()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
    // SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
}
